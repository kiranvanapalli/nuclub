<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Referrals extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
        $this->load->library('my_file_upload');
    }
    public function index()
    {
        $where = '';
        $data['file'] = 'referrals/referrals_list';
        $data['custom_js'] = 'referrals/all_files_js';
        $sql="SELECT b.member_id,b.fullname,b.member_code,b.mobilenumber,b.points, COUNT(a.member_id) as member_ref_count FROM tb_referrals as a RIGHT JOIN tb_members as b ON a.member_id = b.member_id where b.status = 1 GROUP BY b.member_id";    
        $query = $this->db->query($sql);
        $data['count'] = $query->result_array(); 
        $this->load->view('admin_template/main', $data);
    }
    public function get_ref_data()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $where = ['member_id' => $_GET['id']];
            $type = "array";
            $data['member_list'] = $this->Allfiles_model->GetDataAllmodels("tb_referrals", $where, $type, 'member_id', $limit = '');
            $data['file'] = "referrals/get_ref_list";
            $data['custom_js'] = 'referrals/all_files_js';
            $this->load->view('admin_template/main', $data);

        }
    }
}
