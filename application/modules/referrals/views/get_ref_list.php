<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <div class="row mb-2">
                <!-- main title -->
                <div class="col-6">
                    <div class="main__title">
                        <h2>User Referrals List</h2>
                    </div>
                </div>

                <!-- <div class="col-6">
                    <div class="main__title">
                        <a href="<?php echo base_url() ?>add_member" class="main__title-link w-50">Add Member</a>
                    </div>
                </div> -->
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="table-responsive">
                                <table class="table table-sm mb-0 text-center user_referral_table">
                                    <thead>
                                        <tr>
                                            <th>FULL NAME</th>
                                          
                                            <th>Mobile Number</th>
                                            <th>Status</th>
                                            <th>Earned AMOUNT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($member_list as $member_list) {


                                        ?>

                                            <tr>
                                                <td>
                                                    <div class="main__table-text"><?php echo $member_list['fullname']; ?></div>
                                                </td>
                                                <!-- <td>
                                                    <?php if($member_list['member_code'] != '')
                                                    {
                                                        $member_code = $member_list['member_code'];
                                                    }
                                                    else
                                                    {
                                                        $member_code = "";
                                                    } ?> 
                                                    <div class="main__table-text"><?php echo $member_code; ?></div>
                                                </td> -->
                                                <td>
                                                    <div class="main__table-text"><?php echo $member_list['mobilenumber']; ?></div>
                                                </td>
                                                <td>
                                                    <div class="main__table-text">

                                                        <?php

                                                        if ($member_list['status'] == 2) {
                                                            $status = "Referral Send";
                                                            $class = 'badge badge-danger';
                                                        } else if ($member_list['status'] == 0) {
                                                            $status = "Payment Pending";
                                                            $class = 'badge badge-secondary';
                                                        } else if ($member_list['status'] == 1) {
                                                            $status = "Success";
                                                            $class = 'badge badge-success';
                                                        }
                                                        ?>

                                                    </div>
                                                    <span class="<?php echo $class; ?>"><?php echo $status; ?></span>
                                                </td>
                                                <td>
                                                    <div class="main__table-text"><?php echo $member_list['points']; ?></div>
                                                </td>


                                            </tr>

                                        <?php }

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>