<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <div class="row mb-2">
                <!-- main title -->
                <div class="col-6">
                    <div class="main__title">
                        <h2>NU Coin Transaction List</h2>
                    </div>
                </div>

                <!-- <div class="col-6">
                    <div class="main__title">
                        <a href="<?php echo base_url() ?>add_member" class="main__title-link w-50">Add Member</a>
                    </div>
                </div> -->
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="table-responsive">
                                <table class="table table-sm mb-0 text-center transcationtable">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Full Name</th>
                                            <th>Mobile Number</th>
                                            <th>Member Code</th>
                                            <th>Added NU Coins</th>
                                            <th>Total NU Coins</th>
                                            <th>Remark</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($all_nutransactions as $all_nutransactions) { 
                                            ?>

                                           

                                            <tr>
                                                <td>
                                                    <div class="main__table-text"><?php echo $i++; ?></div>
                                                </td>
                                                <td>
                                                    <div class="main__table-text"><?php echo $all_nutransactions['fullname']; ?></div>
                                                </td>
                                                <td>
                                                    <div class="main__table-text"><?php echo $all_nutransactions['mobilenumber']; ?></div>
                                                </td>
                                                <td>
                                                    <div class="main__table-text"><?php echo $all_nutransactions['member_code']; ?></div>
                                                </td>
                                                <td>
                                                    <div class="main__table-text"><?php echo $all_nutransactions['nucoins']; ?></div>
                                                </td>
                                                <td>
                                                    <div class="main__table-text"><?php echo $all_nutransactions['nu_coins']; ?></div>
                                                </td>
                                                <td class="text-center">
                                                    <div class="main__table-btns text-center">
                                                    <a href="javascript:void(0);" id="get_message"
                                                    class="get_message main__table-btn main__table-btn--view"
                                                    data-id="<?php echo $all_nutransactions['nu_trans_id']; ?>"
                                                    style="font-size: medium;"><i class="las la-eye "
                                                        aria-hidden="true"></i></a>
                                                    </div>
                                                </td>
                        
                                               
                                               
                                            </tr>

                                        <?php  } 

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modaldelete" id="modaldelete">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body p-4">
            <div class="row">
               <div class="col-12 pb-1">
                  <div class="text-center">
                     <h4>Delete Member Details</h4>
                     <p>Are you sure to permanently delete this Member Details</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-6">
                  <div class="main__title">
                     <a href="#" class="main__title-link w-100" id="delete_member">Delete</a>
                  </div>
               </div>
               <div class="col-6">
                  <div class="main__title">
                     <a href="#" class="main__title-link w-100" data-dismiss="modal" id="close">Close</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade text-left show" id="msgshowmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel8"
    style="padding-right: 17px;" aria-modal="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary white">
                <h4 class="modal-title white" id="myModalLabel8">Message</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="remark" name="remark"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-outline-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>