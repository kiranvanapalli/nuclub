<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Subadmin extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
        $this->load->model("general_model", "general");
        $this->load->library('my_file_upload');
    }

    public function index()
    {
        $where = ['role_id' => 1];
        $data['file'] = 'subadmin/list';
        $data['custom_js'] = 'subadmin/all_files_js';
        // $data['validation_js'] = 'admin/all_common_js/frontend_validation_admin';
        $type = "array";
        $all_users = $this->Allfiles_model->GetDataAll("users", $where, $type, 'user_id', $limit = '');
        $data['all_users'] = $all_users;
        $this->load->view('admin_template/main', $data);
    }

    public function add_admin_view()
    {
        $data['file'] = 'subadmin/add_member';
        $where = ["status" => 1];
        $type = "array";
        $data['states_list'] = $this->Allfiles_model->GetDataAllmodels("tb_states", $where, $type, 'state_name', $limit = '');
        $data['custom_js'] = 'subadmin/all_files_js';
        $this->load->view('admin_template/main', $data);
    }


    public function saveAdmin()
    {
        $res = [];
        $def_password = 123456;
        $password = base64_encode(base64_encode($def_password));
        $mobile_number = $_POST['mobile_number'];
        $email = $_POST['email'];

        $errors = array();
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|trim|numeric|min_length[10]|max_length[10]|is_unique[users.user_phone]', array('is_unique' => 'This %s already exists'));
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[users.user_email]', array('is_unique' => 'This %s already exists'));
        if ($this->form_validation->run() == FALSE) {

            $errors['errors'] = $this->form_validation->error_array();
        } else {

            $res = array();
            if (empty($errors)) {
                $data = array(
                    'user_name' => $this->input->post("name"),
                    'user_email' => $this->input->post("email"),
                    'user_phone' => $this->input->post("mobile_number"),
                    'user_pass' => $password,
                    "role_id" => $this->input->post("user_type"),
                    'created_on' => date('Y-m-d H:i:s'),
                    'updated_on' => date('Y-m-d H:i:s'),
                    'status' => 1,
                );
                $result = $this->Allfiles_model->data_save("users", $data);
            } else {
                $errors[] = "Admin Data Not Saved";
            }
        }
        if (isset($errors) && !empty($errors)) {
            // $errors['status'] = FALSE;
            // $errors['message'] = $errors;
            $res = array(
                "status" => FALSE,
                "errors" => $errors
            );
        } else {
            $res = array(
                "status" => TRUE,
                "message" => "Admin Added Successfully",
                "Data" => $result
            );
        }
        echo json_encode($res);
    }
    public function edit_admin()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $get_member_details = $this->Allfiles_model->get_data('users', '*', 'user_id', $id);
            $data['get_member_details'] = $get_member_details['resultSet'];
            $data['file'] = 'subadmin/edit_member';
            $data['custom_js'] = 'subadmin/all_files_js';
            $this->load->view('admin_template/main', $data);
        }
    }

    public function update_subadmin()
    {
        if (!empty($_POST['edit_id'])) {


            // $sql = "SELECT * FROM `tb_users` WHERE mobile_number = '$value' OR email = '$value' OR nucode = '$value'";
            // $res = $this->general->custom_query($sql, 'row');
            $response = [];
            $result = $this->db->where('user_id !=', $_POST['edit_id'])
                ->where('user_email', $_POST['email'])
                ->or_where('user_phone', $_POST['mobile_number'])
                ->where('user_id !=', $_POST['edit_id'])
                ->get('users')
                ->row_array();
            if ($result != "") {
                $response = array(
                    'status' => 'fail',
                    'message' => 'Mobile Number or email already existed'

                );
            } 
            else {

                $where = ['user_id' => $_POST['edit_id']];
                $data = array(
                    'user_name' => $this->input->post("name"),
                    'user_email' => $this->input->post("email"),
                    'user_phone' => $this->input->post("mobile_number"),
                    'role_id' => $this->input->post("user_type"),
                    'updated_on' => date('Y-m-d H:i:s'),
                    'status' => $this->input->post('status'),
                );

                $update_member_details = $this->Allfiles_model->updateData("users", $data, $where);
                if ($update_member_details) {
                    $response = ['status' => 'success'];
                    $response = array(
                        'status' => 'success',
                        'message' => 'Admin Details Updated'

                    );
                } else {
                    $response = array(
                        'status' => 'fail',
                        'message' => 'Data Not Updated'

                    );
                }
            }
            echo json_encode($response);
        }
    }

    public function delete_member()
    {


        $where = ['member_id' => $_POST['member_id']];
        $result = $this->Allfiles_model->deleteData("tb_members", $where);
        // echo $this->db->last_query();
        echo $result;
    }
}
