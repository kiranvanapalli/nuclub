	<!--START PAGE HEADER -->
	<header class="page-header">
		<div class="d-flex align-items-center">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<h1>Sub Admin</h1>
					</div>
					<div class="col-lg-4"><a href="<?= base_url() ?>add_admin" class="btn btn-primary btn-rounded float-right m-r-20">
							Add Sub Admin</a></div>
				</div>
			</div>
		</div>
	</header>
	<!--END PAGE HEADER -->
	<!--START PAGE CONTENT -->
	<section class="page-content container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body p-20">
						<table id="bs4-table" class="table table-striped table-bordered text-center subadmintable" style="width:100%">
							<thead>
								<tr>
									<th>S.no</th>
									<th>Name</th>
									<th>Mobile No.</th>
									<th>Email ID</th>
									<th>Role</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $i = 1;
								foreach ($all_users as $all_users) { ?>
									<tr>
										<td><?= $i++; ?></td>
										<td><?= $all_users['user_name']; ?></td>
										<td><?= $all_users['user_phone']; ?></td>
										<td><?= $all_users['user_email']; ?></td>
										<td>Admin</td>
										<td class="text-center">
											<?php if ($all_users['status'] == 1) {
												$status = "Active";
												$class = "badge-primary";
											} else {
												$status = "In Active";
												$class = "badge-danger";
											}
											echo '<span class="badge ' . $class . ' badge-sm">' . $status . '</span>';
											?>
										</td>
										<td class="text-center action">
											<div class="m-0">
												<div class="icon col-xs-6 col-md-3"><a href="<?= base_url() ?>edit_admin?id=<?= $all_users['user_id']; ?>" class="tile-primary"><i class="zmdi zmdi-edit zmdi-hc-fw zmdi-hc-lg"></i></div>
												<!-- <div class="icon col-md-3" data-name="delete" data-code="f154"><a href="#" id="sweetalert_demo_8"><i class="zmdi zmdi-delete zmdi-hc-fw zmdi-hc-lg"></i></a></div> -->
											</div>
										</td>
									</tr>
								<?php	}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--END PAGE CONTENT -->