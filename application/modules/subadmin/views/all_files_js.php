<script>
   
    $(document).ready(function() {
        var tran_table = $('.subadmintable').DataTable({});
       
        $(document).on('submit', '#admin_form', function(event) {
            event.preventDefault();
            var name = $('#name').val();
            var email = $('#email').val();
            var mobile_number = $('#mobile_number').val();
            var user_type = $('#user_type').val();
            if (name == '') {
                toastr["error"]("Full Name is required!");
                return false;
            }
            if (email == '') {
                toastr["error"]("Email Id is required!");
                return false;
            }
            if (mobile_number == '') {
                toastr["error"]("Please Enter Mobile Number!");
                return false;
            }
           
            if (user_type == '') {
                toastr["error"]("Please Enter City Name");
                return false;
            }
        
           
            $.ajax({
                url: "<?php echo base_url() ?>saveAdmin",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    console.log(data.status);
                    if (data.status == true) {
                        toastr["success"]("Admin Details Added Successfully!");
                        window.location.href = "<?php echo base_url(); ?>subadmin";
                    }
                    else if(data.status == false)
                    {   
                        let error_data = data.errors['errors'];
                        console.log(error_data);
                        // console.log(error_data.length);
                       if(error_data['mobile_number'] != '')
                       {
                        toastr["error"](error_data['mobile_number']);
                        return false;
                       }
                       if(error_data['email'] != '')
                       {
                        toastr["error"](error_data['email']);
                        return false;
                       }
                        
                    }
                }
            });
        });
        $(document).on('submit', '#update_subadmin', function(event) {
            event.preventDefault();
            var name = $('#name').val();
            var email = $('#email').val();
            var mobile_number = $('#mobile_number').val();
            var user_type = $('#user_type').val();
            if (name == '') {
                toastr["error"]("Full Name is required!");
                return false;
            }
            if (email == '') {
                toastr["error"]("Email Id is required!");
                return false;
            }
            if (mobile_number == '') {
                toastr["error"]("Please Enter Mobile Number!");
                return false;
            }
            if (user_type == '') {
                toastr["error"]("Please Select User Type");
                return false;
            }
            
            if($('#status').val() == '')
            {
                toastr["error"]("Please Select Status");
                return false;
            }
             $.ajax({
             url:"<?php echo base_url() ?>update_subadmin",
             method:'POST',
             data:new FormData(this),
             contentType:false,
             processData:false,
             dataType:'JSON',
             success:function(data)
             {
              console.log(data);
                if(data.status == 'success')
                {
                    toastr["success"]("Admin Details Updated Successfully!");
                    window.location.href = "<?php echo base_url(); ?>subadmin";
                }
                else
                {
                      toastr["error"](data.message);
                      return false;
                }
             }
        });
    });
    $(document).on('click', '.delete', function(event) {
            event.preventDefault();
            var id = $(this).attr('id');
            $('.modaldelete').attr('value',id)
    })
    $(document).on('click', '.modaldelete', function(event) {
            event.preventDefault();
            var id = $(this).attr('value');
            console.log(id);
            $.ajax({
                url: "<?php echo base_url() ?>delete_member",
                method: "POST",
                data: {
                    member_id: id,
                    <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                },
                success: function(data) {
                    if (data) {
                        toastr["success"]("Member Deleted Successfully");
                        window.location.href = "<?php echo base_url(); ?>members";
                    } else {
                        toastr["error"]("Delete failed! Please try again.");
                        return false;
                    }
                }
            })
        });
        $(document).on('click', '#close', function(event) {
            event.preventDefault();
            asset_table.ajax.reload();
        });

    $('#sweetalert_demo_8').on('click', function(e) {
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
        }
      })
    });
    });
</script>