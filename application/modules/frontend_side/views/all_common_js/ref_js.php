<script>
     function showPassword() {
        var x = document.getElementById("password");
        var y = document.getElementById("confirm_password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
        if (y.type === "password") {
            y.type = "text";
        } else {
            y.type = "password";
        }
    }
     $(document).ready(function() {
     $("#date").datepicker({
            format: 'dd/mm/yyyy',
            endDate: '-3d',
            autoclose: true //to close picker once year is selected
        });
    $(document).on('submit', '#ref_form', function(e) {
        e.preventDefault();
        var fullname = $('#fullname').val();
        var mobilenumber = $('#mobilenumber').val();
        var email = $('#email').val();
        var date = $('#date').val();
        var gender = $('#gender').val();
        var state = $('#state').val();
        var city = $('#city').val();
        var password = $('#password').val();
        var confirm_password = $('#confirm_password').val();


        var checked = false;

        if (fullname == '') {
                toastr["error"]("Full Name is required!");
                return false;
                checked = true;
            }
            if (mobilenumber == '') {
                toastr["error"]("Please Enter Mobile Number!");
                return false;
                checked = true;
            }
            if (mobilenumber.length < 10) {
                toastr["error"]("Mobile Number Must Contain 10 Digits");
                return false;
                checked = true;
            }
            if (email == '') {
                toastr["error"]("Email Id is required!");
                return false;
                checked = true;
            }
            if (gender == '') {
                toastr["error"]("Please Select Gender");
                return false;
                checked = true;
            }
            if (date == '') {
                toastr["error"]("Please Select Date Of Birth");
                return false;
                checked = true;
            }
            if (state == '') {
                toastr["error"]("Please Select State");
                return false;
                checked = true;
            }
            if (city == '') {
                toastr["error"]("Please Enter City Name");
                return false;
                checked = true;
            }
            if (password == '') {
                toastr["error"]("Please Enter Password");
                return false;
                checked = true;
            }
            if (confirm_password == '') {
                toastr["error"]("Please Enter Confirm password");
                return false;
                checked = true;
            }
            if (password.length <= 6) {
                toastr["error"]("Password Should Be At Least 6 Letters");
                return false;
                checked = true;

            }
            if (password.length > 20) {
                toastr["error"]("Password Should Be Lessthen 20 Letters");
                return false;
                checked = true;

            }
            if (password.search(/[a-z]/i) < 0) {
                toastr["error"]("Password must contain at least one letter.");
                return false;
                checked = true;

            }
            if (password.search(/[A-Z]/i) < 0) {
                toastr["error"]("Password must contain at least one letter.");
                return false;
                checked = true;

            }
            if (password.search(/[0-9]/) < 0) {
                toastr["error"]("Password must contain at least one digit.");
                return false;
                checked = true;
            }
            if (password.search(/[!#$%&?@]/) < 0) {
                toastr["error"]("Password must contain at least one special character.");
                return false;
                checked = true;
            }
            if (confirm_password.length <= 6) {
                toastr["error"]("Confirm Password Should Be At Least 6 Letters");
                return false;
                checked = true;
            }
            if (confirm_password.length > 20) {
                toastr["error"]("Confirm Password Should Be Lessthen 20 Letters");
                return false;
                checked = true;
            }
            if (confirm_password.search(/[a-z]/i) < 0) {
                toastr["error"]("Confirm must contain at least one letter.");
                return false;
                checked = true;
            }
            if (confirm_password.search(/[A-Z]/i) < 0) {
                toastr["error"]("Confirm must contain at least one letter.");
                return false;
                checked = true;
            }
            if (confirm_password.search(/[0-9]/) < 0) {
                toastr["error"]("Confirm Password must contain at least one digit.");
                return false;
                checked = true;
            }
            if (confirm_password.search(/[!#$%&?@]/) < 0) {
                toastr["error"]("Confirm Password must contain at least one special character.");
                return false;
                checked = true;
            }
            if (password != confirm_password) {
                toastr["error"]("Please Password & Confirm Password Not Match");
                return false;
                checked = true;

            }
        if(checked == false) {
        $.ajax({
                url: "<?php echo base_url() ?>saveUser",
        		data:$(this).serialize(),
        		type:"POST",
        		dataType:"JSON",
        		success: function(res) {
        			console.log(res);
        			if(res.status == "failure") {
        			     toastr["error"](res.message);
        			} 
                    else {
        			      var url = "<?php echo base_url(); ?>"+res.redirect_url;
                          toastr["success"](res.message);
                        window.setTimeout(function() {
                            window.location.href = url;
                        },3000);
        			}
        		}
        	});
        }
    });
});
</script>