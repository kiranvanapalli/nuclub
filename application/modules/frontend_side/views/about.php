
<style>
.text-justify {
    text-align: justify;
}
</style>


<body>
    <!--Start Preloader-->
    <div class="preloader">
        <div class="d-table">
            <div class="d-table-cell align-middle">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
    </div>
    <!--End Preloader-->
<section class="hero-section ptb-100 gradient-overlay" style="background: url('img/header-bg-5.jpg')no-repeat center center / cover">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-7">
                    <div class="page-header-content text-white text-center pt-sm-5 pt-md-5">
                        <h1 class="text-white mb-0">About Us</h1>
                        <div class="custom-breadcrumb">
                            <ol class="breadcrumb d-inline-block bg-transparent list-inline py-0">
                                <li class="list-inline-item breadcrumb-item"><a class="text-white" href="#">Home</a></li>
                                <li class="list-inline-item breadcrumb-item">About Us</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	
	<section class="promo-section pt-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-4">
                    <div class="promo-single-wrap p-5 text-center border rounded">
                        <div class="promo-icon mb-4">
                            <img src="user_assets/new-assets/images/search-engine.svg" alt="promo" width="65">
                            <span class="number-bg">01</span>
                        </div>
                        <div class="promo-info">
                            <strong class="color-secondary">Discuss with Users</strong>
                            <h4>E-Commerce</h4>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="promo-single-wrap p-5 text-center border rounded">
                        <div class="promo-icon mb-4">
                            <img src="user_assets/new-assets/images/bullhorn.svg" alt="promo" width="65">
                            <span class="number-bg">02</span>
                        </div>
                        <div class="promo-info">
                            <strong class="color-secondary">Discuss with Users</strong>
                            <h4>Digital Marketing</h4>
                           
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="promo-single-wrap p-5 text-center border rounded">
                        <div class="promo-icon mb-4">
                            <img src="user_assets/new-assets/images/increase.svg" alt="promo" width="65">
                            <span class="number-bg">03</span>
                        </div>
                        <div class="promo-info">
                            <strong class="color-secondary">Discuss with Users</strong>
                            <h4>Affiliated Market</h4>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	
	<section class="about-us-section ptb-100">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-md-12 col-lg-5">
                    <div class="about-us-content-wrap">
                        <strong class="color-secondary">About Us</strong>
                        <p><strong>NU CLUB</strong> is a unique platform with an E-Business framework dealing with both E-Business &amp; E-Commerce were specially designed for managing start-ups, small business projects and supporting modern companies.</p>
                        <span class="animate-border mb-4"></span>
						<h3>NU CLUB E-Business Modules :</h3>
                        <ul class="list-unstyled tech-feature-list">
							
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                E-Commerce 
                            </li>
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                Digital Marketing
                            </li>
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                Affiliate Marketing
                            </li>
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                Crypto Marketing
                            </li>
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                Social Media Marketing
                            </li>
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                E-mail Marketing
                            </li>
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                Brand Marketing
                            </li>
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                Product Marketing
                            </li>
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                Digital Network Marketing 
                            </li>
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                Network Marketing
                            </li>
                            <li class="py-1"><span class="fa-angle-double-right fas mr-2 color-secondary"></span>
                                Influencer Marketing
                            </li>
							<p>And Many More <strong>Coming Soon...</strong></p>
                            
                        </ul>
<!--
                        <div class="row mt-4">
                            <div class="col-4">
                                <div class="counter-single">
                                    <h2 class="mb-0 color-secondary">45</h2>
                                    <strong>Total Product</strong>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="counter-single">
                                    <h2 class="mb-0 color-secondary">502</h2>
                                    <strong>Total Referrals</strong>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="counter-single">
                                    <h2 class="mb-0 color-secondary">66</h2>
                                    <strong>Total Earls</strong>
                                </div>
                            </div>
                        </div>
-->
                        <div class="action-btns mt-4">
                            <a href="<?php echo base_url() ?>register" class="btn secondary-solid-btn mr-3">Start Now</a>
<!--                            <a href="#" class="btn secondary-outline-btn">Learn More</a>-->
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="card border-0 text-white">
                        <img src="user_assets/new-assets/images/why-choose-us.png" alt="video" class="img-fluid">
                        <div class="card-img-overlay text-center">
                            <a href="https://www.youtube.com/watch?v=9No-FiEInLA" class="popup-youtube video-play-icon color-bip shadow"><span class="fa-play fas"></span> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	


   