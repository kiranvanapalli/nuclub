<section class="hero-section ptb-100 gradient-overlay" style="background: url('img/header-bg-5.jpg')no-repeat center center / cover">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-7">
                    <div class="page-header-content text-white text-center pt-sm-5 pt-md-5">
                        <h1 class="text-white mb-0">Services</h1>
                        <div class="custom-breadcrumb">
                            <ol class="breadcrumb d-inline-block bg-transparent list-inline py-0">
                                <li class="list-inline-item breadcrumb-item"><a class="text-white" href="#">Home</a></li>
                                <li class="list-inline-item breadcrumb-item">Services</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	
	<section class="services-section ptb-100 gray-light-bg">
    <div class="container">
<!--
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="section-heading text-center mb-4">
                        <strong class="color-secondary">Our Services</strong>
                        <h2>First Class Business Solutions for You</h2>
                        <span class="animate-border mr-auto ml-auto mb-4"></span>
                        <p class="">Efficiently aggregate end-to-end core competencies without maintainable ideas. Dynamically
                            foster tactical solutions without enabled value.</p>
                    </div>
                </div>
            </div>
-->
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/E-Commerce.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">E-Commerce </h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
				<div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/DigitalMarketing.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">Digital Marketing</h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
				<div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/AffiliateMarketing.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">Affiliate Marketing</h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/CryptoMarketing.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">Crypto Marketing</h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
				<div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/SocialMediaMarketing.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">Social Media Marketing</h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
				<div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/E-mailMarketing.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">E-mail Marketing</h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/BrandMarketing.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">Brand Marketing</h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
				<div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/ProductMarketing.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">Product Marketing</h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
				<div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/DigitalNetworkMarketing.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">Digital Network Marketing</h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
            </div>
			<div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/NetworkMarketing.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">Network Marketing</h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
				<div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="my-lg-3 my-md-3 my-sm-0 rounded services-single shadow-sm white-bg">
                        <span class="ti-announcement icon-lg color-primary d-block mb-4">
							<img src="user_assets/new-assets/images/Services/InfluencerMarketing.jpg" alt="" class="img-fluid">
						</span>
                        <h5 class="pl-4 pr-4">Influencer Marketing	</h5>
                        <p class="mb-0 pl-4 pr-4">-------</p>
<!--                        <a href="services-details.html" target="_blank" class="detail-link mt-4 pb-4 pl-4 pr-4">Read more <span class="ti-arrow-right"></span></a>-->
                    </div>
                </div>
				
            </div>
			
        </div>
    </section>

  