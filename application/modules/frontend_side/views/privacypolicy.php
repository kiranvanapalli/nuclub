<section class="hero-section ptb-100 gradient-overlay" style="background: url('img/header-bg-5.jpg')no-repeat center center / cover">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-7">
                <div class="page-header-content text-white text-center pt-sm-5 pt-md-5">
                    <h1 class="text-white mb-0">Privacy Policy</h1>
                    <div class="custom-breadcrumb">
                        <ol class="breadcrumb d-inline-block bg-transparent list-inline py-0">
                            <li class="list-inline-item breadcrumb-item"><a class="text-white" href="<?= base_url() ?>">Home</a></li>
                            <li class="list-inline-item breadcrumb-item">Privacy Policy</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="promo-section pt-100">
    <div class="container">
        <div class="row justify-content-center">

            <p>NU CLUB is a unique platform with an E-Business framework dealing with both E-Business & E-Commerce were specially designed for managing start-ups, small business projects and supporting modern companies.</p>
            <p>
                NU Club is designed for those who love to user interface. You will love the seamless way we display the user inter face on your devices. As businesses rely on software or app to engage customers, innovation and velocity becomes core to delivering value. <br><br>
                But you’re held back by antiquated technology and legacy development processes. Our customers use our app, to adopt next-generation development practices, deliver new applications, and modernize existing applications.
            </p>

        </div>
    </div>
</section>