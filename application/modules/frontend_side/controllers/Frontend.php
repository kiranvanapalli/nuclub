<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Frontend extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Allfiles_model');
        $this->load->model('admin/Admin_model');
    }
    public function index()
    {
        $this->load->view('frontend_side/index');
    }
    public function nuclub_view()
    {
        $data['file'] = 'frontend_side/nuclub';
        $data['custom_js'] = 'frontend_side/custom_js';
        $this->load->view('user_template/main', $data);
    }

    public function contactus()
    {

        $data['file'] = 'frontend_side/contact';
        $data['custom_js'] = 'frontend_side/custom_js';
        $this->load->view('user_template/main', $data);
    }


    public function events()
    {

        $data['file'] = 'frontend_side/events';
        $data['custom_js'] = 'frontend_side/custom_js';
        $this->load->view('user_template/main', $data);
    }

    public function services()
    {

        $data['file'] = 'frontend_side/services';
        $data['custom_js'] = 'frontend_side/custom_js';
        $this->load->view('user_template/main', $data);
    }

    public function service_details()
    {

        $data['file'] = 'frontend_side/servicedetails';
        $data['custom_js'] = 'frontend_side/custom_js';
        $this->load->view('user_template/main', $data);
    }
    public function joinus()
    {
        $data['file'] = 'frontend_side/joinus';
        $data['custom_js'] = 'frontend_side/custom_js';
        $this->load->view('user_template/main', $data);
    }

    public function savejoinus()
    {
        $data = array(
            'full_name' => $this->input->post('full_name'),
            'email' => $this->input->post('email'),
            'mobile_number' => $this->input->post('mobile_number'),

        );

        $result = $this->Allfiles_model->data_save('tb_join_us', $data);

        echo json_encode($result);
    }

    public function savecontact()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'mobile_no' => $this->input->post('mobile_no'),
            'message' => $this->input->post('message'),

        );

        $result = $this->Allfiles_model->data_save('tb_contact', $data);

        echo json_encode($result);
    }

    public function forgotpasswordmail()
    {
        $email = $this->input->post('email');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        if ($this->form_validation->run() == false) {

            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        } else {
            $whr = ['email' => $email, 'status' => 1];
            $userDetails = $this->Allfiles_model->getCustomerDetails("tb_members", $whr)->row_array();

            $decoded = base64_decode(base64_decode($userDetails['password']));

            if ($userDetails['email'] == $email) {
                $username = $userDetails['fullname'];

                // $password = $userDetails['password'];

                $decoded = base64_decode(base64_decode($userDetails['password']));
                // Load PHPMailer library
                $this->load->library('phpmailer_lib');

                // PHPMailer object
                $mail = $this->phpmailer_lib->load();

                // SMTP configuration
                $mail->isSMTP();
                $mail->Host = 'sg2plmcpnl485224.prod.sin2.secureserver.net';
                $mail->SMTPDebug = 1;
                $mail->SMTPAuth = true;
                $mail->Username = 'info@nuclubindia.com';
                $mail->Password = 'Adnectics@1222';
                $mail->SMTPSecure = 'ssl';
                $mail->Port = 465;

                $mail->setFrom('info@nuclubindia.com', 'NU CLUB');
                $mail->addReplyTo('info@nuclubindia.com', 'NU CLUB');

                // Add a recipient
                $mail->addAddress($email);

                // Email subject
                $mail->Subject = 'Forgot Password Notification';

                // Set email format to HTML
                $mail->isHTML(true);

                // Email body content
                $mailContent = "Dear $username,<br/>
                    Please find the below password here.<br/>";
                $mailContent .= "Your Password is: <strong>" . $$decoded . "</strong>";
                $mail->Body = $mailContent;

                // Send email
                if (!$mail->send()) {
                    echo 'Message could not be sent.';
                    echo 'Mailer Error: ' . $mail->ErrorInfo;
                } else {
                    $this->session->set_flashdata('success_msg', "Password Sent Succesfully!");
                    redirect(base_url());
                }
            } else {

                $this->session->set_flashdata('error_msg', "Email Not Valid");
                redirect('user_forgotpassword');
            }
        }
    }
    public function userLogin()
    {
        $data['page_title'] = "Login";

        if ($this->session->set_userdata('user_login_is')) {
            redirect('member_dashboard');
        }

        $data['file'] = 'frontend_side/login';
        $data['validation_js'] = 'frontend_side/all_common_js/login_js';
        $this->load->view('user_template/main', $data);
    }

    public function CheckUser()
    {
        $response = array();
        $email = $this->input->post('email');
        $email_where = [$email];
        $password = $this->input->post('password');
        $decode = base64_encode(base64_encode($password));
        $password_where = ['password' => $decode];
        $whr = ['email' => $email, 'password' => $decode];
        // $getdata = $this->Allfiles_model->checkuserdata('tb_members', $email_where, $password_where);
        // $getdata = $this->Allfiles_model->checkUserLogin('tb_members',$whr);
        $getdata = $this->Allfiles_model->checkUserLogin_data('tb_members', $whr);
       
       
        // $get_member_id = $getdata['member_id'];
        // $whr_data = ['email' => $get_member_id];
        // $userDetails = $this->Allfiles_model->getCustomerDetails("tb_referrals", $whr_data)->row_array();

        //  echo $this->db->last_query();die();
        //    print_r($getdata);die();
        if ($getdata) {
            $whr_data = ['member_saved_id' => $getdata['member_id']];
            $getref_data = $this->Allfiles_model->getCustomerDetails('tb_referrals', $whr_data)->row_array();
            $user_status = $getdata['status'];
            if ($user_status == 1) {
                if (count($getdata) > 0) {
                    $user_session_data = array(
                        'email' => $getdata['email'],
                        'member_id' => $getdata['member_id'],
                        'city' => $getdata['city'],
                        'fullname' => $getdata['fullname'],
                        'mobilenumber' => $getdata['mobilenumber'],
                        'member_code' => $getdata['member_code'],
                        'points' => $getdata['points'],
                        'user_login_is' => true,
                    );
                    $this->session->set_userdata($user_session_data);
                    $response = ['status' => 'success', 'redirect_url' => "Wallet", 'message' => "Login Sucess"];
                } else {
                    $message = "Invalid Email/Password";
                    $response = ['status' => 'failure', 'message' => $message];
                }
            } else {
                $this->session->set_userdata('user_logged', 1);
                $this->session->set_userdata('fullname', $getdata['fullname']);
                $this->session->set_userdata('member_id', $getdata['member_id']);
                if($getref_data)
                {

                    $this->session->set_userdata('member_inserted_id_after_ref',$getref_data['member_saved_id']);

                }
                $response = ['status' => 'success', 'redirect_url' => "payment", 'message' => "Payment Page"];
            }
        } else {
            $response = ['status' => 'failure', 'message' => "Email Or Mobile Number Not Found"];
        }

        echo json_encode($response);
    }


    public function ref_page()
    {

        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $where = ['status' => 1];
            $type = "array";
            $decode = base64_decode(base64_decode($id));
            $this->session->set_userdata('ref_id', $decode);
            $data['states_list'] = $this->Allfiles_model->GetDataAllmodels("tb_states", $where, $type, 'state_name', $limit = '');
            $get_member_details = $this->Allfiles_model->get_data('tb_referrals', '*', 'ref_id', $decode);
            $data['get_member_details'] = $get_member_details['resultSet'];
            // $data['nucode'] = $decode;
            $data['file'] = 'frontend_side/ref_page';
            $data['validation_js'] = 'frontend_side/all_common_js/frontend_validation_admin';
            $data['custom_js'] = 'frontend_side/all_common_js/ref_js';
            $this->load->view('user_template/main', $data);
        } else {
            redirect('');
        }
    }



    public function is_logged_in()
    {
        $is_logged_in = $this->session->userdata('user_login_is');
        if (!isset($is_logged_in) || $is_logged_in != true) {
            redirect(base_url('user_login'));
        }
    }
    public function user_register()
    {
        $data['file'] = 'frontend_side/register';
        $where = ['status' => 1];
        $type = "array";
        $data['states_list'] = $this->Allfiles_model->GetDataAllmodels("tb_states", $where, $type, 'state_name', $limit = '');
        $data['validation_js'] = 'frontend_side/all_common_js/frontend_validation_admin';
        $data['custom_js'] = 'frontend_side/all_common_js/register';
        $this->load->view('user_template/main', $data);
    }
    public function saveUser()
    {
        $response = array();
        $insert_id = "";
        $email = $this->input->post('email');
        $mobile_number = $this->input->post('mobilenumber');
        $password = base64_encode(base64_encode($this->input->post('password')));
        $getdata = $this->Allfiles_model->checkuser('tb_members', $email, $mobile_number);
        $ref_id_data = $this->session->userdata('ref_id');
        

        if (sizeof($getdata) == 0) {
            $ref_code = $this->input->post('ref_code');
            $chec_ref = $this->input->post('chec_ref');

            if ($chec_ref == 1) {
                $get_userdata = $this->Allfiles_model->checkref('tb_members', $ref_code);
                if (!empty($get_userdata)) {
                    $data = array(
                        'fullname' => $this->input->post('fullname'),
                        'email' => $this->input->post('email'),
                        'mobilenumber' => $this->input->post('mobilenumber'),
                        'gender' => $this->input->post('gender'),
                        'date_of_birth' => $this->input->post('date'),
                        'state' => $this->input->post('state'),
                        'city' => $this->input->post('city'),
                        'password' => $password,
                        'status' => 0,
                        'points' => 0,
                    );

                    $save_user = $this->Allfiles_model->data_save('tb_members', $data);


                    $insert_id = $this->db->insert_id();

                    if ($save_user) {
                        $get_member_id = $get_userdata['member_id'];
                        $get_nucode = $get_userdata['member_code'];
                        $code = mt_rand(11111, 99999999);
                        $tr_id = "NRF" . $code;
                        $data = array(
                            'tr_id' => $tr_id,
                            'member_id' => $get_member_id,
                            'member_nucode' => $get_nucode,
                            'member_saved_id' => $insert_id,
                            'fullname' => $this->input->post('fullname'),
                            'email' => $this->input->post('email'),
                            'mobilenumber' => $this->input->post('mobilenumber'),
                            'points' => 0,
                            'status' => 2
                        );
                        $result = $this->Allfiles_model->data_save('tb_referrals', $data);
                        $insert_id = $this->db->insert_id();

                        $whr = ['ref_id' => $insert_id];
                        $userDetails = $this->Allfiles_model->getCustomerDetails("tb_referrals", $whr)->row_array();
                      
                        //After Added in Ref Table Save in session 2. ref_id and 1.ref_member_id

                        // $this->session->set_userdata('ref_member_id', $userDetails['member_id']);
                        // $this->session->set_userdata('ref_id', $userDetails['ref_id']);
                    }
                    if ($result) {
                        $whr = ['member_id' => $insert_id];
                        $userDetails = $this->Allfiles_model->getCustomerDetails("tb_members", $whr)->row_array();
                        $this->session->set_userdata('user_logged', 1);
                        $this->session->set_userdata('fullname', $userDetails['fullname']);
                        $this->session->set_userdata('member_id', $userDetails['member_id']);
                        $this->session->set_userdata('ref_table_id', $userDetails['ref_id']);
                        
                        $response = ['status' => 'success', 'redirect_url' => "payment", 'message' => "Member Added"];
                    } else {
                        $response = ['status' => 'failure', 'message' => "Referral code Not Found"];
                    }
                } else {
                    $response = ['status' => 'failure', 'message' => "Referral code Not Found"];
                }
            } else if ($ref_id_data != '') {
                $data = array(
                    'fullname' => $this->input->post('fullname'),
                    'email' => $this->input->post('email'),
                    'mobilenumber' => $this->input->post('mobilenumber'),
                    'gender' => $this->input->post('gender'),
                    'date_of_birth' => $this->input->post('date'),
                    'state' => $this->input->post('state'),
                    'city' => $this->input->post('city'),
                    'password' => $password,
                    'status' => 0,
                    'points' => 0,
                );

                $save_user = $this->Allfiles_model->data_save('tb_members', $data);

                $insert_id = $this->db->insert_id();

                $whr = ['member_id' => $insert_id];

                $ref_id = ['ref_id' => $ref_id_data];
                $save_ref = array(
                    'status' => 0,

                );
                if ($save_user) {

                    $this->Allfiles_model->update('tb_referrals', $save_ref, $ref_id);

                    $userDetails = $this->Allfiles_model->getCustomerDetails("tb_members", $whr)->row_array();

                    $this->session->set_userdata('user_logged', 1);
                    $this->session->set_userdata('fullname', $userDetails['fullname']);
                    $this->session->set_userdata('member_id', $userDetails['member_id']);
                    $this->session->set_userdata('ref_member_id',$ref_id_data);
                    $response = ['status' => 'success', 'redirect_url' => "payment", 'message' => "Added Reffer Code"];
                } else {
                    $response = ['status' => 'failure', 'message' => "Data Not Saved"];
                }
            } else {

                $data = array(
                    'fullname' => $this->input->post('fullname'),
                    'email' => $this->input->post('email'),
                    'mobilenumber' => $this->input->post('mobilenumber'),
                    'gender' => $this->input->post('gender'),
                    'date_of_birth' => $this->input->post('date'),
                    'state' => $this->input->post('state'),
                    'city' => $this->input->post('city'),
                    'password' => $password,
                    'status' => 0,
                    'points' => 0,
                );

                $save_user = $this->Allfiles_model->data_save('tb_members', $data);
                if ($save_user) {
                    $insert_id = $this->db->insert_id();
                    $whr = ['member_id' => $insert_id];
                    $userDetails = $this->Allfiles_model->getCustomerDetails("tb_members", $whr)->row_array();
                    $this->session->set_userdata('user_logged', 1);
                    $this->session->set_userdata('fullname', $userDetails['fullname']);
                    $this->session->set_userdata('member_id', $userDetails['member_id']);
                    $response = ['status' => 'success', 'redirect_url' => "payment", 'message' => "Member Added With Out Refferal"];
                }
            }
        } else {
            $message = "Mobile Number/Email Already Existed";
            $response = ['status' => 'failure', 'message' => $message];
        }
        echo json_encode($response);
    }
    public function saveTranscation()
    {
        $transaction_type = $this->input->post('transaction_type');
        $utr_number = '';
        $transaction_id = '';
        $ref_member_data = "";
        $ref_id ="";
        $member_id = $this->session->userdata('ref_id');
        $memner_id_ref_table = $this->session->userdata('member_inserted_id_after_ref');
        $ref_data = $this->session->userdata('ref_table_id');
        if ($member_id != "") {
            echo "If Reffer Code in Link:".$member_id;
            $whr = ['ref_id' => $member_id];
            $userDetails = $this->Allfiles_model->getCustomerDetails("tb_referrals", $whr)->row_array();

           

            $ref_member_id = $userDetails['member_id'];

           
        }
        else if($memner_id_ref_table!="")
        {
            echo "If Reffer Code in Form,:".$memner_id_ref_table;
            $whr_ref = ['member_saved_id' => $memner_id_ref_table];
            $ref_details = $this->Allfiles_model->getCustomerDetails("tb_referrals", $whr_ref)->row_array();
            $ref_member_data = $ref_details['member_id'];
        }
        $member_id =  $this->input->post('member_id');
        if ($member_id == '') {
            $ref_data = 0;
        }
        else if($ref_member_data) 
        {
            $where1 = ['member_saved_id' => $member_id];
            $ref = $this->Allfiles_model->getCustomerDetails("tb_referrals", $where1)->row_array();
            $ref_id = $ref['ref_id'];
            $ref_data = $ref_member_data;
        }
        else {
            $ref_data =  $member_id;
            $where1 = ['member_id' => $member_id];
            $ref = $this->Allfiles_model->getCustomerDetails("tb_referrals", $where1)->row_array();
            echo $this->db->last_query();
            print_r($ref);
            $ref_id = $ref['ref_id'];
           
        }
        if ($transaction_type == 'utr_number') {
            $utr_number = $this->input->post('UTR_Transaction_value');
        } else if ($transaction_type == 'transaction_id') {
            $transaction_id = $this->input->post('UTR_Transaction_value');
        }

        $data = array(
            'member_id' => $member_id,
            'ref_id' =>  $ref_data,
            'ref_tb_id'=>  $ref_id,
            'transaction_type' => $transaction_type,
            'transaction_id' => $transaction_id,
            'utr_number' => $utr_number,
            'status' => 2,
        );
        $result = $this->Allfiles_model->data_save('tb_transaction', $data);
        echo json_encode($result);
    }
    public function forgotpassword()
    {
        $data['file'] = 'frontend_side/forgot_password';
        $data['custom_js'] = 'frontend_side/forgot_password_js';
        $this->load->view('user_template/main', $data);
    }

    public function about()
    {
        $data['file'] = 'frontend_side/about';
        $data['custom_js'] = 'frontend_side/custom_js';
        $this->load->view('user_template/main', $data);
    }

    public function payment_page()
    {
        $log_status =  $this->session->userdata('user_logged');
        
        if ($log_status == 1) {

            $data['file'] = 'frontend_side/payment';
            $data['custom_js'] = 'frontend_side/all_common_js/payment_js';
            $this->load->view('user_template/main', $data);
        } else {
            redirect('user_login');
        }
    }

    

    public function saveNewsLetter()
    {


        $data = array(
            'email' => $this->input->post('email_data'),
            'created_at' => date('Y-m-d H:i:s')
        );

        $result = $this->Allfiles_model->data_save('tb_news_letter', $data);

        if ($result) {
            $this->session->set_flashdata('success', 'Thank You For News Letter Subscription');
            redirect('nuclub');
        } else {
            $this->session->set_flashdata('error', 'Some Thing Went Wrong');
            redirect('nuclub');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('member_id');
        $this->session->unset_userdata('user_logged');
        $this->session->unset_userdata('fullname');
        $this->session->unset_userdata('ref_id');
        $this->session->unset_userdata('ref_member_id');
        $this->session->unset_userdata('member_inserted_id_after_ref');
        redirect(base_url(''));
    }


    public function privacypolicy()
    {
        $data['file'] = 'frontend_side/privacypolicy';
      
        $this->load->view('user_template/main', $data);
    }
}
