<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Withdraw extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
    }

    public function index()
    {
        $where = [["column" => "a.status", "value" => "2"]];

        $row_type = "array";
        $order_by =  ["column" => "a.withdraw_id", "Type" => "DESC"];
        $array = [
            "fileds" => "a.*,b.nucode as nucode,b.mobile_number as mobile_number,b.full_name as full_name",
            "table" => 'tb_withdraw_request as a',
            "join_tables" => [['table' => 'tb_users as b', 'join_on' => 'a.user_id = b.user_id', 'join_type' => 'left']],
            "where" => $where,
            "row_type" => $row_type,
            "order_by" => $order_by,
        ];

        $data['all_trascations'] = $this->Allfiles_model->GetDataFromJoin($array);
        $data['file'] = 'withdraw/withdraw_list';
        $data['custom_js'] = 'withdraw/all_files_js';
        $this->load->view('admin_template/main', $data);
    }

    public function view()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];

            $type = "array";
            $withdraw_data = $this->Allfiles_model->get_data('tb_withdraw_request', '*', 'withdraw_id', $id);
            $data['withdraw_data']   = $withdraw_data['resultSet'];
            $member_data = $withdraw_data['resultSet'];
            $user_id = $member_data['user_id'];
            $user_data = $this->Allfiles_model->get_data('tb_users', '*', 'user_id', $user_id);
            $data['user_data']   = $user_data['resultSet'];
            $data['file'] = 'withdraw/withdraw_req';
            $data['custom_js'] = 'withdraw/all_files_js';
            $this->load->view('admin_template/main', $data);
        }
    }

    public function update()
    {

        if (!empty($_POST['edit_id'])) {
            $admin_status = $this->input->post('status');
            $response = [];
            $where = ['withdraw_id' => $_POST['edit_id']];
            if ($admin_status == 1) {
                $data = array(
                    'status' => 1,
                    'comment' => $this->input->post('comment'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $result = $this->Allfiles_model->updateData('tb_withdraw_request', $data, $where);
            }

            if ($admin_status == 0) {
                $data = array(
                    'status' => 0,
                    'comment' => $this->input->post('comment'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $result = $this->Allfiles_model->updateData('tb_withdraw_request', $data, $where);

                if ($result) {
                    //Get Withdraw Data
                    $withdraw_id =  $_POST['edit_id'];
                    $withdraw_data = $this->Allfiles_model->get_data('tb_withdraw_request', '*', 'withdraw_id', $withdraw_id);
                    $get_withdraw = $withdraw_data['resultSet'];
                    $withdraw_req_account_type =  $get_withdraw['withdraw_wallet'];
                    $amount = $get_withdraw['amount'];
                    //Get User Data for update wallet
                    $user_id = $get_withdraw['user_id'];
                    $user_data = $this->Allfiles_model->get_data('tb_users', '*', 'user_id', $user_id);
                    $get_userData = $user_data['resultSet'];
                    $get_nufound = $get_userData['nufount'];
                    $get_referral_amount = $get_userData['referral_amount'];
                    $where_userdata = ['user_id' => $user_id];
                    if ($withdraw_req_account_type == 1) {
                        $update_wallet_nufound = $get_nufound + $amount;

                        $data_wallet = array(
                            'nufount' => $update_wallet_nufound,
                        );
                        $result = $this->Allfiles_model->updateData('tb_users', $data_wallet, $where_userdata);
                    }

                    if ($withdraw_req_account_type == 2) {
                        $update_wallet_ref = $get_referral_amount + $amount;
                        $data_wallet = array(
                            'referral_amount' => $update_wallet_ref,
                        );
                        $result = $this->Allfiles_model->updateData('tb_users', $data_wallet, $where_userdata);
                    }
                }
            }
        }
        echo json_encode($result);
    }

    public function withdraw_list()
    {
        $where = [["column" => "a.status!=", "value" => "2"]];

        $row_type = "array";
        $order_by =  ["column" => "a.tr_id", "Type" => "DESC"];
        $array = [
            "fileds" => "a.*,b.member_id as member_id,b.mobilenumber as mobilenumber,b.fullname as fullname",
            "table" => 'tb_withdraw as a',
            "join_tables" => [['table' => 'tb_members as b', 'join_on' => 'a.member_id = b.member_id', 'join_type' => 'left']],
            "where" => $where,
            "row_type" => $row_type,
            "order_by" => $order_by,
        ];
        $data['all_trascations'] = $this->Allfiles_model->GetDataFromJoin($array);
        $data['file'] = 'withdraw/withdraw_tras_list';
        $data['custom_js'] = 'withdraw/all_files_js';
        $this->load->view('admin_template/main', $data);
    }

    public function withdraw_list_view()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];

            $type = "array";
            $withdraw_list_data = $this->Allfiles_model->get_data('tb_withdraw', '*', 'withdraw_id', $id);
            $data['withdraw_list_data']   = $withdraw_list_data['resultSet'];
            $data['file'] = 'withdraw/withdrawtrans_list_view';
            $data['custom_js'] = 'withdraw/all_files_js';
            $this->load->view('admin_template/main', $data);
        }
    }
}
