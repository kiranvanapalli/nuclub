<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <div class="row mb-2">
                <!-- main title -->
                <div class="col-6">
                    <div class="main__title">
                        <h2>Withdrawal Transactions List</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="table-responsive">
                                <table class="table table-sm mb-0 text-center" id="total_withdraw_table">
                                    <thead>
                                        <tr>
                                            <th>Transcation Id</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Req Amount</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($all_trascations as $all_trascations){ ?>
                                        <tr>
                                            <td>
                                                <div class="main__table-text"><?php echo $all_trascations['tr_id']; ?></div>
                                            </td>
                                            <td>
                                                <div class="main__table-text"><?php echo $all_trascations['name']; ?></div>
                                            </td>
                                            <td>
                                                <div class="main__table-text"><?php echo $all_trascations['mobilenumber']; ?></div>
                                            </td>
                                          
                                            <td>
                                                <div class="main__table-text"><strong>₹ <?php echo $all_trascations['amount']; ?>/-</strong></div>
                                            </td>
                                            <td>
                                                <div class="main__table-text"><?php echo $all_trascations['updated_at']; ?></div>
                                            </td>
                                            <td>
                                                <?php if($all_trascations['status'] == 1)
                                                {
                                                    $status = "Approved";
                                                    $class = "main__table-text main__table-text--green";
                                                }
                                                else if($all_trascations['status'] == 0)
                                                {
                                                    $status = "Rejected";
                                                    $class = "main__table-text main__table-text--red";
                                                } ?>
                                                <div class="<?php echo $class; ?>"><?php echo $status; ?></div>
                                            </td>
                                            <td>
                                                <div class="main__table-btns">
                                                    <a href="<?php echo base_url() ?>withdraw_list_view?id=<?php echo $all_trascations['withdraw_id']; ?>" target="_blank" class="main__table-btn main__table-btn--view">
                                                        <i class="lar la-eye"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>