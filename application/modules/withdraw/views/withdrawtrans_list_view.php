<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-body">
<!--
          <div class="row mb-2">
              <div class="col-8">
                  <div class="main__title">
                      <h2>Janatha Garage</h2>
                  </div>
              </div>
              
              <div class="col-4">
                  <div class="main__title">
                      <a href="detailviewpage.html" class="main__title-link">Save</a>
                  </div>
              </div>
          </div>
-->
          
          
<!--			--------------------body-------------------------->
          
          
          <div class="row">
              
              <div class="col-12">
                  <div class="profile__content">
                      <!-- profile user -->
                      <div class="profile__user">
                          <div class="profile__avatar">
                              <i class="h2 la-user-alt las m-0 pb-1 pl-0 pr-0 pt-1 text-center text-white w-100"></i>
                          </div>
                          <!-- or red -->
                          <div class="profile__meta profile__meta--green">
                              <h3><?php echo $withdraw_list_data['name'] ?></h3>
                              <span><?php echo $withdraw_list_data['tr_id'] ?></span>
                          </div>
                      </div>
                      <!-- end profile user -->

                      
                  </div>
              </div>
          
          <div class="col-10 offset-1">
                  <form action="#" class="form">
                      <div class="row row--form">
                          <div class="col-12 col-md-12">
                              <div class="row row--form">
                                  <div class="col-6">
                                      <input type="text" class="form__input" placeholder="Enter Amont : 500/-" readonly value="Amount: <?php echo $withdraw_list_data['amount'] ?>">
                                  </div>

                                  <div class="col-12 col-sm-6 col-lg-6">
                                  <?php if($withdraw_list_data['withdraw_from'] == 1)
                                        {
                                            $debit_from = "Referral Income";
                                        }
                                        elseif($withdraw_list_data['withdraw_from'] == 2)
                                        {
                                            $debit_from = "Affiliated Income";
                                        }
                                         ?>
                                      <input type="text" class="form__input" placeholder="Debit From : Affiliated Income " readonly value="Debit From :<?php echo $debit_from ?>">
                                  </div>

                                  <div class="col-12 col-sm-12 col-lg-12">
                                      <input type="text" class="form__input" placeholder="Full Name : LakshminarayanaPilla" value="Full Name :<?php echo $withdraw_list_data['name'] ?>" readonly>
                                  </div>

                                  <div class="col-12 col-sm-6 col-lg-6">
                                      <input type="text" class="form__input" placeholder="Account Number : IOB15000002020" readonly value="Account Number :<?php echo $withdraw_list_data['account_number'] ?>">
                                  </div>
                                  
                                  <div class="col-12 col-sm-6 col-lg-6">
                                      <input type="text" class="form__input" placeholder="IFSC Code : IOB152020" readonly value="IFSC Code :<?php echo $withdraw_list_data['ifsc_code'] ?>">
                                  </div>
                                  <div class="col-12 col-sm-6 col-lg-6">
                                      <input type="text" class="form__input" placeholder="Bank Name : IOB" readonly value="Bank Name :<?php echo $withdraw_list_data['bank'] ?>">
                                  </div>
                                  <div class="col-12 col-sm-6 col-lg-6">
                                      <input type="text" class="form__input" placeholder="Branch Name : Akkayyapalem" readonly value="Branch Name :<?php echo $withdraw_list_data['branch'] ?>">
                                  </div>
                                  <div class="col-12 col-sm-6 col-lg-12">
                                  <?php if($withdraw_list_data['status'] == 0)
                                        {
                                            $status = "Rejected";
                                        }
                                        elseif($withdraw_list_data['status'] == 1)
                                        {
                                            $status = "Approved";
                                        }
                                         ?>
                                      <input type="text" class="form__input" placeholder="Type : Appoved" readonly value="Type :<?php echo $status ?>">
                                  </div>
                                  <div class="col-lg-12">
                                      <textarea id="text" name="text" class="form__textarea" placeholder="Comment" readonly ><?php echo $withdraw_list_data['remark'] ?></textarea>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
<!--			--------------------body-------------------------->
          
          

      </div>
  </div>
</div>
