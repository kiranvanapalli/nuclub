<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1><strong>NUCODE : </strong> <?= $user_data['nucode']; ?> </h1>
                </div>
                <div class="col-lg-4"><a href="<?= base_url() ?>withdraw" class="btn btn-primary btn-rounded float-right m-r-20">
                        WithDraw List</a>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <form name="update_withdraw" id="update_withdraw">
                <div class="card">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="fullname">Full Name</label>
                                <input type="text" name="fullname" class="form-control" id="fullname" placeholder="Full Name" value="<?php echo $withdraw_data['name']; ?>" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="mobile_number">Mobile Nmber</label>
                                <input type="text" name="mobile_number" class="form-control" id="mobile_number" placeholder="Mobile Number" value="<?php echo $user_data['mobile_number']; ?>" onkeypress="return onlyNumberKey(event)" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="wallet">Wallet</label>
                                <input type="text" name="wallet" class="form-control" id="wallet" placeholder="Wallet" value="<?php 
                                
                                if($withdraw_data['withdraw_wallet'] == 1)
                                {
                                    $wallet = "NuCredit/Rewards";
                                }
                                else if($withdraw_data['withdraw_wallet'] == 2)
                                {
                                    $wallet = "Referral";
                                }
                                
                                echo $wallet; ?>" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="bank">Bank</label>
                                <input type="text" class="form-control" id="bank" name="bank" placeholder="Bank" value="<?php echo $withdraw_data['bank']; ?>" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="bank">Branch</label>
                                <input type="text" class="form-control" id="branch" name="branch" placeholder="branch" value="<?php echo $withdraw_data['branch']; ?>" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ifsc_code">IFSC Code</label>
                                <input type="text" class="form-control" id="ifsc_code" name="ifsc_code" placeholder="Ifsc code" value="<?php echo $withdraw_data['ifsc_code']; ?>" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="acc_no">Account Number</label>
                                <input type="text" class="form-control" name="acc_no" placeholder="Account Number" value="<?php echo $withdraw_data['account_number']; ?>" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="amount">Amount Request</label>
                                <input type="text" class="form-control" name="amount" id="amount" placeholder="Amount" value="<?php echo $withdraw_data['amount']; ?>" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputState">Withdraw Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">Select Status</option>
                                    <option value="1" <?php if ($withdraw_data['status'] == "1") {
                                                            echo 'selected';
                                                        } ?>>Completed</option>
                                    <option value="2" <?php if ($withdraw_data['status'] == "2") {
                                                            echo 'selected';
                                                        } ?>>Pending</option>
                                    <option value="0" <?php if ($withdraw_data['status'] == "0") {
                                                            echo 'selected';
                                                        } ?>>Rejected</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="comment">Comment</label>
                                <textarea name="comment" id="comment" class="form-control" rows="3" required="required"></textarea>

                            </div>

                        </div>
                    </div>
                    <div class="card-footer bg-light text-right">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- <a href="registeruser-view.html" class="btn btn-primary btn-rounded">Submit</a> -->
                                    <button class="btn btn-primary btn-rounded" type="submit">Update</button>
                                    <!-- <button class="btn btn-light btn-rounded btn-outline">Cancel</button> -->
                                    <input class="form-control" type="hidden" name="edit_id" value="<?= $withdraw_data['withdraw_id']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>