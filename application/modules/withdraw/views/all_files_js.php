<script>
    $(document).ready(function() {
       
        var withdraw_table = $('.withdraw_table').DataTable({});
        var total_withdraw_table = $('#total_withdraw_table').DataTable({});
        $(document).on('submit', '#update_withdraw', function(event) {
            event.preventDefault();
            
            if($('#status').val() == '')
            {
                toastr["error"]("Please Select Withdraw Status");
                return false;
            }
            if($('#comment').val() == '')
            {
                toastr["error"]("Please Enter Comment");
                return false;
            }
             $.ajax({
             url:"<?php echo base_url() ?>updateWithdraw",
             method:'POST',
             data:new FormData(this),
             contentType:false,
             processData:false,
             dataType:'JSON',
             success:function(data)
             {
              console.log(data);
                if(data)
                {

                    toastr["success"]("Withdraw Details Updated Successfully!");
                    window.location.href = "<?php echo base_url(); ?>withdraw";
                }
                else
                {
                      toastr["error"]("Withdraw Details updated failed! Please try again.");
                      return false;
                }

             }
        });
    });

    $(document).on('click', '.delete', function(event) {
            event.preventDefault();
            var id = $(this).attr('id');
            $('.modaldelete').attr('value',id)


    })

    $(document).on('click', '.modaldelete', function(event) {
            event.preventDefault();
            var id = $(this).attr('value');
            console.log(id);
            $.ajax({
                url: "<?php echo base_url() ?>delete_member",
                method: "POST",
                data: {
                    member_id: id,
                    <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                },
                success: function(data) {
                    if (data) {
                        toastr["success"]("Member Deleted Successfully");
                        window.location.href = "<?php echo base_url(); ?>members";
                    } else {
                        toastr["error"]("Delete failed! Please try again.");
                        return false;
                    }
                }
            })
        });
        $(document).on('click', '#close', function(event) {
            event.preventDefault();
            asset_table.ajax.reload();
        });


        $('.get_message').click(function() {
        var id = $(this).data('id');
        // console.log(id);
        var url = '<?php echo base_url(); ?>get_remark_data';
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                id: id
            },
            dataType: 'json',
            success: function(json) {
                $('#msgshowmodel').modal('show');
                // console.log(json.message);
                $('#remark').text(json.remark);
            }

        });

    });

    });
</script>