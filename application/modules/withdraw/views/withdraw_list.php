	<!--START PAGE HEADER -->

	<header class="page-header">
		<div class="d-flex align-items-center">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<h1>Withdraw Request List</h1>
					</div>
					<!-- <div class="col-lg-4"><a href="<?= base_url() ?>add_banner" class="btn btn-primary btn-rounded float-right m-r-20">
							Add Banner</a></div> -->
				</div>
			</div>
		</div>
	</header>
	<!--END PAGE HEADER -->
	<!--START PAGE CONTENT -->
	<section class="page-content container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body p-20">
						<table id="bs4-table" class="table table-striped table-bordered text-center withdraw_table" style="width:100%">
							<thead>
								<tr>
									<th>S.no</th>
									<th>Name</th>
                                    <th>NUCode</th>
									<th>Withdraw Request Amount</th>
									<th>Status</th>
									<th>Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $i = 1;
								foreach ($all_trascations as $list) { ?>
									<tr>
										<td><?= $i++; ?></td>
										<td><?= $list['full_name']; ?></td>
										<td><?= $list['nucode']; ?></td>
										<td><?= $list['amount']; ?></td>
										<td class="text-center">
											<?php if ($list['status'] == 2) {
												$status = "Pending";
												$class = "badge-warning";
											} else {
												$status = "In Active";
												$class = "badge-danger";
											}
											echo '<span class="badge ' . $class . ' badge-sm">' . $status . '</span>';
											?>
										</td>
										<td><?= date('d-m-Y', strtotime($list['created_at'])); ?></td>
										<td class="text-center action">
											<div class="m-0">
												<div class="icon col-xs-6 col-md-3">
													<a href="<?= base_url() ?>view_info?id=<?= $list['withdraw_id']; ?>" class="tile-primary">
														<i class="zmdi zmdi-edit zmdi-hc-fw zmdi-hc-lg"></i>
													</a>
												</div>
												<!-- <div class="icon col-xs-6 col-md-3">
													<a href="javascript:;" id="delete_banner" data-id="<?= $bannerslist['banner_id']; ?>" class="tile-primary delete" value="<?= $bannerslist['banner_id']; ?>">
														<i class="zmdi zmdi-delete zmdi-hc-fw zmdi-hc-lg"></i>
													</a>
												</div> -->
												<!-- <div class="icon col-md-3"><button id="delete_banner" data-id="<?= $bannerslist['banner_id']; ?>" class="delete btn btn-link"><i class="zmdi zmdi-delete zmdi-hc-fw zmdi-hc-lg"></i></button></div> -->
											</div>
										</td>
									</tr>
								<?php	}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>






	<!--END PAGE CONTENT -->