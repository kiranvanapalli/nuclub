<style>
    .nucoins {
  position: relative;
  width: auto;
  pointer-events: none;
  top: 0px;
}
</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-body">
            <div class="row mb-2">
                <!-- main title -->
                <div class="col-6">
                    <div class="main__title">
                        <h2>NU Coins List</h2>
                    </div>
                </div>

                <!-- <div class="col-6">
                    <div class="main__title">
                        <a href="<?php echo base_url() ?>add_member" class="main__title-link w-50">Add Member</a>
                    </div>
                </div> -->
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-content collapse show">
                            <div class="table-responsive">
                                <table class="table table-sm mb-0 text-center contact_us_table">
                                    <thead>
                                        <tr>
                                            <th>Full Name</th>
                                            <th>Mobile Number</th>
                                            <th>Email</th>
                                            <th>Member Code</th>
                                            <th>NU Coins</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $i = 1;
                                        foreach ($nucoins_list as $nucoins_list) { ?>

                                            <tr>
                                                <td>
                                                    <div class="main__table-text"><?php echo $nucoins_list['fullname'] ?></div>
                                                </td>
                                                <td>
                                                    <div class="main__table-text"><?php echo $nucoins_list['mobilenumber']; ?></div>
                                                </td>
                                                <td>
                                                    <div class="main__table-text"><?php echo $nucoins_list['email']; ?></div>
                                                </td>

                                                 <td>
                                                    <div class="main__table-text"><?php echo $nucoins_list['member_code']; ?></div>
                                                </td>
                                                <td>
                                                    <div class="main__table-text"><?php echo $nucoins_list['nu_coins']; ?></div>
                                                </td>

                                                <td class="text-center">
                                                <div class="main__table-btns text-center">
                                                    <a class="btn btn-dropbox get_data"
                                                    data-id="<?php echo $nucoins_list['member_id']; ?>" id="<?php echo $nucoins_list['member_id']; ?>">Add</a>
                                                    </div>
                                                </td>
                                                
                                            </tr>

                                        <?php }

                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="modal modaldelete" id="modaldelete">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body p-4">
            <div class="row">
               <div class="col-12 pb-1">
                  <div class="text-center">
                     <h4>Delete Join Us Details</h4>
                     <p>Are you sure to permanently delete this Join US Details</p>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-6">
                  <div class="main__title">
                     <a href="#" class="main__title-link w-100" id="delete_member">Delete</a>
                  </div>
               </div>
               <div class="col-6">
                  <div class="main__title">
                     <a href="#" class="main__title-link w-100" data-dismiss="modal" id="close">Close</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> -->

<div class="modal fade text-left show" id="nu_code_model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel8"
    style="padding-right: 17px;" aria-modal="true">
    <div class="modal-backdrop modal-dialog nucoins" role="document" >
        <div class="modal-content">
            <div class="modal-header bg-primary white">
                <h4 class="modal-title white" id="myModalLabel8">NU Coins</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
           <form name="nu_model_form" id="nu_model_form">
            <div class="modal-body">
              
                <label for="" class="mt-1">NU Code</label>
                <input type = "text" class="form-control" id="nu_code" name="nu_code"  readonly>
                <label for="" class="mt-1">Full Name</label>
                <input type = "text" class="form-control" id="full_name" name="full_name" readonly>
                <label for="" class="mt-1">Mobile Number</label>
                <input type = "text" class="form-control" id="mobile_number" name="mobile_number" readonly>
                <label for="" class="mt-1">Present NU Coins</label>
                <input type = "text" class="form-control" id="nu_coins" name="nu_coins" readonly>
                <label for="" class="mt-1">Add NU Coins</label>
                <input type = "text" class="form-control" id="add_nu_coins" name="add_nu_coins">
                <label for="" class="mt-1">Remark</label>
                <textarea id="remark" rows="3" columns="30" class="form-control" name="remark"></textarea>
                 <input type="hidden" name="member_id" id="member_id">

               </div>
            
            <div class="modal-footer">
            <button type="submit" name="update_nucoin" class="main__title-link">Update</button>

                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-outline-primary">Save changes</button> -->
            </div>
           </form>
        </div>
    </div>

    </div>

 

  
 