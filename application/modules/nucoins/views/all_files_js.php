<script>
   
    $(document).ready(function() {
        var contact_us_table = $('.contact_us_table').DataTable({});
    $(document).on('click', '.delete', function(event) {
            event.preventDefault();
            var id = $(this).attr('id');
            $('.modaldelete').attr('value',id)


    })

    $(document).on('click', '.modaldelete', function(event) {
            event.preventDefault();
            var id = $(this).attr('value');
            console.log(id);
            $.ajax({
                url: "<?php echo base_url() ?>delete_contact_us",
                method: "POST",
                data: {
                    id: id,
                    <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                },
                success: function(data) {
                    if (data) {
                        toastr["success"]("Contact Us Data Deleted Successfully");
                        window.location.href = "<?php echo base_url(); ?>contact";
                    } else {
                        toastr["error"]("Delete failed! Please try again.");
                        return false;
                    }
                }
            })
        });


        $('.get_message').click(function() {
        var id = $(this).data('id');
        console.log(id);
        var url = '<?php echo base_url(); ?>get_message_data';
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                id: id
            },
            dataType: 'json',
            success: function(json) {
                $('#msgshowmodel').modal('show');
                // console.log(json.message);
                $('#fullname').attr(json.fullname);
                $('#mobilenumber').attr(json.mobilenumber);
                $('#email').attr(json.email);
            }

        });

    });
        $(document).on('click', '#close', function(event) {
            event.preventDefault();
            window.location.href = "<?php echo base_url(); ?>join_us";
        });

        $(document).on('click', '.get_data', function(){

var id = $(this).attr("id");
console.log(id);
$.ajax({
 url:"<?php echo base_url() . 'get_dataId'?>",
 method:"POST",
 data:{member_id:id,<?php echo $this->security->get_csrf_token_name(); ?> : <?php echo "'".$this->security->get_csrf_hash()."'" ?>},
 dataType:"json",
   success:function(data)
   {
        // $('.nu_model_form')[0].reset();
        $("#nu_code").val(data.member_code);
        $("#full_name").val(data.fullname);
        $("#mobile_number").val(data.mobilenumber);
        $("#nu_coins").val(data.nu_coins);
        $("#member_id").attr('value',data.member_id);
        $('#nu_code_model').modal('show');
   }
})
});


$(document).on('submit', '#nu_model_form', function(event) {
            event.preventDefault();
            var add_nu_coins = $('#add_nu_coins').val();
            var remark = $('#remark').val();

            if (add_nu_coins == '') {
                toastr["error"]("Please add NU Coins");
                return false;
            }
            if (remark == '') {
                toastr["error"]("Remark is required!");
                return false;
            }
             $.ajax({
                url: "<?php echo base_url() ?>add_nucoins",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    if (data) {

                        toastr["success"]("NU Coins Added Successfully!");
                         window.location.href = "<?php echo base_url(); ?>nucoins";

                    }
                    else
                    {
                        toastr["error"]("NU Coins Added Failed!");
                        return false;
                    }
                }
            });

        });
    });
</script>