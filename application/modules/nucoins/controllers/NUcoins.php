<?php
defined('BASEPATH') or exit('No direct script access allowed');
class NUcoins extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
        $this->load->library('my_file_upload');

    }

    public function index()
    {
        $where = '';
        $where1 = ['status' => 1];
        $data['file'] = 'nucoins/nucoins_list';
        $data['custom_js'] = 'nucoins/all_files_js';
        $type = "array";
        $nucoins_list = $this->Allfiles_model->GetDataAll("tb_members", $where1, $type, 'member_id', $limit = '');
        $data['nucoins_list'] = $nucoins_list;
        $this->load->view('admin_template/main', $data);
    }

   

    public function delete_contact_us()
    {
      

            $where = ['contact_id' => $_POST['id']];
            $result = $this->Allfiles_model->deleteData("tb_contact", $where);
            // echo $this->db->last_query();
            echo $result;
         }
    
    public function get_message_data()
        {
         $result = array();
         $post_data = $this->input->post();
           if(!empty($post_data) && count($post_data) > 0)
           {
             $whr = ['member_id'=> $post_data['id']];
                  $result = $this->Allfiles_model->getSingleData("tb_members",$whr)->row_array();
                  echo json_encode($result);
           }
         }
         public function get_dataId()
         {
          if (isset($_POST['member_id']) && !empty($_POST['member_id'])) 
          {
              $where = ['member_id' => $_POST['member_id']];
              $type = "row";
              $result  = $this->Allfiles_model->GetDataAll("tb_members as a",$where,$type,$order='',$limit='');
              echo json_encode($result);   
          }
          else
          {
              echo "Something went wrong please try again!";
          }  
         }
     


         public function add_nucoins() {

            $code = mt_rand(11111, 99999999);
            $tr_id = "NUC" . $code;
              $data = array(
                  'nucode' => $this->input->post("nu_code"),
                  'tr_id' => $tr_id,
                  'nucoins' => $this->input->post("add_nu_coins"),
                  'remark' => $this->input->post("remark"),
                  'member_id' => $this->input->post('member_id'),
                  'credit_debit' => 1,
                  'created_at' => date('Y-m-d H:i:s'),
                  'updated_at' => date('Y-m-d H:i:s'),
                  'status' => 1,
              );
  
           
              $result = $this->Allfiles_model->data_save("tb_nucoin_transaction", $data);
              if($result)
              {
                $where = $this->input->post('member_id');
                $type ="array";
                $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $where);
                $member_data = $get_member_details['resultSet'];
                $total_nucoins = $member_data['nu_coins'];
                $admin_add_coins = $this->input->post("add_nu_coins");
                $total = $total_nucoins+$admin_add_coins;
                $data = array(
                    'nu_coins' => $total 
                );
                $where_member_id = ['member_id' =>  $where];
                $result = $this->Allfiles_model->updateData('tb_members',$data,$where_member_id);
               
              }
              echo  json_encode($result);
          }   
}
