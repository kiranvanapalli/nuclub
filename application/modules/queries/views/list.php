	<!--START PAGE HEADER -->

	<header class="page-header">
	    <div class="d-flex align-items-center">
	        <div class="container">
	            <div class="row">
	                <div class="col-lg-8">
	                    <h1>Queries List</h1>
	                </div>
	                <!-- <div class="col-lg-4"><a href="<?= base_url() ?>add_products" class="btn btn-primary btn-rounded float-right m-r-20">
							Add Product</a></div> -->
	            </div>
	        </div>
	    </div>
	</header>
	<!--END PAGE HEADER -->
	<!--START PAGE CONTENT -->
	<section class="page-content container-fluid">
	    <div class="row">
	        <div class="col-12">
	            <div class="card">
	                <div class="card-body p-20">
	                    <table id="bs4-table" class="table table-striped table-bordered text-center queries_table" style="width:100%">
	                        <thead>
	                            <tr>
	                                <th>S.no</th>
	                                <th>Full Name</th>
	                                <th>Mobile Number</th>
	                                <th>Status</th>
	                                <th>Date</th>
	                                <th>Actions</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            <?php $i = 1;
                                foreach ($all_queries as $quiers) { ?>
	                                <tr>
	                                    <td><?= $i++; ?></td>
	                                    <td><?php

                                            foreach ($all_users as $user_list) {
                                                if ($user_list['user_id'] == $quiers['user_id']) {
                                                    echo $user_list['full_name'];
                                                }
                                            }

                                            ?></td>
	                                    <td><?php

                                            foreach ($all_users as $user_list) {
                                                if ($user_list['user_id'] == $quiers['user_id']) {
                                                    echo $user_list['mobile_number'];
                                                }
                                            }
                                            ?></td>
	                                    <td class="text-center">
	                                        <?php if ($quiers['status'] == 2) {
                                                $status = "Pending";
                                                $class = "badge-warning";
                                            } else {
                                                $status = "In Active";
                                                $class = "badge-danger";
                                            }
                                            echo '<span class="badge ' . $class . ' badge-sm">' . $status . '</span>';
                                            ?>
	                                    </td>
	                                    <td><?= date('d-m-Y', strtotime($quiers['created_at'])); ?></td>
	                                    <td class="text-center action">
	                                        <div class="m-0">
	                                            <div class="icon col-xs-6 col-md-3"><a href="<?= base_url() ?>edit_query?id=<?= $quiers['id']; ?>&user_id=<?= $quiers['user_id']; ?>" class="tile-primary"><i class="zmdi zmdi-edit zmdi-hc-fw zmdi-hc-lg"></i></div>
	                                            <!-- <div class="icon col-md-3"><a href="javascript:;" id="sweetalert_demo_8" data-id="<?= $quiers['id']; ?>" class="delete"><i class="zmdi zmdi-delete zmdi-hc-fw zmdi-hc-lg"></i></a></div> -->
	                                        </div>
	                                    </td>
	                                </tr>
	                            <?php    }
                                ?>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>





	<!--END PAGE CONTENT -->