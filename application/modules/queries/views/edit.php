<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Update Query</h1>
                </div>
                <div class="col-lg-4"><a href="<?= base_url() ?>quires" class="btn btn-primary btn-rounded float-right m-r-20">
                        Query's List</a></div>
            </div>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form name="update_query_form" id="update_query_form">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="category_name">Full Name</label>
                                <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name" value="<?= $get_user['full_name']; ?>" readonly>
                            </div>
                            <div class="form-group col-md-4">
                            <label for="category_name">Mobile Number</label>
                                <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Full mobile_number" value="<?= $get_user['mobile_number']; ?>" readonly>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" value="<?= $get_query['subject']; ?>" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            
                            <div class="form-group col-md-6">
                            <label for="comment">Comment</label>
                               <textarea name="comment" id="comment" class="form-control" rows="3" required="required" readonly><?= $get_query['comment']; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                            <label for="admin_comment">Admin Remark</label>
                               <textarea name="admin_comment" id="admin_comment" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-row">
                        <div class="form-group col-md-3">
                                <label for="inputState">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">Select Status</option>
                                    <option value="2" <?php if ($get_query['status'] == "2") {
                                                            echo 'selected';
                                                        } ?>>Pending</option>
                                    <option value="0" <?php if ($get_query['status'] == "0") {
                                                            echo 'selected';
                                                        } ?>>Close</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light text-right">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-rounded" type="submit">Update</a>
                                        <button class="btn btn-light btn-rounded btn-outline">Cancel</button>
                                        <input type="hidden" name="edit_id" id="edit_id" value="<?= $get_query['id']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>