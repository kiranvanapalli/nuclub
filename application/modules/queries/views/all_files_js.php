<script>
    function onlyNumberKey(evt) {

          // Only ASCII character in that range allowed
          var ASCIICode = (evt.which) ? evt.which : evt.keyCode
          if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
              return false;
          return true;
      }
    $(document).ready(function() {
        var asset_table = $('.queries_table').DataTable({});
        var d = new Date();
        var year = d.getFullYear();
        $("#date").datepicker({
            format: 'mm/dd/yyyy',
            endDate: '-3d',
            autoclose: true //to close picker once year is selected
        });
        $(document).on('submit', '#add_news', function(event) {
            event.preventDefault();
            var title = $('#title').val();
            var description = $('#desc').val();
            var favourite = $('#favourite').val();
            var image = $('#formimage').val();
           
            if (title == '') {
                toastr["error"]("Title is required!");
                return false;
            }
            if (description == '') {
                toastr["error"]("Description is required!");
                return false;
            }
            if (favourite == '') {
                toastr["error"]("Please Select Favourite!");
                return false;
            }
            if (image == '') {
                toastr["error"]("Please Select Image");
                return false;
            }
           
           
            $.ajax({
                url: "<?php echo base_url() ?>save_news",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    if (data) {

                        toastr["success"]("News Added Successfully!");
                         window.location.href = "<?php echo base_url(); ?>newsnevents";

                    }
                    else
                    {
                        toastr["error"]("News added failed");
                        return false;
                    }
                }
            });

        });


        $(document).on('submit', '#update_query_form', function(event) {
            event.preventDefault();
            var admin_comment = $('#admin_comment').val();
            var status = $('#status').val();
            if (admin_comment == '') {
                toastr["error"]("Admin Comment is required");
                return false;
            }
            if (status == '') {
                toastr["error"]("Please Select Status");
                return false;
            }
            
             $.ajax({
             url:"<?php echo base_url() ?>update_query",
             method:'POST',
             data:new FormData(this),
             contentType:false,
             processData:false,
             dataType:'JSON',
             success:function(data)
             {
              console.log(data);
                if(data.status == 'success')
                {

                    toastr["success"]("Quire Updated Successfully");
                    window.location.href = "<?php echo base_url(); ?>quires";
                }
                else
                {
                      toastr["error"]("Quire Updated updated failed! Please try again.");
                      return false;
                }

             }
        });
    });

    $(document).on('click', '.delete', function(event) {
            event.preventDefault();
            var id = $(this).attr('id');
            $('.modaldelete2').attr('value',id)


    })

    $(document).on('click', '.modaldelete', function(event) {
            event.preventDefault();
            var id = $(this).attr('value');
            console.log(id);
            $.ajax({
                url: "<?php echo base_url() ?>delete_news",
                method: "POST",
                data: {
                    news_id: id,
                    <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                },
                success: function(data) {
                    if (data) {
                        toastr["success"]("News Deleted Successfully");
                        window.location.href = "<?php echo base_url(); ?>newsnevents";
                    } else {
                        toastr["error"]("Delete failed! Please try again.");
                        return false;
                    }
                }
            })
        });
        $(document).on('click', '#close', function(event) {
            event.preventDefault();
            asset_table.ajax.reload();
        });
    });
</script>