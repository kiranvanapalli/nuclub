<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Queries extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
        $this->load->library('my_file_upload');

    }

    public function index()
    {
        $where = ["status" => 2];
        $where2 = ""; 
        $data['file'] = 'queries/list';
        $data['custom_js'] = 'queries/all_files_js';
        $data['validation_js'] = 'admin/all_common_js/frontend_validation_admin';
        $type = "array";
        $data['all_users'] = $this->Allfiles_model->GetDataAll("tb_users", $where2, $type, 'user_id', $limit = '');
        $data['all_queries'] = $this->Allfiles_model->GetDataAll("tb_querys", $where, $type, 'id', $limit = '');
       
        $this->load->view('admin_template/main', $data);
    }

    public function add_news()
    {
        $data['file'] = 'news_n_events/add_news';
        $where = ["status" => 1];
        $type = "array";
        // $data['states_list'] = $this->Allfiles_model->GetDataAllmodels("tb_states", $where, $type, 'state_name', $limit = '');
        $data['custom_js'] = 'news_n_events/all_files_js';
        $this->load->view('admin_template/main', $data);

    }
    public function edit()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $user_id = $_GET['user_id'];   
            $get_query = $this->Allfiles_model->get_data('tb_querys', '*', 'id', $id);
            $data['get_query'] = $get_query['resultSet'];
            $get_user = $this->Allfiles_model->get_data('tb_users', '*', 'user_id', $user_id);
            $data['get_user'] = $get_user['resultSet'];
            $data['file'] = 'queries/edit';
            $data['custom_js'] = 'queries/all_files_js';
            $this->load->view('admin_template/main', $data);
        }
    }

    public function update()
    {
        if (!empty($_POST['edit_id'])) {

            $response = [];
            $where = ['id' => $_POST['edit_id']];
          
            
            $data = array(
                'admin_comment' => $this->input->post("admin_comment"),
                'updated_at' => date('Y-m-d H:i:s'),
                'status' => $this->input->post('status'),
            );

            $update = $this->Allfiles_model->updateData("tb_querys", $data, $where);
            if ($update) {
                $response = ['status' => 'success'];
            } else {
                $response = ['status' => 'fail'];
            }
            echo json_encode($response);
        }

    }

    public function delete_news()
    {
      

            $where = ['news_id' => $_POST['news_id']];
            $result = $this->Allfiles_model->deleteData("tb_newsnevents", $where);
            // echo $this->db->last_query();
            echo $result;
         }
    

}
