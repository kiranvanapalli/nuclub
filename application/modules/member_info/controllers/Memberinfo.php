<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Memberinfo extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
        $this->load->library('my_file_upload');

    }

    public function index()
    {
        $where = ['status' => 1];
        $data['file'] = 'member_info/list';
        $data['custom_js'] = 'member_info/all_files_js';
        // $data['validation_js'] = 'admin/all_common_js/frontend_validation_admin';
        $type = "array";
        $data['member_list'] = $this->Allfiles_model->GetDataAll("tb_members", $where, $type, 'member_id', $limit = '');
        $this->load->view('admin_template/main', $data);
    }

   

    public function delete()
    {
      
           $where = ['id' => $_POST['id']];
            $result = $this->Allfiles_model->deleteData("tb_news_letter", $where);
            // echo $this->db->last_query();
            echo $result;
         }
    
        
         public function listinfo()
         {
             if (isset($_GET['id']) && !empty($_GET['id'])) {
                $id = $_GET['id'];
              
                $type = "array";
                $member_info = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $id);
                $data['member_info']   = $member_info['resultSet'];
                $where = ['member_id' => $id];
                $data['referral_info'] = $this->Allfiles_model->GetDataAll("tb_referrals", $where, $type, 'ref_id', $limit = '');
                $data['nucoins'] = $this->Allfiles_model->GetDataAll("tb_nucoin_transaction", $where, $type, 'nu_trans_id', $limit = '');
                $data['withdraw'] = $this->Allfiles_model->GetDataAll("tb_withdraw", $where, $type, 'withdraw_id', $limit = '');
                 $where = '';

             $data['state_list'] = $this->Allfiles_model->GetDataAll("tb_states", $where, $type, 'state_id', $limit = '');

            $data['file'] = 'member_info/list-info';
            $data['custom_js'] = 'member_info/all_files_js';
            $type = "array";
            // $data['member_info'] = $this->Allfiles_model->GetDataAll("tb_members", $where, $type, 'member_id', $limit = '');

            $this->load->view('admin_template/main', $data); 
         }
         }
}
