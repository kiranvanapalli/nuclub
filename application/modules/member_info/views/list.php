<div class="app-content content">
      <div class="content-wrapper">
        <div class="content-body">
			<div class="row mb-2">
				<!-- main title -->
				<div class="col-6">
					<div class="main__title">
						<h2>Member Info</h2>
					</div>
				</div>
				
				<div class="col-6">
<!--
					<div class="main__title">
						<a href="Add-Members.html" target="_blank" class="main__title-link w-50">Add Member</a>
					</div>
-->
				</div>
			</div>
			
<div class="row">
	<div class="col-12">
		<div class="card">
			
			<div class="card-content collapse show">
				<div class="table-responsive">
					<table class="table table-sm mb-0 member_info_table">
						<thead>
							<tr>
								<th>Name</th>
								<th>NU Code</th>
								<th>NU Coins</th>
								<th>Ref Money</th>
								<th>Affiliated Money</th>
								<th>ACTIONS</th>
							</tr>
						</thead>
						<tbody>
                            <?php
                            foreach($member_list as $member_list) {
                            ?>
							<tr>
									<td><div class="main__table-text"><?php echo $member_list['fullname'] ?></div></td>
									<td><div class="main__table-text"><?php echo $member_list['member_code'] ?></div></td>
									<td><div class="main__table-text"><i class="la la-coins mr-1"></i><?php echo $member_list['nu_coins'] ?></div></td>
									<td><div class="main__table-text"><strong>₹ <?php echo $member_list['points'] ?></strong></div></td>
									<td><div class="main__table-text"><strong>₹ <?php echo $member_list['affiliated_rupees'] ?></strong></div></td>
									<td>
										<div class="main__table-btns">
											<a href="<?php echo base_url() ?>listinfo?id= <?php echo $member_list['member_id'] ?>" class="main__table-btn main__table-btn--view">
												<i class="lar la-eye"></i>
											</a>
										</div>
									</td>
								</tr>

                                <?php
                                }
                                ?>
						</tbody>
					</table>
				</div>		
			</div>
		</div>
	</div>
</div>
    	</div>
	</div>
</div>

