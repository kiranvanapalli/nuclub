<div class="app-content content">
      <div class="content-wrapper">
        <div class="content-body">
			

<div class="col-12">
					<div class="profile__content">
						<!-- profile user -->
						<div class="profile__user">
							<div class="profile__avatar">
								<i class="h2 la-user-alt las m-0 pb-1 pl-0 pr-0 pt-1 text-center text-white w-100"></i>
							</div>
							<!-- or red -->
							<div class="profile__meta profile__meta--green">
								<h3>Hi, welcome back! <strong>LakshmiNarayanaPilla</strong></h3>
								<span>Your business dashboard template</span>
							</div>
						</div>
						<!-- end profile user -->
					</div>
				</div>
				
				<!-- main title -->
				<div class="col-12">
					<div class="main__title">
						<h2>Personal Information</h2>
					</div>
				</div>
			
				<div class="col-12">
                    
					<div id="about-me">
                                                <div class="profile-personal-info">
                                                    <h4 class="text-primary mb-1"></h4>
                                                    <div class="row mb-1">
                                                        <div class="col-3">
                                                            <h5 class="f-w-500">Name <span class="pull-right">:</span>
                                                            </h5>
                                                        </div>
                                                        <div class="col-9"><span><?php echo $member_info['fullname'] ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col-3">
                                                            <h5 class="f-w-500">Email <span class="pull-right">:</span>
                                                            </h5>
                                                        </div>
                                                        <div class="col-9"><span><?php echo $member_info['email'] ?></span>
                                                        </div>
                                                    </div>
													<div class="row mb-1">
                                                        <div class="col-3">
                                                            <h5 class="f-w-500">Mobile Number <span class="pull-right">:</span>
                                                            </h5>
                                                        </div>
                                                        <div class="col-9"><span><?php echo $member_info['mobilenumber'] ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col-3">
                                                            <h5 class="f-w-500">Gender <span class="pull-right">:</span></h5>
                                                        </div>
                                                        <div class="col-9"><span><?php echo $member_info['gender'] ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col-3">
                                                            <h5 class="f-w-500">DOB <span class="pull-right">:</span>
                                                            </h5>
                                                        </div>
                                                        <div class="col-9"><span><?php echo $member_info['date_of_birth'] ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col-3">
                                                            <h5 class="f-w-500">State <span class="pull-right">:</span></h5>
                                                        </div>
                                                        <div class="col-9">
                                                            <?php foreach($state_list as $state_list) {
                                                                if($state_list['state_id'] ==  $member_info['state'])
                                                                {
                                                                    ?>
                                                                    <span><?php echo $state_list['state_name'] ?></span>
                                                               <?php
                                                                }
                                                                } ?>

                                                        </div>
                                                    </div>
                                                    <div class="row mb-1">
                                                        <div class="col-3">
                                                            <h5 class="f-w-500">City<span class="pull-right">:</span></h5>
                                                        </div>
                                                        <div class="col-9"><span><?php echo $member_info['city'] ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
				</div>
		
			<hr>	
			
			<div class="col-12">
					<div class="main__title">
						<h2>Ref List</h2>
					</div>
				</div>
			
			<div class="col-12">
		<div class="card">
			
			<div class="card-content collapse show">
				<div class="table-responsive">
					<table class="table table-sm mb-0">
						<thead>
							<tr>
								<th>Transaction ID</th>
								<th>Ref Name</th>
								<th>Ref Email</th>
								<th>Ref Status</th>
								<th>Earned Money</th>
							</tr>
						</thead>
						<tbody>
                         <?php 
                         foreach($referral_info as $referral_info) {
                         ?>
							<tr>

								<td><div class="main__table-text"><?php echo $referral_info['tr_id'] ?></div></td>
								<td><div class="main__table-text"><?php echo $referral_info['fullname'] ?></div></td>
								<td><div class="main__table-text"><?php echo $referral_info['email'] ?></div></td>
								<td>
								<?php if($referral_info['status'] == 0)
								{
									$status = "User Send Request";
									$class = "text-secondary";
								}
								elseif($referral_info['status'] == 1)
								{
									$status = "User Refer Success";
									$class = "text-success";
								}
								else
								{
									$status = "Pending with Admin for";
									$class = "text-danger";
								}
								?>
								<div class="<?php echo $class; ?>"><?php echo $status; ?></div></td>
								<td><div class="main__table-text"><?php echo $referral_info['points'] ?></div></td>
							</tr>
                            <?php 
                         }
                         ?>
						</tbody>
					</table>
				</div>		
			</div>
		</div>
	</div>
			
			<hr>	
			
			<div class="col-12">
					<div class="main__title">
						<h2>Coins</h2>
					</div>
				</div>
			
			<div class="col-12">
		<div class="card">
			
			<div class="card-content collapse show">
				<div class="table-responsive">
					<table class="table table-sm mb-0">
						<thead>
							<tr>
								<th>Transaction ID</th>
								<th>Admin Added Coins</th>
								<th>Remark</th>
							</tr>
						</thead>
						<tbody>
						<?php 
                         foreach($nucoins as $nucoins) {
                         ?>
							<tr>
								<td><div class="main__table-text"><?php echo $nucoins['tr_id']; ?></div></td>
								<td><div class="main__table-text"><i class="la la-coins mr-1"></i><?php echo $nucoins['nucoins']; ?></div></td>
								<td><div class="main__table-text"><?php echo $nucoins['remark']; ?></div></td>
							</tr>
							<?php
						 }
						 ?>
						</tbody>
					</table>
				</div>		
			</div>
		</div>
	</div>
			
			<hr>	
			
			<div class="col-12">
					<div class="main__title">
						<h2>Affiliated</h2>
					</div>
				</div>
			
			<div class="col-12">
		<div class="card">
			
			<div class="card-content collapse show">
				<div class="table-responsive">
					<table class="table table-sm mb-0">
						<thead>
							<tr>
								<th>Transaction ID</th>
								<th>Product Name</th>
								<th>No Of Shame</th>
								<th>Earned Money</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><div class="main__table-text">#NUID152022</div></td>
								<td><div class="main__table-text">Product Name</div></td>
								<td><div class="main__table-text">25</div></td>
								<td><div class="main__table-text">Earned Money</div></td>
								<td><div class="main__table-text main__table-text--green">Visible</div></td>
							</tr>
						</tbody>
					</table>
				</div>		
			</div>
		</div>
	</div>
			
			<hr>	
			
			<div class="col-12">
					<div class="main__title">
						<h2>Withdraw</h2>
					</div>
				</div>
			
			<div class="col-12">
		<div class="card">
			
			<div class="card-content collapse show">
				<div class="table-responsive">
					<table class="table table-sm mb-0">
						<thead>
							<tr>
								<th>Transaction ID</th>
								<th>Withdraw</th>
								<th>Admin Status</th>
								<th>Remark</th>
								<th>ACTIONS</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($withdraw as $withdraw) {
							?>
							<tr>
								<td><div class="main__table-text"><?php echo $withdraw['tr_id'] ?></div></td>
								<td><div class="main__table-text"><?php echo $withdraw['amount'] ?></div></td>
								<td>
								<?php
									if($withdraw['status'] == 0 || $withdraw['status'] == 1 )
									{
										$url = "tras_data";
									}

									else
									{
										$url = "transcations";
									}
									?>
									<a href="<?php echo base_url() ?><?php echo $url ?>" class="main__table-text main__table-text--green">
									<?php if($withdraw['status'] == 0)
								{
									$status = "Rejected";
								}
								elseif($withdraw['status'] == 1)
								{
									$status = "Approved";
								}
								else
								{
									$status = "Pending with Admin";
								}
								?>
										<?php echo $status ?></a></td>
								
								<td><div class="main__table-text"><?php echo $withdraw['remark'] ?></div></td>
								<td><div class="main__table-btns">
									<?php
									if($withdraw['status'] == 0 || $withdraw['status'] == 1 )
									{
										$url = "tras_data";
									}

									else
									{
										$url = "transcations";
									}
									?>
											<a href="<?php echo base_url() ?><?php echo $url ?>" class="main__table-btn main__table-btn--view">
												<i class="lar la-eye"></i>
											</a>
										</div></td>
							</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div>		
			</div>
		</div>
	</div>
			

			</div>
</div>
</div>