<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title> :: NU Club :: </title>
   <!-- ================== GOOGLE FONTS ==================-->
   <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
   <!-- ======================= GLOBAL VENDOR STYLES ========================-->
   <link rel="stylesheet" href="admin_assets/css/vendor/bootstrap.css">
   <link rel="stylesheet" href="admin_assets/vendor/metismenu/dist/metisMenu.css">
   <link rel="stylesheet" href="admin_assets/vendor/switchery-npm/index.css">
   <link rel="stylesheet" href="admin_assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
   <!-- ======================= LINE AWESOME ICONS ===========================-->
   <link rel="stylesheet" href="admin_assets/css/icons/line-awesome.min.css">
   <!-- ======================= DRIP ICONS ===================================-->
   <link rel="stylesheet" href="admin_assets/css/icons/dripicons.min.css">
   <!-- ======================= MATERIAL DESIGN ICONIC FONTS =================-->
   <link rel="stylesheet" href="admin_assets/css/icons/material-design-iconic-font.min.css">
   <!-- ======================= GLOBAL COMMON STYLES ============================-->
   <link rel="stylesheet" href="admin_assets/css/common/main.bundle.css">
   <!-- ======================= LAYOUT TYPE ===========================-->
   <link rel="stylesheet" href="admin_assets/css/layouts/vertical/core/main.css">
   <!-- ======================= MENU TYPE ===========================================-->
   <link rel="stylesheet" href="admin_assets/css/layouts/vertical/menu-type/default.css">
   <!-- ======================= THEME COLOR STYLES ===========================-->
   <link rel="stylesheet" href="admin_assets/css/layouts/vertical/themes/theme-a.css">
</head>

<body>
   <div class="container">
      <form class="sign-in-form" method="POST">
         <div class="card">
            <div class="card-body nu-logo">
               <a href="<?= base_url(); ?>" class="brand text-center d-block m-b-20">
                  <img src="admin_assets/img/logo.png" alt="nuclub Logo" />
               </a>
               <h5 class="sign-in-heading text-center m-b-20">Sign in to your account</h5>
               <div class="form-group nu-login">
                  <label for="inputEmail" class="sr-only">Email address</label>
                  <input type="email" id="inputEmail" class="form-control" name="admin_email" placeholder="Email address" required="">
               </div>

               <div class="form-group nu-login">
                  <label for="inputPassword" class="sr-only">Password</label>
                  <input type="password" id="inputPassword" class="form-control" name="admin_pass" placeholder="Password" required="">
               </div>
               <div class="checkbox m-b-10 m-t-20">
                  <!--
						<div class="custom-control custom-checkbox checkbox-primary form-check">
							<input type="checkbox" class="custom-control-input" id="stateCheck1" checked="">
							<label class="custom-control-label" for="stateCheck1">	Remember me</label>
						</div>
-->
                  <!--						<a href="auth.forgot-password.html" class="float-right">Forgot Password?</a>-->
               </div>
               <!-- <a class="btn btn-primary btn-rounded btn-floating btn-lg btn-block" href="index.html">Sign In</a> -->
               <button class="btn btn-primary btn-rounded btn-floating btn-lg btn-block" type="submit">Sign In</button>
               <!--				 <p class="text-muted m-t-25 m-b-0 p-0">Don't have an account yet?<a href="auth.register.html"> Create an account</a></p>-->
            </div>

         </div>
      </form>
   </div>

   <!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
   <script src="admin_assets/vendor/modernizr/modernizr.custom.js"></script>
   <script src="admin_assets/vendor/jquery/dist/jquery.min.js"></script>
   <script src="admin_assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
   <script src="admin_assets/vendor/js-storage/js.storage.js"></script>
   <script src="admin_assets/vendor/js-cookie/src/js.cookie.js"></script>
   <script src="admin_assets/vendor/pace/pace.js"></script>
   <script src="admin_assets/vendor/metismenu/dist/metisMenu.js"></script>
   <script src="admin_assets/vendor/switchery-npm/index.js"></script>
   <script src="admin_assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
   <!-- ================== GLOBAL APP SCRIPTS ==================-->
   <script src="admin_assets/js/global/app.js"></script>
   <!--Toster plugins -->
   <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
   <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
   <!--Toster end plugins -->

   <!--Start toster validation -->
   <?php if ($this->session->flashdata('success')) {  ?>
      <script type="text/javascript">
         toastr.success("<?php echo $this->session->flashdata('success'); ?>", "", {
            "closeButton": "true",
            "progressBar": "true",
            "timeOut": "5000",
            "extendedTimeOut": "2000"
         });
      </script>
   <?php  } elseif ($this->session->flashdata('error')) { ?>
      <script type="text/javascript">
         toastr.error("<?php echo $this->session->flashdata('error'); ?>", "", {
            "closeButton": "true",
            "progressBar": "true"
         });
      </script>
   <?php } ?>

   <?php
   $err = validation_errors();
   $err_msg   = str_replace(array("\r", "\n"), '\n', $err);
   if (isset($err_msg) &&  $err_msg != "") { ?>
      <script type="text/javascript">
         toastr.error("<?php echo $err_msg; ?>", "", {
            "closeButton": "true",
            "progressBar": "true"
         });
      </script>
   <?php  } ?>
   <!--End toster validation -->

</body>

</html>