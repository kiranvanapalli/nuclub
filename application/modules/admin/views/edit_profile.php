<header class="page-header">
   <div class="d-flex align-items-center">
      <div class="container">
         <div class="row">
            <div class="col-lg-8">
               <h1>Profile</h1>
            </div>
            <div class="col-lg-4"><a href="<?= base_url() ?>dashboard" class="btn btn-primary btn-rounded float-right m-r-20">
                  Home</a></div>
         </div>
      </div>
   </div>
</header>
<section class="container-fluid page-content">
   <div class="row">
      <div class="col-md-6">
         <div class="card">
            <h5 class="card-header">Update Profile</h5>
            <form method="POST">
               <div class="card-body">
               <div class="form-group">
                     <label for="admin_name">Full Name</label>
                     <input type="text" class="form-control" id="admin_name" name="admin_name" autocomplete="current-password" placeholder="Full Name" value="<?= $admin_data['user_name'] ?>">
                  </div>
                  <div class="form-group">
                     <label for="admin_email">Email address</label>
                     <input type="email" class="form-control" id="admin_email" name="admin_email" aria-describedby="emailHelp1" autocomplete="email" placeholder="Enter email" value="<?= $admin_data['user_email'] ?>">
                  </div>
                  <div class="form-group">
                     <label for="admin_phone">Mobile Number</label>
                     <input type="text" class="form-control" id="admin_phone" name="admin_phone" autocomplete="mobile-number" placeholder="Mobile Number" value="<?= $admin_data['user_phone'] ?>">
                  </div>
               
               </div>
               <div class="card-footer bg-light">
                  <button class="btn btn-primary" type="submit" name="profile_update" value="profile_update">Update</button>
                  <button type="button" class="btn btn-secondary clear-form">Clear</button>
               </div>
            </form>
         </div>
      </div>
      <div class="col-md-6">
         <div class="card">
            <h5 class="card-header">Update Password</h5>
            <form method="POST" name="update_password" id="update_password">
               <div class="card-body">
               <div class="form-group">
                     <label for="old_pass">Old Password</label>
                     <input type="password" class="form-control" id="old_pass" name="old_pass" autocomplete="current-password" placeholder="Old Password">
                  </div>
                  <div class="form-group">
                     <label for="new_pass">New Password</label>
                     <input type="password" class="form-control" id="new_pass" name="new_pass" aria-describedby="emailHelp1" autocomplete="new_password" placeholder="New Password">
                  </div>
                  <div class="form-group">
                     <label for="confirm_pass">Confirm Password</label>
                     <input type="password" class="form-control" id="confirm_pass" name="confirm_pass"autocomplete="confirm_password" placeholder="Confirm Password">
                  </div>
               
               </div>
               <div class="card-footer bg-light">
                  <button type="submit" class="btn btn-primary" name="password_update" value="password_update">Update</button>
                  <button type="button" class="btn btn-secondary clear-form">Clear</button>
                 <input id="user_id" type="hidden" name="user_id" value="<?= $admin_data['user_id'] ?>">
               </div>
            </form>
         </div>
      </div>
   </div>