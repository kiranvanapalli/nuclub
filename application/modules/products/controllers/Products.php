<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Products extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
        $this->load->model("general_model", "general");
        $this->load->library('my_file_upload');
    }

    public function index()
    {
        $where = "";
        $data['file'] = 'products/list';
        $data['custom_js'] = 'products/all_files_js';
        // $data['validation_js'] = 'admin/all_common_js/frontend_validation_admin';
        $type = "array";
        $category = $this->Allfiles_model->GetDataAll("tb_category", $where, $type, 'category_id', $limit = '');
        $data['category'] = $category;
        $products = $this->Allfiles_model->GetDataAll("tb_products", $where, $type, 'product_id', $limit = '');
        $data['products'] = $products;
        $this->load->view('admin_template/main', $data);
    }

    public function add_view()
    {
        $where = "";
        $type = "array";
        $category = $this->Allfiles_model->GetDataAll("tb_category", $where, $type, 'category_id', $limit = '');
        $data['category'] = $category;
        $data['file'] = 'products/add';
        $data['custom_js'] = 'products/all_files_js';
        $this->load->view('admin_template/main', $data);
    }


    public function save()
    {

        $response = [];
        $number = count($_POST["size"]); 
        $image1 = "";
        $image2 = "";
        $image3 = "";
        $image4 = "";

       
            $config = array(
                'upload_path' => "./uploads/products/",
                'allowed_types' => "png|jpg|jpeg",
                'overwrite' => TRUE,
                'encrypt_name' => TRUE

            );
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image1')) {
                $imgname1 = $this->upload->data();
                $image1 =  $imgname1['file_name'];
                
            }
            if ($this->upload->do_upload('image2')) {
                $imgname2 = $this->upload->data();
                $image2 =  $imgname2['file_name'];
                
            }
            if ($this->upload->do_upload('image3')) {
                $imgname3 = $this->upload->data();
                $image3 =  $imgname3['file_name'];
                
            }
            if ($this->upload->do_upload('image4')) {
                $imgname4 = $this->upload->data();
                $image4 =  $imgname4['file_name'];
                
            }

            $calc_product_count = $this->input->post('available_pieces');

            

            $available_pieces_data = $this->input->post('available_pieces_data');

            if($calc_product_count != "")
            {
                $available_pieces =  $calc_product_count;
            }
            else
            {
                $available_pieces =  $available_pieces_data;
             
            }
        $data = array(
            'product_name' => $this->input->post("product_name"),
            'category' => $this->input->post("category"),
            'product_description' => $this->input->post("product_description"),
            'product_price'=> $this->input->post("product_price"),
            'product_final_price'=> $this->input->post("product_final_price"),
            'coins_can_used'=> $this->input->post("coins_can_used"),
            'available_pieces' => $available_pieces,
            'image1' => $image1,
            'image2' => $image2,
            'image3' => $image3,
            'image4' => $image4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'status' => 1,
        );
        $result = $this->Allfiles_model->data_save("tb_products", $data);
        if ($result) {
            $insert_id = $this->db->insert_id();

            if($number > 0)
            {
                for($i=0; $i<$number;$i++)
                {
                    if(trim($_POST['size'][$i] != ''))
                    {
                        $data = array(

                            'product_id' => $insert_id,
                            'size' => $_POST["size"][$i],
                            'available_pieces' => $_POST["available_pieces_list"][$i],
                            'status' => 1,
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $result = $this->Allfiles_model->data_save("tb_product_sizes",$data);
                    }
                }
            }

            $response = array(
                'status' => true,
                'message' => "Added Successfully"
            );
        } else {
            $response = array(
                'status' => false,
                'message' => "Added Fail"
            );
        }

        echo json_encode($response, true);
    }
    public function edit()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $get_product = $this->Allfiles_model->get_data('tb_products', '*', 'product_id', $id);
            $data['get_product'] = $get_product['resultSet'];
            $where = "";
            $type = "array";
            $category = $this->Allfiles_model->GetDataAll("tb_category", $where, $type, 'category_id', $limit = '');
            $data['category'] = $category;
            $data['file'] = 'products/edit';
            $data['custom_js'] = 'products/all_files_js';
            $this->load->view('admin_template/main', $data);
        }
    }

    public function update()
    {
        if (!empty($_POST['edit_id'])) {

            $response = [];

            $category_image = '';
            if (isset($_FILES['category_image']['name']) && !empty($_FILES['category_image']['name'])) {
                $img_data = array(
                    'img_name' => 'category_image',
                    'img_path' => './uploads/category_images/',
                );
                $category_image = $this->my_file_upload->file_uploads($img_data);
            } else {
                $category_image  = $this->input->post('old_file');
            }

            $where = ['category_id' => $_POST['edit_id']];
            $data = array(
                'category_name' => $this->input->post('category_name'),
                'category_image' => $category_image,
                'status' => $this->input->post('status'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $update_member_details = $this->Allfiles_model->updateData("tb_category", $data, $where);

            if ($update_member_details) {
                $response = array(
                    'status' => true,
                    'message' => "Update Successfully"
                );
            } else {
                $response = array(
                    'status' => false,
                    'message' => "Updated Fail"
                );
            }
        }

        echo json_encode($response, true);
    }



    public function delete()
    {
        $where = ['category_id' => $_POST['id']];
        $result = $this->Allfiles_model->deleteData("tb_category", $where);
        // echo $this->db->last_query();
        echo $result;
    }


    public function get_sizes()
    {
        $res = [];
        if (!empty($_POST['id'])) {
            $where = ['cat_id' => $_POST['id']];
            $type = "array";
            $result = $this->Allfiles_model->GetDataAll("tb_sizes", $where, $type, 'size_id', $limit = '');
            if ($result) {
                $res = array(
                    'status' => true,
                    'size_list' => $result
                );
            } else {
                $res = array(
                    'status' => false,
                );
            }
        }
        echo json_encode($res);
    }
}
