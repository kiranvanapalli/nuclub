<script>
    function previewcover(input) {
        $('#form__img_cover').show();
        $('#form__img').hide();
        form__img_cover.src = URL.createObjectURL(event.target.files[0]);
    }
    $(document).ready(function() {
        j = 1;
        var product_table = $('.product_table').DataTable({});
        $(document).on('submit', '#product_form', function(event) {
            event.preventDefault();
            var product_name = $('#product_name').val();
            var category = $('#category').val();
            var Description = $('#editor1').val();
            var product_price = $('#product_price').val();
            var product_final_price = $('#product_final_price').val();
            var coins_can_used = $('#coins_can_used').val();
            if (product_name == '') {
                toastr["error"]("Please Enter Prodcut Name");
                return false;
            }
            if (category == '') {
                toastr["error"]("Please Select Category");
                return false;
            }
            if (Description == '') {
                toastr["error"]("Please Enter Description");
                return false;
            }
            if (product_price == '') {
                toastr["error"]("Please Enter Product Price");
                return false;
            }
            if (product_final_price == '') {
                toastr["error"]("Please Enter Product Final Price");
                return false;
            }
            if (coins_can_used == '') {
                toastr["error"]("Please Enter Coins Can Used");
                return false;
            }
            $.ajax({
                url: "<?php echo base_url() ?>saveproduct",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    console.log(data);
                    if (data.status == true) {
                        toastr["success"]("Category Data Added Successfully!");
                        window.location.href = "<?php echo base_url(); ?>category";
                    } else {
                        toastr["error"]("Something Went Wrong");
                        return false;
                    }
                }
            });
        });
        $(document).on('submit', '#Updatecategory_form', function(event) {
            event.preventDefault();
            var category_name = $('#category_name').val();

            var id = $('#edit_id').val();
            console.log(id);
            if (category_name == '') {
                toastr["error"]("Please Enter Category Name");
                return false;
            }
            if ($('#status').val() == '') {
                toastr["error"]("Please Select Status");
                return false;
            }
            $.ajax({
                url: "<?php echo base_url() ?>update_cat",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    console.log(data);
                    if (data.status == true) {
                        toastr["success"](data.message);
                        window.location.href = "<?php echo base_url(); ?>category";
                    } else {
                        toastr["error"](data.message);
                        return false;
                    }
                }
            });
        });
        $(document).on('click', '.delete', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            console.log(id);
            $('.modaldelete').attr('value', id)
        })
        $(document).on('click', '.modaldelete', function(event) {
            event.preventDefault();
            var id = $(this).attr('value');
            console.log(id);
            $.ajax({
                url: "<?php echo base_url() ?>delete_member",
                method: "POST",
                data: {
                    member_id: id,
                    <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                },
                success: function(data) {
                    if (data) {
                        toastr["success"]("Member Deleted Successfully");
                        window.location.href = "<?php echo base_url(); ?>members";
                    } else {
                        toastr["error"]("Delete failed! Please try again.");
                        return false;
                    }
                }
            })
        });
        $(document).on('click', '#close', function(event) {
            event.preventDefault();
            asset_table.ajax.reload();
        });

        $('#sweetalert_demo_8').on('click', function(e) {
            var id = $(this).data("id");

            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',

            }, ).then((result) => {
                if (result.value == true) {


                    $.ajax({
                        url: "<?php echo base_url() . 'delete_cat' ?>",
                        method: "POST",
                        data: {
                            id: id,
                            <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                        },
                        success: function(resp) {
                            window.location.href = "<?php echo base_url(); ?>category";
                        }
                    });
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                } else if (result.dismiss == 'cancel') {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });
        $('#category').change(function() {
            id = $(this).val();
            // $('#sizes_table').remove();
          
            $.ajax({
                url: "<?php echo base_url(); ?>get_sizes",
                method: "POST",
                data: {
                    id: id,
                    <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                },
                dataType: 'json',
                success: function(resp) {
                    console.log(resp);
                    if (resp.status == true) {
                        let dropdown = $('#size');
                        dropdown.empty();
                        // $(".pro_qun").show();
                        $(".pro_qun").removeClass("d-none");
                        $("#available_pieces_data").addClass("d-none");
                        var array_list = resp.size_list;
                        $("#size").append('<option value="">Please Select Size</option>')
                        $.each(array_list, function(i, field) {
                            $("#size").append('<option id=' + field.size_id + '>' + field.size + '</option>');
                        })
                    }
                    else
                    {
                        $(".pro_qun").addClass("d-none");
                        $("#available_pieces_data").removeClass("d-none");
                    }
                }
            });

        })
        $('#add_sizes').click(function() {
            id = $('#category').val();
            $.ajax({
                url: "<?php echo base_url(); ?>get_sizes",
                method: "POST",
                data: {
                    id: id,
                    <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                },
                dataType: 'json',
                success: function(resp) {
                    console.log(resp);
                    if (resp.status == true) {
                       
                        
                        var array_list = resp.size_list;

                        console.log(array_list);
                            j++;
                            $('#sizes_table').append(
                                '<tr id="row' + j + '"><td>' + j +
                                '</td><td><select name="size[]" id="size'+j+'"class="form-control size"><option value="">Please Select Size</option></select><td><input type="text" name="available_pieces_list[]" id="available_pieces_list" class="form-control available_pieces_list" placeholder="Enter Quantity"></td><td><button id = "' +
                                j +
                                '" name ="remove" class="btn btn-danger btn_remove">Remove</button></td></tr>'
                            )
                          
                            $.each(array_list, function(i, field) {
                            $("#size"+j).append('<option id=' + field.size_id + '>' + field.size + '</option>');
                        })
                    }
                }
            });
            CalculateGrandTotal();
        });

        $("table").on("keyup", ".available_pieces_list", function () {
             CalculateGrandTotal();
         });

        function CalculateGrandTotal() {
            var grandTotal = 0;
            $.each($('#sizes_table').find('.available_pieces_list'), function () {
            if ($(this).val() != '' && !isNaN($(this).val())) {
                grandTotal += parseFloat($(this).val());
            }
             $('#available_pieces').val(grandTotal);

        });
        }


        $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();
           CalculateGrandTotal();   

      });
    });
    CKEDITOR.replace('editor1');
</script>