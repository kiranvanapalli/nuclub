<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Add Product </h1>
                </div>
                <div class="col-lg-4"><a href="<?= base_url() ?>products" class="btn btn-primary btn-rounded float-right m-r-20">
                        Product List</a></div>
            </div>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form name="product_form" id="product_form">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="product_name">Product Name</label>
                                <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="category">Select Category</label>
                                <select id="category" name="category" class="form-control">
                                    <option selected="">Choose...</option>
                                    <?php

                                    foreach ($category as $category) { ?>
                                        <option value="<?php echo $category['category_id'] ?>"><?php echo $category['category_name']; ?></option>
                                    <?php }

                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label for="product_description">Product Description</label>
                                <textarea name="product_description" id="editor1" class="cke_wrapper" rows="10" cols="80">
                                                    </textarea>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="product_price">Price</label>
                                <input type="text" class="form-control" id="product_price" name="product_price" placeholder="Price">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="product_final_price">Final Price</label>
                                <input type="text" class="form-control" id="product_final_price" name="product_final_price" placeholder="Product Final Price">
                            </div>
                            <!-- <div class="form-group col-md-4">
                                <label for="available_pieces">Available Pieces</label>
                                <input type="text" class="form-control" id="available_pieces" name="available_pieces" placeholder="Available Pieces">
                            </div> -->
                            <div class="form-group col-md-4">
                                <label for="coins_can_used">Coins Can Used</label>
                                <input type="text" class="form-control" id="coins_can_used" name="coins_can_used" placeholder="Coins Can Used">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="d-none available_pieces" id="available_pieces_data">
                                <div class="form-group col-md-12">
                                    <label for="available_pieces">Available Pieces</label>
                                    <input type="text" class="form-control" id="available_pieces_data" name="available_pieces_data" placeholder="Available Pieces">
                                </div>
                            </div>
                            <div class="text-center pro_qun d-none col-md-12">
                                <h5>Product Sizes With Quantity</h5>
                                <button class="btn btn-info add_sizes mb-3" type="button" id="add_sizes" name="add_sizes">Add</button>
                                <table class="table table-bordered sizes_table " id="sizes_table">
                                    <thead>
                                        <tr>
                                            <th>S.no</th>
                                            <th>Size</th>
                                            <th>Quantity</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <tr id="row1">
                                            <td>1</td>
                                            <td>
                                                <select name="size[]" id="size" class="form-control size">
                                                    <option value="">Please Select Size</option>

                                                </select>
                                            </td>
                                            <td><input type="text" name="available_pieces_list[]" id="available_pieces_list" class="form-control available_pieces_list" placeholder="Enter Quantity"></td>
                                            <td><button class="btn btn-danger btn_remove text-center" type="button" id="1" name="remove">Remove</button></td>
                                        </tr>
                                        <input type="hidden" name="available_pieces" id="available_pieces">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="validatedCustomLabel">Product Image 1</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image1" name="image1">
                                    <label class="custom-file-label" for="image1">Choose file...</label>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="img-card">
                                    <img src="./assets/img/demos/hal-gatewood-405338-unsplash.jpg" class="w-100" alt="">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="validatedCustomLabel">Product Image 3</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" multiple="" id="image3">
                                    <label class="custom-file-label" for="image3">Choose file...</label>
                                    <div class="invalid-feedback">Example custom file feedback</div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="img-card">
                                    <img src="./assets/img/demos/hal-gatewood-405338-unsplash.jpg" class="w-100" alt="">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="validatedCustomLabel">Product Image 2</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image2" name="image2">
                                    <label class="custom-file-label" for="image2">Choose file...</label>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="img-card">
                                    <img src="./assets/img/demos/hal-gatewood-405338-unsplash.jpg" class="w-100" alt="">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="validatedCustomLabel">Product Image 4</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image4" name="image4">
                                    <label class="custom-file-label" for="image4">Choose file...</label>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="img-card">
                                    <img src="./assets/img/demos/hal-gatewood-405338-unsplash.jpg" class="w-100" alt="">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="card-footer bg-light text-right mb-4">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-rounded" type="submit">Submit</button>
                                    <button class="btn btn-light btn-rounded btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>