<script>
    function previewcover(input) {
        $('#form__img_cover').show();
        $('#form__img').hide();
        form__img_cover.src = URL.createObjectURL(event.target.files[0]);
    }
    $(document).ready(function() {
        var banner_list = $('.banner_list').DataTable({});

        $(document).on('submit', '#banner_form', function(event) {
            event.preventDefault();
            var name = $('#name').val();
            var image = $('#image').val();

            if (name == '') {
                toastr["error"]("Please Enter Banner Name");
                return false;
            }
            if (image == '') {
                toastr["error"]("Please Select Banner");
                return false;
            }


            $.ajax({
                url: "<?php echo base_url() ?>save_banner",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    console.log(data);
                    if (data.status == true) {
                        toastr["success"](data.message);
                        window.location.href = "<?php echo base_url(); ?>banners";
                    } else {
                        toastr["error"](data.message);
                        return false;
                    }
                }
            });
        });
        $(document).on('submit', '#update_banners_form', function(event) {
            event.preventDefault();
            var name = $('#name').val();
            

            var id = $('#edit_id').val();
            console.log(id);
            if (name == '') {
                toastr["error"]("Please Enter Banner Image");
                return false;
            }
            if ($('#status').val() == '') {
                toastr["error"]("Please Select Status");
                return false;
            }
            $.ajax({
                url: "<?php echo base_url() ?>update_banner",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    console.log(data);
                    if (data.status == true) {
                        toastr["success"](data.message);
                        window.location.href = "<?php echo base_url(); ?>banners";
                    } else {
                        toastr["error"](data.message);
                        return false;
                    }
                }
            });
        });
        // $(document).on('click', '.delete', function(event) {
        //     event.preventDefault();
        //     var id = $(this).data('id');
        //     console.log(id);
        //     $('.modaldelete').attr('value', id)
        // })
        // $(document).on('click', '.modaldelete', function(event) {
        //     event.preventDefault();
        //     var id = $(this).attr('value');
        //     console.log(id);
        //     $.ajax({
        //         url: "<?php echo base_url() ?>delete_member",
        //         method: "POST",
        //         data: {
        //             member_id: id,
        //             <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
        //         },
        //         success: function(data) {
        //             if (data) {
        //                 toastr["success"]("Member Deleted Successfully");
        //                 window.location.href = "<?php echo base_url(); ?>members";
        //             } else {
        //                 toastr["error"]("Delete failed! Please try again.");
        //                 return false;
        //             }
        //         }
        //     })
        // });
        // $(document).on('click', '#close', function(event) {
        //     event.preventDefault();
        //     asset_table.ajax.reload();
        // });

        $('.delete').on('click', function(e) {
            var id = $(this).attr('value');
            console.log(id);
           
            swal({
                title: 'Are you sure?',
                text: "You won't be delete this Banner !",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',

            }, ).then((result) => {
                if (result.value == true) {


                    $.ajax({
                        url: "<?php echo base_url() . 'delete_banner' ?>",
                        method: "POST",
                        data: {
                            id: id,
                            <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                        },
                        success: function(resp) {
                            window.location.href = "<?php echo base_url(); ?>banners";
                        }
                    });
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                } else if(result.dismiss == 'cancel'){
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });
    });
</script>