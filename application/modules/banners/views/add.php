<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Add Banner</h1>
                </div>
                <div class="col-lg-4"><a href="<?= base_url() ?>banners" class="btn btn-primary btn-rounded float-right m-r-20">
                        Banner List</a></div>
            </div>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form name="banner_form" id="banner_form" enctype='multipart/form-data'>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Banner Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Banner Name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="image">Banner Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="image" name="image" onchange="previewcover(this);">
                                    <label class="custom-file-label" for="image">Choose file...</label>
                                    <div class="invalid-feedback">Example custom file feedback</div>
                                </div>
                                <img id="form__img_cover" style="display: none;" src="#" alt="" />
                            </div>
                            <div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light text-right">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-rounded" type="submit">Submit</a>
                                    <button class="btn btn-light btn-rounded btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>