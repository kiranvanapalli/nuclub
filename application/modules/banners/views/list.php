	<!--START PAGE HEADER -->

	<header class="page-header">
		<div class="d-flex align-items-center">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<h1>Banners List</h1>
					</div>
					<div class="col-lg-4"><a href="<?= base_url() ?>add_banner" class="btn btn-primary btn-rounded float-right m-r-20">
							Add Banner</a></div>
				</div>
			</div>
		</div>
	</header>
	<!--END PAGE HEADER -->
	<!--START PAGE CONTENT -->
	<section class="page-content container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body p-20">
						<table id="bs4-table" class="table table-striped table-bordered text-center banner_list" style="width:100%">
							<thead>
								<tr>
									<th>S.no</th>
									<th>Banner Name</th>
									<th>Banner</th>
									<th>Status</th>
									<th>Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $i = 1;
								foreach ($banners as $bannerslist) { ?>
									<tr>
										<td><?= $i++; ?></td>
										<td><?= $bannerslist['name']; ?></td>
										<td style="width: 38%;"><img src="<?php echo $bannerslist['image']; ?>" class="img-responsive img-rounded" style="height:50%; width:50%;" alt=""></td>


										<td class="text-center">
											<?php if ($bannerslist['status'] == 1) {
												$status = "Active";
												$class = "badge-primary";
											} else {
												$status = "In Active";
												$class = "badge-danger";
											}
											echo '<span class="badge ' . $class . ' badge-sm">' . $status . '</span>';
											?>
										</td>
										<td><?= date('d-m-Y', strtotime($bannerslist['created_at'])); ?></td>
										<td class="text-center action">
											<div class="m-0">
												<div class="icon col-xs-6 col-md-3">
													<a href="<?= base_url() ?>edit_banner?id=<?= $bannerslist['banner_id']; ?>" class="tile-primary">
														<i class="zmdi zmdi-edit zmdi-hc-fw zmdi-hc-lg"></i>
													</a>
												</div>
												<div class="icon col-xs-6 col-md-3">
													<a href="javascript:;" id="delete_banner" data-id="<?= $bannerslist['banner_id']; ?>" class="tile-primary delete" value="<?= $bannerslist['banner_id']; ?>">
														<i class="zmdi zmdi-delete zmdi-hc-fw zmdi-hc-lg"></i>
													</a>
												</div>
												<!-- <div class="icon col-md-3"><button id="delete_banner" data-id="<?= $bannerslist['banner_id']; ?>" class="delete btn btn-link"><i class="zmdi zmdi-delete zmdi-hc-fw zmdi-hc-lg"></i></button></div> -->
											</div>
										</td>
									</tr>
								<?php	}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>





	<!--END PAGE CONTENT -->