<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Banners extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
        $this->load->model("general_model", "general");
        $this->load->library('my_file_upload');
    }

    public function index()
    {
        $where = "";
        $data['file'] = 'banners/list';
        $data['custom_js'] = 'banners/all_files_js';
        $type = "array";
        $banners = $this->Allfiles_model->GetDataAll("tb_banners", $where, $type, 'banner_id', $limit = '');
        $data['banners'] = $banners;
        $this->load->view('admin_template/main', $data);
    }

    public function add()
    {
        $data['file'] = 'banners/add';
        $data['custom_js'] = 'banners/all_files_js';
        $this->load->view('admin_template/main', $data);
    }


    public function save()
    {

        $response = [];
        $banner_image = "";

        if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            $config = array(
                'upload_path' => "./uploads/banners/",
                'allowed_types' => "png|jpg|jpeg",
                'overwrite' => TRUE,
                'encrypt_name' => TRUE

            );
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('image')) {
                $imgname = $this->upload->data();
                $banner_image =  $imgname['file_name'];
              
            }
        }
        $image_data = base_url() . "uploads/banners/" . $banner_image;
        $data = array(
            'name' => $this->input->post("name"),
            'image' => $image_data,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'status' => 1,
        );
        $result = $this->Allfiles_model->data_save("tb_banners", $data);
        if ($result) {
            $response = array(
                'status' => true,
                'message' => "Added Successfully"
            );
        } else {
            $response = array(
                'status' => false,
                'message' => "Added Fail"
            );
        }

        echo json_encode($response, true);
    }
    public function edit()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $get_banner_data = $this->Allfiles_model->get_data('tb_banners', '*', 'banner_id', $id);
            $data['get_banner_data'] = $get_banner_data['resultSet'];
            $data['file'] = 'banners/edit';
            $data['custom_js'] = 'banners/all_files_js';
            $this->load->view('admin_template/main', $data);
        }
    }

    public function update()
    {
        if (!empty($_POST['edit_id'])) {

            $response = [];


            if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
                $config = array(
                    'upload_path' => "./uploads/banners/",
                    'allowed_types' => "png|jpg|jpeg",
                    'overwrite' => TRUE,
                    'encrypt_name' => TRUE

                );
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if ($this->upload->do_upload('image')) {
                    $imgname = $this->upload->data();
                    $banner_image =  $imgname['file_name'];
                    $image_data = base_url() . "uploads/banners/" . $banner_image;
                }
            } else {
                $image_data  = $this->input->post('old_file');
            }
            $where = ['banner_id' => $_POST['edit_id']];
            $data = array(
                'name' => $this->input->post('name'),
                'image' => $image_data,
                'status' => $this->input->post('status'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $update_banner_data = $this->Allfiles_model->updateData("tb_banners", $data, $where);

            if ($update_banner_data) {
                $response = array(
                    'status' => true,
                    'message' => "Update Successfully"
                );
            } else {
                $response = array(
                    'status' => false,
                    'message' => "Updated Fail"
                );
            }
        }

        echo json_encode($response, true);
    }

    public function delete()
    {
        $where = ['banner_id' => $_POST['id']];
        $result = $this->Allfiles_model->deleteData("tb_banners", $where);
        // echo $this->db->last_query();
        echo $result;
    }

   
}
