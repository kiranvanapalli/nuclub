<div class="content">
<!--START PAGE HEADER -->
<header class="page-header">
   <div class="d-flex align-items-center">
      <div class="container">
         <div class="row">
            <div class="col-lg-8">
               <h1>User View</h1>
            </div>
            <div class="col-lg-4">
               <button type="button" class="btn btn-primary btn-rounded float-right m-r-20 add_rewards" id="<?= $get_member_details['user_id']; ?>" data-toggle="modal" data-target="#exampleModalCenter">Credits/Rewads</button>
               <button type="button" class="btn btn-primary btn-rounded float-right m-r-20 add_coins" id="<?= $get_member_details['user_id']; ?>" data-toggle="modal" data-target="#exampleModalCenter_Coien">Coine</button>
            </div>
         </div>
      </div>
   </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
<div class="row">
   <div class="col-xl-12">
      <div class="card p-20">
         <div class="profile-wrapper">
            <div class="row">
               <div class="col-lg-12">
                  <ul class="list-reset p-t-10">
                     <h5>Basic Info</h5>
                     <hr>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>NUID : </strong></span><span><?= $get_member_details['nucode']; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Ref ID : </strong></span><span><?php
                           if ($get_member_details['refcode'] != '') {
                               $ref_code = $get_member_details['refcode'];
                           } else {
                               $ref_code = '-';
                           }
                           
                           echo $ref_code; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Name :</strong></span><span><?= $get_member_details['full_name']; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Mobile Mo. :</strong></span><span><?= $get_member_details['mobile_number']; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Email ID :</strong></span><span><?= $get_member_details['email']; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Gender :</strong></span><span><?= $get_member_details['gender']; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Date Of Brith :</strong></span><span><?= date('d-m-Y', strtotime($get_member_details['dob'])); ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>State :</strong></span><span><?= $get_member_details['state']; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>City :</strong></span><span><?= $get_member_details['city']; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Coins :</strong></span><span><?= $get_member_details['nucoins']; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>NuFount :</strong></span><span><?= $get_member_details['nufount']; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Referrals Coins:</strong></span><span><?= $get_member_details['referral_amount']; ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Pin :</strong></span><span><?= $get_member_details['pin']; ?></span>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-xl-12">
      <div class="card p-20">
         <div class="profile-wrapper">
            <div class="row">
               <div class="col-lg-12">
                  <ul class="list-reset p-t-10">
                     <h5>Bank Details</h5>
                     <hr>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Bank Name : </strong></span><span><?php
                           if ($bank_data != "") {
                               $bank_name = $bank_data['bank'];
                           } else {
                               $bank_name = "";
                           }
                           
                           echo $bank_name;
                           
                           ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Branch Name :</strong></span><span><?php
                           if ($bank_data != "") {
                               $branch = $bank_data['branch'];
                           } else {
                               $branch = "";
                           }
                           
                           echo $branch;
                           
                           ?>
                        </span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>IFSC Code :</strong></span><span><?php
                           if ($bank_data != "") {
                               $ifsccode = $bank_data['ifsc_code'];
                           } else {
                               $ifsccode = "";
                           }
                           
                           echo $ifsccode;
                           
                           ?></span>
                     </li>
                     <li class="p-b-10">
                        <span class="w-250 d-inline-block"><strong>Account No. : </strong></span><span><?php
                           if ($bank_data != "") {
                               $account_number = $bank_data['account_number'];
                           } else {
                               $account_number = "";
                           }
                           
                           echo $account_number;
                           
                           ?></span>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-xl-12">
      <div class="card p-20">
         <div class="profile-wrapper">
            <div class="row">
               <div class="col-lg-12">
                  <h5>Credits/Rewads</h5>
                  <table class="table table-striped table-bordered rewards_list" style="width:100%">
                     <thead>
                        <tr>
                           <th>S No.</th>
                           <th>Transaction ID</th>
                           <th>Date</th>
                           <th>Credits/Rewads</th>
                           <th>Transcation Type</th>
                           <th>Remark</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $i = 1;
                           foreach ($rewards_data as $rewards_data) {
                           ?>
                        <tr>
                           <td><?= $i++; ?></td>
                           <td><?= $rewards_data['transaction_id']; ?></td>
                           <td><?= date('d-m-Y', strtotime($rewards_data['created_at'])) ?></td>
                           <td><?= $rewards_data['rewards']; ?></td>
                           <td class="text-center">
                              <?php 
                              
                              if($rewards_data['trasncation_type'] == "cr")
                              {
                                 $status = "Credit";
                                 $class = "badge-success";
                              }
                              else if($rewards_data['trasncation_type'] == "dr")
                              {
                                 $status = "Debit";
                                 $class = "badge-danger";
                              }
                              echo '<span class="badge ' . $class . ' badge-sm">' . $status . '</span>';
                              ?>
                           </td>
                           <td><?= $rewards_data['remark']; ?></td>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-xl-12">
      <div class="card p-20">
         <div class="profile-wrapper">
            <div class="row">
               <div class="col-lg-12">
                  <h5>Coins</h5>
                  <table class="table table-striped table-bordered coins_table" style="width:100%">
                     <thead>
                        <tr>
                           <th>S No.</th>
                           <th>Transaction ID</th>
                           <th>Date</th>
                           <th>Coins</th>
                           <th>Transcation Type</th>
                           <th>Remark</th>
                           
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $i = 1;
                           foreach ($coin_trascations as $coins_data) {
                           ?>
                        <tr>
                           <td><?= $i++; ?></td>
                           <td><?= $coins_data['transaction_id']; ?></td>
                           <td><?= date('d-m-Y', strtotime($coins_data['created_at'])) ?></td>
                           <td><?= $coins_data['coins']; ?></td>
                           <td class="text-center">
                              <?php 
                              
                              if($coins_data['trasncation_type'] == "cr")
                              {
                                 $status = "Credit";
                                 $class = "badge-success";
                              }
                              else if($coins_data['trasncation_type'] == "dr")
                              {
                                 $status = "Debit";
                                 $class = "badge-danger";
                              }
                              echo '<span class="badge ' . $class . ' badge-sm">' . $status . '</span>';
                              ?>
                           </td>
                           <td><?= $coins_data['remark']; ?></td>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-xl-12">
      <div class="card p-20">
         <div class="profile-wrapper">
            <div class="row">
               <div class="col-lg-12">
                  <h5>Referals</h5>
                  <table class="table table-striped table-bordered ref_table" style="width:100%">
                     <thead>
                        <tr>
                           <th>S No.</th>
                           <th>Transaction ID</th>
                           <th>Date</th>
                           <th>Referal Amount</th>
                           <th>Name</th>
                           <th>NU ID</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $i = 1;
                           foreach ($user_info as $ref_data) { ?>
                        <tr>
                           <td><?= $i++; ?></td>
                           <td><?= $ref_data['ref_transcation_id']; ?></td>
                           <td><?= date('d-m-Y', strtotime($ref_data['created_at'])) ?></td>
                           <td><?= $ref_data['ref_amount']; ?></td>
                           <td><?= $ref_data['full_name'] ?></td>
                           <td><?= $ref_data['nucode'] ?></td>
                        </tr>
                        <?php }
                           ?>
                     </tbody>
                     <!--
                        <tfoot>
                        	<tr>
                        		<th>S No.</th>
                        		<th>Title</th>
                        		<th>Category</th>
                        		<th>Final Price</th>
                        		<th>Available Pieces</th>
                        		<th>Status</th>
                        		<th>ACTIONs</th>
                        	</tr>
                        </tfoot>
                        -->
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-xl-12">
      <div class="card p-20">
         <div class="profile-wrapper">
            <div class="row">
               <div class="col-lg-12">
                  <h5>Referals Transcations</h5>
                  <table class="table table-striped table-bordered ref_table" style="width:100%">
                     <thead>
                        <tr>
                           <th>S No.</th>
                           <th>Transaction ID</th>
                           <th>Date</th>
                           <th>Amount Request</th>
                           <th>Status</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $i = 1;
                           foreach ($user_ref_data as $ref_details) { ?>
                        <tr>
                           <td><?= $i++; ?></td>
                           <td><?= $ref_details['transaction_id']; ?></td>
                           <td><?= date('d-m-Y', strtotime($ref_details['created_at'])) ?></td>
                           <td><?= $ref_details['amount']; ?></td>
                           <td class="text-center">
                              <?php 
                              
                              if($ref_details['transaction_type'] == "cr")
                              {
                                 $status = "Credit";
                                 $class = "badge-success";
                              }
                              else if($ref_details['transaction_type'] == "dr")
                              {
                                 $status = "Debit";
                                 $class = "badge-danger";
                              }
                              echo '<span class="badge ' . $class . ' badge-sm">' . $status . '</span>';
                              ?>
                           </td>
                           
                        </tr>
                        <?php }
                           ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-xl-12">
      <div class="card p-20">
         <div class="profile-wrapper">
            <div class="row">
               <div class="col-lg-12">
                  <h5>Withdraw Transcations </h5>
                  <table class="table table-striped table-bordered withdraw_table" style="width:100%">
                     <thead>
                        <tr>
                           <th>S No.</th>
                           <th>Request ID</th>
                           <th>Date</th>
                           <th>Amount</th>
                           <th>Bank</th>
                           <th>Branch</th>
                           <th>Account Number</th>
                           <th>Status</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                           $i = 1;
                           foreach ($withdraw_request as $list) {
                           ?>
                        <tr>
                           <td><?= $i++; ?></td>
                           <td><?= $list['transaction_id']; ?></td>
                           <td><?= date('d-m-Y', strtotime($list['created_at'])) ?></td>
                           <td><?= $list['amount']; ?></td>
                           
                           <td><?= $list['bank']; ?></td>
                           <td><?= $list['branch']; ?></td>
                           <td><?= $list['account_number']; ?></td>
                           <td class="text-center"> <?php if ($list['status'] == 1) {
                                            $status = "Completed";
                                            $class = "badge-primary";
                                        } else if($list['status'] == 2) {
                                            $status = "Pending";
                                            $class = "badge-warning";
                                        }
                                        else if($list['status'] == 2)
                                        {
                                           $status = "Rejected";
                                          $class = "badge-danger";
                                        }
                                        echo '<span class="badge ' . $class . ' badge-sm">' . $status . '</span>';
                                        ?></td>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="ModalTitle1">Credits/Rewads</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="zmdi zmdi-close"></span>
            </button>
         </div>
         <form name="rewards_form" id="rewards_form">
            <div class="modal-body">
               <div class="form-row">
                  <div class="form-group col-md-12">
                     <label for="inputPassword4">Enter No of Credits/Rewads</label>
                     <input type="text" class="form-control" id="rewards" name="rewards" placeholder="Credits/Rewads">
                  </div>
                  <div class="form-group col-md-12">
                     <label for="inputEmail4">Remark</label>
                     <textarea name="remark" id="remark" class="form-control" rows="3" required="required"></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary btn-outline" data-dismiss="modal">Close</button>
               <button type="button" class="btn btn-primary add_rewards_data" id=<?= $get_member_details['user_id']; ?>>Save changes</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="exampleModalCenter_Coien" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="ModalTitle1">Coine</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" class="zmdi zmdi-close"></span>
            </button>
         </div>
         <form name="coins_form" id="coins_form">
            <div class="modal-body">
               <div class="form-row">
                  <div class="form-group col-md-12">
                     <label for="inputPassword4">Enter No of Coine</label>
                     <input type="text" class="form-control coins" id="coins" name="coins" placeholder="Coine">
                  </div>
                  <div class="form-group col-md-12">
                     <label for="inputEmail4">Remark</label>
                     <textarea name="remark" id="remark_coins" class="form-control" rows="3" required="required" placeholder="Enter Comments"></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-secondary btn-outline" data-dismiss="modal">Close</button>
               <button type="button" class="btn btn-primary add_user_coins" id=<?= $get_member_details['user_id']; ?>>Save changes</button>
            </div>
         </form>
      </div>
   </div>
</div>