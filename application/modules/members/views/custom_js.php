<script>
    $(document).ready(function() {

        $(document).on('submit', '#edit_user_form', function(event) {
            event.preventDefault();
            var fullname = $('#fullname').val();
            var email = $('#email').val();
            var mobile_number = $('#mobile_number').val();
            var gender = $('#gender').val();
            var dob = $('#dob').val();
            var state = $('#state').val();
            var city = $('#city').val();
            var pin = $('#pin').val();
            var status = $('#status').val();


            if (fullname == '') {
                toastr["error"]("Full Name is required!");
                return false;
            }
            if (email == '') {
                toastr["error"]("Email Id is required!");
                return false;
            }
            if (mobile_number == '') {
                toastr["error"]("Please Enter Mobile Number!");
                return false;
            }
            if (gender == '') {
                toastr["error"]("Please Select Gender");
                return false;
            }
            if (dob == '') {
                toastr["error"]("Please Select Date Of Birth");
                return false;
            }
            if (state == '') {
                toastr["error"]("Please Select State");
                return false;
            }
            if (city == '') {
                toastr["error"]("Please Enter City Name");
                return false;
            }
            if (pin == '') {
                toastr["error"]("Please Enter Pin");
                return false;
            }
            if (status == '') {
                toastr["error"]("Please Select Status");
                return false;
            }
            if (pin.length < 4) {
                toastr["error"]("Please Enter 4 Digit Pin");
                return false;
            }

            $.ajax({
                url: "<?php echo base_url() ?>update_member",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    if (data) {

                        toastr["success"]("Member Details Added Successfully!");
                        window.location.href = "<?php echo base_url(); ?>members";

                    } else {
                        toastr["error"]("Mobile Number Already Existed");
                        return false;
                    }
                }
            });
        });
    })
</script>