<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Register User</h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-20">
                    <table id="bs4-table" class="table table-striped table-bordered reg_users" style="width:100%">
                        <thead>
                            <tr>
                                <th>NU ID</th>
                                <th>Name</th>
                                <th>Mobile No.</th>
                                <th>Gender</th>
                                <th>Ref By</th>
                                <th>Status</th>
                                <th>Date</th>
                                <th class="text-center">ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($all_members as $all_members) { ?>
                                <tr>
                                    <td><?= $all_members['nucode']; ?></td>
                                    <td><?= $all_members['full_name']; ?></td>
                                    <td><?= $all_members['mobile_number']; ?></td>
                                    <td><?= $all_members['gender']; ?></td>
                                    <td class="text-center"><?php

                                                            if ($all_members['refcode'] == '') {
                                                                $ref_code = "-";
                                                            } else {
                                                                $ref_code = $all_members['refcode'];
                                                            }

                                                            echo $ref_code; ?></td>
                                    <td class="text-center">
                                        <?php if ($all_members['status'] == 1) {
                                            $status = "Active";
                                            $class = "badge-primary";
                                        } else {
                                            $status = "In Active";
                                            $class = "badge-danger";
                                        }
                                        echo '<span class="badge ' . $class . ' badge-sm">' . $status . '</span>';
                                        ?>
                                    </td>

                                    <td><?= date('d-m-Y', strtotime($all_members['created_at'])); ?></td>

                                    <td class="text-center">
                                        <div class="m-0">
                                            <div class="icon col-xs-6 col-md-3" data-name="edit" data-code="f158"><a href="<?= base_url() ?>member_view?id=<?= $all_members['user_id']; ?>" class="tile-primary"><i class="zmdi zmdi-eye zmdi-hc-fw zmdi-hc-lg"></i></div>
                                            <div class="icon col-md-3" data-name="delete" data-code="f154"><a href="<?= base_url() ?>edit_member?id=<?= $all_members['user_id']; ?>" class="tile-primary"><i class="zmdi zmdi-edit zmdi-hc-fw zmdi-hc-lg"></i></a></div>
                                            <!-- <div class="icon col-md-3" data-name="delete" data-code="f154"><a href="<?= base_url() ?>edit_member?id=<?= $all_members['user_id']; ?>" class="tile-primary"><i class="zmdi zmdi-delete zmdi-hc-fw zmdi-hc-lg"></i></a></div> -->
                                        </div>
                                    </td>
                                </tr>

                            <?php }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>