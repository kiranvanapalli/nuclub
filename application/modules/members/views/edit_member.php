<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1><strong>NUCODE : </strong> <?= $get_member_details['nucode']; ?> </h1>
                </div>
                <div class="col-lg-4">
                    <h1><strong>Referral code : </strong> <?php

                                                            if ($get_member_details['refcode'] != "") {
                                                                $ref_code = $get_member_details['refcode'];
                                                            } else {
                                                                $ref_code = "";
                                                            }

                                                            echo $ref_code;
                                                            ?> </h1>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form name="edit_user_form" id="edit_user_form">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="fullname">Full Name</label>
                                <input type="text" name="fullname" class="form-control" id="fullname" placeholder="Full Name" value="<?php echo $get_member_details['full_name']; ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="mobile_number">Mobile Nmber</label>
                                <input type="text" name="mobile_number" class="form-control" id="mobile_number" placeholder="Mobile Number" value="<?php echo $get_member_details['mobile_number']; ?>" onkeypress="return onlyNumberKey(event)">
                            </div>
                        </div>
                        <div class="form-row">

                            <div class="form-group col-md-4">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $get_member_details['email']; ?>">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputState">Date Of Brith</label>
                                <?php
                                $dob = date('d-m-Y', strtotime($get_member_details['dob']))
                                ?>
                                <input type="date" class="form-control" placeholder="Select Date" name="dob" id="dob" value="<?php echo $get_member_details['dob'] ?>">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="gender">Gender</label>
                                <select id="gender" class="form-control" name="gender">
                                    <option value="">Select Gender </option>
                                    <option value="Male" <?php if ($get_member_details['gender'] == "Male") {
                                                            echo 'selected';
                                                        } ?>>Male</option>
                                    <option value="Female" <?php if ($get_member_details['gender'] == "Female") {
                                                            echo 'selected';
                                                        } ?>>Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="inputEmail4">City</label>
                                <input type="text" class="form-control" name="city" placeholder="City" value="<?php echo $get_member_details['city']; ?>">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="inputState">State</label>
                                <select id="state" name="state"class="form-control">
                                    <option selected="">Select State</option>
                                    <?php
                                    foreach ($states as $states) { ?>
                                        <option value="<?php echo $states['state_name']; ?>" <?php
                                                                                                if ($states['state_name'] == $get_member_details['state']) {
                                                                                                    echo 'selected="selected"';
                                                                                                } ?>>
                                            <?php echo $states['state_name'];  ?>
                                        </option>

                                    <?php
                                    } ?>
                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="inputEmail4">Pin</label>
                                <input type="text" class="form-control" name="pin" id="pin" placeholder="Pin" value="<?php echo $get_member_details['pin']; ?>" maxlength="4" minlength="4" onkeypress="return onlyNumberKey(event)">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="inputState">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">Select Status</option>
                                    <option value="1" <?php if ($get_member_details['status'] == "1") {
                                                            echo 'selected';
                                                        } ?>>Active</option>
                                    <option value="0" <?php if ($get_member_details['status'] == "0") {
                                                            echo 'selected';
                                                        } ?>>In Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light text-right">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- <a href="registeruser-view.html" class="btn btn-primary btn-rounded">Submit</a> -->
                                    <button class="btn btn-primary btn-rounded" type="submit">Update</button>
                                    <button class="btn btn-light btn-rounded btn-outline">Cancel</button>
                                    <input class="form-control" type="hidden" name="edit_id" value="<?= $get_member_details['user_id']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>