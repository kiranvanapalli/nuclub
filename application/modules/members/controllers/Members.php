<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Members extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
        $this->load->library('my_file_upload');
    }

    public function index()
    {
        $where =  ["nuclub_user" => 1];
        $data['file'] = 'members/member_list';
        $data['custom_js'] = 'members/all_files_js';
        $data['validation_js'] = 'admin/all_common_js/frontend_validation_admin';
        $type = "array";
        $all_members = $this->Allfiles_model->GetDataAll("tb_users", $where, $type, 'created_at', $limit = '');
        $data['all_members'] = $all_members;
        $this->load->view('admin_template/main', $data);
    }

    public function add_member()
    {
        $data['file'] = 'members/add_member';
        $where = ["status" => 1];
        $type = "array";
        $data['states_list'] = $this->Allfiles_model->GetDataAllmodels("tb_states", $where, $type, 'state_name', $limit = '');
        $data['custom_js'] = 'members/all_files_js';
        $this->load->view('admin_template/main', $data);
    }


    public function member_view()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $where_member = ['ref_memberid' => $id];
            $type = "array";
            $get_member_details = $this->Allfiles_model->get_data('tb_users', '*', 'user_id', $id);
            $data['get_member_details'] = $get_member_details['resultSet'];
            $where = [["column" => "a.ref_memberid =", "value" => $id]];

            $row_type = "array";
            $order_by =  ["column" => "a.ref_id", "Type" => "DESC"];
            $array = [
                "fileds" => "a.*,b.full_name as full_name,b.nucode as nucode,c.transaction_id as transaction_id,c.coins as coins,c.user_id as user_id,c.remark as remark,c.created_at as date, d.account_number as account_number,d.ifsc_code as ifsc_code,d.bank as bank,d.branch as branch",
                "table" => 'tb_referrals as a',
                "join_tables" => [['table' => 'tb_users as b', 'join_on' => 'a.user_id = b.user_id', 'join_type' => 'left'],['table' => 'tb_coins_trascations as c', 'join_on' => 'c.user_id = a.user_id', 'join_type' => 'left'],['table' => 'tb_user_bank_details as d', 'join_on' => 'd.user_id = a.user_id', 'join_type' => 'left']],
                "where" => $where,
                "row_type" => $row_type,
                "order_by" => $order_by,
            ];

            $data['user_info'] = $this->Allfiles_model->GetDataFromJoin($array);
            $where_user = ['user_id' => $id];
            $data['coin_trascations'] = $this->Allfiles_model->GetDataAll("tb_coins_trascations", $where_user, $type, 'id', $limit = '');
            $data['rewards_data'] = $this->Allfiles_model->GetDataAll("tb_rewards_transcations", $where_user, $type, 'id', $limit = '');
            $data['withdraw_request'] = $this->Allfiles_model->GetDataAll("tb_withdraw_request", $where_user, $type, 'withdraw_id', $limit = '');
            $data['user_ref_data'] = $this->Allfiles_model->GetDataAll("tb_referrals_trasncation", $where_user, $type, '', $limit = '');

            // print_r($data['ref_data']);die();
            $bank_details = $this->Allfiles_model->get_data('tb_user_bank_details', '*', 'user_id', $id);
            
            if(isset($bank_details['resultSet']) && !empty($bank_details['resultSet']))
            {
                $data['bank_data'] = $bank_details['resultSet'];
            }
            else
            {
                $data['bank_data'] = "";
            }
            $data['file'] = 'members/member_view';
            $data['custom_js'] = 'members/all_files_js';
            $this->load->view('admin_template/main', $data);
        }
    }

    public function add_member_coins()
    {

        if (isset($_POST['id']) && !empty($_POST['id'])) {
            $id = $_POST['id'];
            $coins = $_POST['coins'];
            $remark = $_POST['remark'];
            $date = current_date();
            $randNumber = mt_rand(11111, 99999);
            $code = 'NUCO' . $randNumber;
            $data =  array(
                'transaction_id' => $code,
                'user_id' => $id,
                'coins' => $coins,
                'trasncation_type' => "cr",
                'remark' => $remark,
                'created_at' => $date,
                'updated_at' => $date,
                'status' => 0
            );

            $result = $this->Allfiles_model->data_save("tb_coins_trascations", $data);

            if($result)
            {
                $fieldname = '';
                $primaryfield = 'user_id';
                $get_member_details = $this->Allfiles_model->get_data("tb_users", $fieldname, $primaryfield, $id);
                $user_data = $get_member_details['resultSet'];
                $get_current_coins_data = $user_data['nucoins'];
                $update_coins = $get_current_coins_data + $coins;

                $data = array(
                    'nucoins' => $update_coins
                );

                $where = ['user_id' => $id];
                $update = $this->Allfiles_model->updateData("tb_users", $data, $where);
            }
            
        }
    }
    public function add_member_rewards()
    {

        if (isset($_POST['id']) && !empty($_POST['id'])) {
            $id = $_POST['id'];
            $rewards = $_POST['rewards'];
            $remark = $_POST['remark'];
            $date = current_date();
            $randNumber = mt_rand(11111, 99999);
            $code = 'NURE' . $randNumber;
            $data =  array(
                'transaction_id' => $code,
                'user_id' => $id,
                'rewards' => $rewards,
                'trasncation_type' => "cr",
                'remark' => $remark,
                'created_at' => $date,
                'updated_at' => $date,
                'status' => 0
            );

            $result = $this->Allfiles_model->data_save("tb_rewards_transcations", $data);

            if($result)
            {
                $fieldname = '';
                $primaryfield = 'user_id';
                $get_member_details = $this->Allfiles_model->get_data("tb_users", $fieldname, $primaryfield, $id);
                $user_data = $get_member_details['resultSet'];
                $get_current_reward_data = $user_data['nufount'];
                $update_reward_data = $get_current_reward_data + $rewards;

                $data = array(
                    'nufount' => $update_reward_data
                );

                $where = ['user_id' => $id];
                $update = $this->Allfiles_model->updateData("tb_users", $data, $where);
            }
            
        }
    }
    public function edit_member()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            //  $where = ['member_id' => $id];
            $where = "";
            $type = "array";
            $get_member_details = $this->Allfiles_model->get_data('tb_users', '*', 'user_id', $id);
            $data['get_member_details'] = $get_member_details['resultSet'];
            $data['states'] = $this->Allfiles_model->GetDataAllmodels("tb_states", $where, $type, 'state_id', $limit = '');
            $data['file'] = 'members/edit_member';
            $data['custom_js'] = 'members/custom_js';
            $data['validation_js'] = 'admin/all_common_js/frontend_validation_admin';
            $this->load->view('admin_template/main', $data);
        }
    }

    public function update_member()
    {
        if (!empty($_POST['edit_id'])) {

            $response = [];
            // $date = $this->input->post('dob');
            // $dob = date("Y-m-d", strtotime($date));
            $where = ['user_id' => $_POST['edit_id']];
            $data = array(
                'full_name' => $this->input->post("fullname"),
                'email' => $this->input->post("email"),
                'mobile_number' => $this->input->post("mobile_number"),
                'gender' => $this->input->post("gender"),
                'dob' => $this->input->post('dob'),
                'state' => $this->input->post("state"),
                'city' => $this->input->post("city"),
                'pin' => $this->input->post("pin"),
                'updated_at' => date('Y-m-d H:i:s'),
                'status' => $this->input->post('status'),
            );

            $update_member_details = $this->Allfiles_model->updateData("tb_users", $data, $where);
            if ($update_member_details) {
                $response = ['status' => 'success'];
            } else {
                $response = ['status' => 'fail'];
            }
            echo json_encode($response);
        }
    }

    public function delete_member()
    {


        $where = ['member_id' => $_POST['member_id']];
        $result = $this->Allfiles_model->deleteData("tb_members", $where);
        // echo $this->db->last_query();
        echo $result;
    }
}
