<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Category extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
        $this->load->model("general_model", "general");
        $this->load->library('my_file_upload');
    }

    public function index()
    {
        $where = "";
        $data['file'] = 'category/list';
        $data['custom_js'] = 'category/all_files_js';
        // $data['validation_js'] = 'admin/all_common_js/frontend_validation_admin';
        $type = "array";
        $category = $this->Allfiles_model->GetDataAll("tb_category", $where, $type, 'category_id', $limit = '');
        $data['category'] = $category;
        $this->load->view('admin_template/main', $data);
    }

    public function add_view()
    {
        $data['file'] = 'category/add';
        $data['custom_js'] = 'category/all_files_js';
        $this->load->view('admin_template/main', $data);
    }


    public function save()
    {

        $response = [];
        $category_image = "";

        if (isset($_FILES['category_image']['name']) && !empty($_FILES['category_image']['name'])) {
            $config = array(
                'upload_path' => "./uploads/category_images/",
                'allowed_types' => "png|jpg|jpeg",
                'overwrite' => TRUE,
                'encrypt_name' => TRUE

            );
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('category_image')) {
                $imgname = $this->upload->data();
                $category_image =  $imgname['file_name'];
            } else {
                $category_image    = "";
            }
        }
        else
        {
            $category_image    = ""; 
        }
        $data = array(
            'category_name' => $this->input->post("category_name"),
            'category_image' => $category_image,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'status' => 1,
        );
        $result = $this->Allfiles_model->data_save("tb_category", $data);
        if($result)
        {
            $response = array(
                'status' => true,
                'message' => "Added Successfully"
            );
        }
        else
        {
            $response = array(
                'status' => false,
                'message' => "Added Fail"
            );
        }

        echo json_encode($response,true);
    }
    public function edit_cat()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
            $get_category = $this->Allfiles_model->get_data('tb_category', '*', 'category_id', $id);
            $data['get_category'] = $get_category['resultSet'];
            $data['file'] = 'category/edit';
            $data['custom_js'] = 'category/all_files_js';
            $this->load->view('admin_template/main', $data);
        }
    }

    public function update()
    {
        if (!empty($_POST['edit_id'])) {

            $response = [];

            $category_image = '';
            if (isset($_FILES['category_image']['name']) && !empty($_FILES['category_image']['name']))  
            { 
             $img_data = array(
                     'img_name' => 'category_image',
                     'img_path' => './uploads/category_images/',
                 );
                 $category_image = $this->my_file_upload->file_uploads($img_data);
             } 
             else 
             {
                   $category_image  = $this->input->post('old_file');
             }
            
            $where = ['category_id' => $_POST['edit_id']];
            $data = array(
                'category_name' => $this->input->post('category_name'),
                'category_image' => $category_image,
                'status' => $this->input->post('status'),
                'updated_at' => date('Y-m-d H:i:s')
            );
            $update_member_details = $this->Allfiles_model->updateData("tb_category", $data, $where);

            if($update_member_details)
            {
                $response = array(
                    'status' => true,
                    'message' => "Update Successfully"
                );
            }
            else
            {
                $response = array(
                    'status' => false,
                    'message' => "Updated Fail"
                );
            }

        }

        echo json_encode($response,true);

            
    }

    public function delete()
    {
        $where = ['category_id' => $_POST['id']];
        $result = $this->Allfiles_model->deleteData("tb_category", $where);
        // echo $this->db->last_query();
        echo $result;
    }
}
