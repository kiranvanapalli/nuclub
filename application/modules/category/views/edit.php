<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Edit Category</h1>
                </div>
                <div class="col-lg-4"><a href="<?= base_url() ?>category" class="btn btn-primary btn-rounded float-right m-r-20">
                        Category List</a></div>
            </div>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form name="Updatecategory_form" id="Updatecategory_form" enctype='multipart/form-data'>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="category_name">Category Name</label>
                                <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Category Name" value="<?= $get_category['category_name']; ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="category_image">Category Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="category_image" name="category_image" onchange="previewcover(this);">
                                    <label class="custom-file-label" for="category_image">Choose file...</label>
                                    <div class="invalid-feedback">Example custom file feedback</div>
                                </div>

                                <?php

                                if ($get_category['category_image'] != "") { ?>
                                    <img id="form__img" src="<?php echo base_url() ?>uploads/category_images/<?php echo $get_category['category_image']; ?>" alt="" />
                                    <input type="hidden" name="old_file" value="<?php echo $get_category['category_image']; ?>">
                                <?php }

                                ?>
                                <img id="form__img_cover" style="display: none;" src="#" alt="" />
                            </div>
                        </div>
                        <div class="form-row">
                        <div class="form-group col-md-3">
                                <label for="inputState">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">Select Status</option>
                                    <option value="1" <?php if ($get_category['status'] == "1") {
                                                            echo 'selected';
                                                        } ?>>Active</option>
                                    <option value="0" <?php if ($get_category['status'] == "0") {
                                                            echo 'selected';
                                                        } ?>>In Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light text-right">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-rounded" type="submit">Update</a>
                                        <button class="btn btn-light btn-rounded btn-outline">Cancel</button>
                                        <input type="hidden" name="edit_id" id="edit_id" value="<?= $get_category['category_id']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>