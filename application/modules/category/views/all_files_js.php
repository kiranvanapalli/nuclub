<script>
    function previewcover(input) {
        $('#form__img_cover').show();
        $('#form__img').hide();
        form__img_cover.src = URL.createObjectURL(event.target.files[0]);
    }
    $(document).ready(function() {
        var tran_table = $('.category_table').DataTable({});

        $(document).on('submit', '#category_form', function(event) {
            event.preventDefault();
            var category_name = $('#category_name').val();

            if (category_name == '') {
                toastr["error"]("Please Enter Category Name");
                return false;
            }


            $.ajax({
                url: "<?php echo base_url() ?>savecategory",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    console.log(data);
                    if (data.status == true) {
                        toastr["success"]("Category Data Added Successfully!");
                        window.location.href = "<?php echo base_url(); ?>category";
                    } else {
                        toastr["error"]("Something Went Wrong");
                        return false;
                    }
                }
            });
        });
        $(document).on('submit', '#Updatecategory_form', function(event) {
            event.preventDefault();
            var category_name = $('#category_name').val();

            var id = $('#edit_id').val();
            console.log(id);
            if (category_name == '') {
                toastr["error"]("Please Enter Category Name");
                return false;
            }
            if ($('#status').val() == '') {
                toastr["error"]("Please Select Status");
                return false;
            }
            $.ajax({
                url: "<?php echo base_url() ?>update_cat",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    console.log(data);
                    if (data.status == true) {
                        toastr["success"](data.message);
                        window.location.href = "<?php echo base_url(); ?>category";
                    } else {
                        toastr["error"](data.message);
                        return false;
                    }
                }
            });
        });
        $(document).on('click', '.delete', function(event) {
            event.preventDefault();
            var id = $(this).data('id');
            console.log(id);
            $('.modaldelete').attr('value', id)
        })
        $(document).on('click', '.modaldelete', function(event) {
            event.preventDefault();
            var id = $(this).attr('value');
            console.log(id);
            $.ajax({
                url: "<?php echo base_url() ?>delete_member",
                method: "POST",
                data: {
                    member_id: id,
                    <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                },
                success: function(data) {
                    if (data) {
                        toastr["success"]("Member Deleted Successfully");
                        window.location.href = "<?php echo base_url(); ?>members";
                    } else {
                        toastr["error"]("Delete failed! Please try again.");
                        return false;
                    }
                }
            })
        });
        $(document).on('click', '#close', function(event) {
            event.preventDefault();
            asset_table.ajax.reload();
        });

        $('#delete_cat').on('click', function(e) {
            var id = $(this).data("id");
           
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',

            }, ).then((result) => {
                if (result.value == true) {


                    $.ajax({
                        url: "<?php echo base_url() . 'delete_cat' ?>",
                        method: "POST",
                        data: {
                            id: id,
                            <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                        },
                        success: function(resp) {
                            window.location.href = "<?php echo base_url(); ?>category";
                        }
                    });
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                } else if(result.dismiss == 'cancel'){
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });
    });
</script>