<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Add Category</h1>
                </div>
                <div class="col-lg-4"><a href="<?= base_url() ?>category" class="btn btn-primary btn-rounded float-right m-r-20">
                        Category List</a></div>
            </div>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form name="category_form" id="category_form" enctype='multipart/form-data'>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="category_name">Category Name</label>
                                <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Category Name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="category_image">Category Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="category_image" name="category_image" onchange="previewcover(this);">
                                    <label class="custom-file-label" for="category_image">Choose file...</label>
                                    <div class="invalid-feedback">Example custom file feedback</div>
                                </div>
                                <img id="form__img_cover" style="display: none;" src="#" alt="" />
                            </div>
                            <div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light text-right">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-rounded" type="submit">Submit</a>
                                    <button class="btn btn-light btn-rounded btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>