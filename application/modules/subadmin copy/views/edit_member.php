<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Edit Sub Admin</h1>
                </div>
                <div class="col-lg-4"><a href="<?= base_url() ?>subadmin" class="btn btn-primary btn-rounded float-right m-r-20">
                        Sub Admin List</a></div>
            </div>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form name="update_subadmin" id="update_subadmin">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="name">Name</label>
                                <input type="name" class="form-control" id="name" name="name" placeholder="Name" value="<?= $get_member_details['user_name']; ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Email ID</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email ID" value="<?= $get_member_details['user_email']; ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="inputEmail4">Mobile Number</label>
                                <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile Number" value="<?= $get_member_details['user_phone']; ?>">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputState">User Type</label>
                                <select id="user_type" name="user_type" class="form-control">
                                    <option selected="">Select Type</option>
                                    <option value="1" selected>Sub Admin</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputState">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">Select Status</option>
                                    <option value="1" <?php if ($get_member_details['status'] == "1") {
                                                            echo 'selected';
                                                        } ?>>Active</option>
                                    <option value="0" <?php if ($get_member_details['status'] == "0") {
                                                            echo 'selected';
                                                        } ?>>In Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light text-right">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-rounded" type="submit">Submit</a>
                                        <button class="btn btn-light btn-rounded btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input class="form-control" type="hidden" name="edit_id" id="edit_id" value="<?= $get_member_details['user_id']; ?>">
                </form>
            </div>
        </div>
    </div>