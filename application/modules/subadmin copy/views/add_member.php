<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Sub Admin</h1>
                </div>
                <div class="col-lg-4"><a href="<?= base_url() ?>subadmin" class="btn btn-primary btn-rounded float-right m-r-20">
                        Sub Admin List</a></div>
            </div>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form name="admin_form" id="admin_form">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Name</label>
                                <input type="name" class="form-control" id="name" name="name" placeholder="Name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Email ID</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email ID">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Mobile Number</label>
                                <input type="text" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile Number">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputState">User Type</label>
                                <select id="user_type" name="user_type" class="form-control">
                                    <option selected="">Select Type</option>
                                    <option value="1">Sub Admin</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light text-right">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-rounded" type="submit">Submit</a>
                                        <button class="btn btn-light btn-rounded btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>