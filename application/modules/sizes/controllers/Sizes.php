<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Sizes extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('admin/admin/is_logged_in');
        $this->load->model('Allfiles_model');
        $this->load->model("general_model", "general");
        $this->load->library('my_file_upload');
    }

    public function index()
    {
        $where = "";
        $data['file'] = 'sizes/list';
        $data['custom_js'] = 'sizes/all_files_js';
        // $data['validation_js'] = 'admin/all_common_js/frontend_validation_admin';
        $type = "array";
        $sizes = $this->Allfiles_model->GetDataAll("tb_sizes", $where, $type, 'size_id', $limit = '');
        $data['sizes'] = $sizes;
        $category = $this->Allfiles_model->GetDataAll("tb_category", $where, $type, 'category_id', $limit = '');
        $data['category'] = $category;
        // print_r($category);die();

        $this->load->view('admin_template/main', $data);
    }

    public function add_view()
    {
        $where = "";
        $type = "array";
        $category = $this->Allfiles_model->GetDataAll("tb_category", $where, $type, 'category_id', $limit = '');
        $data['category'] = $category;
        $data['file'] = 'sizes/add';
        $data['custom_js'] = 'sizes/all_files_js';
        $this->load->view('admin_template/main', $data);
    }


    public function save()
    {

        $response = [];

        $data = array(
            'cat_id' => $this->input->post("category"),
            'size' =>  $this->input->post("size"),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'status' => 1,
        );
        $result = $this->Allfiles_model->data_save("tb_sizes", $data);
        if ($result) {
            $response = array(
                'status' => true,
                'message' => "Added Successfully"
            );
        } else {
            $response = array(
                'status' => false,
                'message' => "Added Fail"
            );
        }

        echo json_encode($response, true);
    }
    public function edit()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $where = "";
            $type = "array";
            $id = $_GET['id'];
            $category = $this->Allfiles_model->GetDataAll("tb_category", $where, $type, 'category_id', $limit = '');
            $data['category'] = $category;
            $get_size = $this->Allfiles_model->get_data('tb_sizes', '*', 'size_id', $id);
            $data['get_size'] = $get_size['resultSet'];
            $data['file'] = 'sizes/edit';
            $data['custom_js'] = 'sizes/all_files_js';
            $this->load->view('admin_template/main', $data);
        }
    }

    public function update()
    {
        if (!empty($_POST['edit_id'])) {
            $response = [];
            $where = ['size_id' => $_POST['edit_id']];
            $data = array(
                'cat_id' => $this->input->post("category"),
                'size' =>  $this->input->post("size"),
                'updated_at' => date('Y-m-d H:i:s'),
                'status' => $this->input->post('status'),
            );
            $update = $this->Allfiles_model->updateData("tb_sizes", $data, $where);

            if ($update) {
                $response = array(
                    'status' => true,
                    'message' => "Update Successfully"
                );
            } else {
                $response = array(
                    'status' => false,
                    'message' => "Updated Fail"
                );
            }
        }
        echo json_encode($response, true);
    }

    public function delete()
    {
        $where = ['size_id' => $_POST['id']];
        $result = $this->Allfiles_model->deleteData("tb_sizes", $where);
        echo $result;
    }
}
