<script>
    function previewcover(input) {
        $('#form__img_cover').show();
        $('#form__img').hide();
        form__img_cover.src = URL.createObjectURL(event.target.files[0]);
    }
    $(document).ready(function() {
        var sizes_table = $('.sizes_table').DataTable({});

        $(document).on('submit', '#sizes_form', function(event) {
            event.preventDefault();
            var category_name = $('#category').val();
            var size = $('#size').val();

            if (category_name == '') {
                toastr["error"]("Please Select Category");
                return false;
            }
            if (size == '') {
                toastr["error"]("Please Enter Size");
                return false;
            }


            $.ajax({
                url: "<?php echo base_url() ?>save_sizes",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    console.log(data);
                    if (data.status == true) {
                        toastr["success"](data.message);
                        window.location.href = "<?php echo base_url(); ?>sizes";
                    } else {
                        toastr["error"](data.message);
                        return false;
                    }
                }
            });
        });
        $(document).on('submit', '#update_size_form', function(event) {
            event.preventDefault();
            var category_name = $('#category').val();
            var size = $('#size').val();
            if (category_name == '') {
                toastr["error"]("Please Select Category");
                return false;
            }
            if (size == '') {
                toastr["error"]("Please Enter");
                return false;
            }
            var id = $('#edit_id').val();
            if ($('#status').val() == '') {
                toastr["error"]("Please Select Status");
                return false;
            }
            $.ajax({
                url: "<?php echo base_url() ?>update_size",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                dataType: 'JSON',
                success: function(data) {
                    console.log(data);
                    if (data.status == true) {
                        toastr["success"](data.message);
                        window.location.href = "<?php echo base_url(); ?>sizes";
                    } else {
                        toastr["error"](data.message);
                        return false;
                    }
                }
            });
        });
        $('#delete_sizes').on('click', function(e) {
            var id = $(this).data("id");
            console.log(id);
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!',

            }, ).then((result) => {
                if (result.value == true) {


                    $.ajax({
                        url: "<?php echo base_url() . 'delete_size' ?>",
                        method: "POST",
                        data: {
                            id: id,
                            <?php echo $this->security->get_csrf_token_name(); ?>: <?php echo "'" . $this->security->get_csrf_hash() . "'"; ?>
                        },
                        success: function(resp) {
                            window.location.href = "<?php echo base_url(); ?>sizes";
                        }
                    });
                    swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                } else if (result.dismiss == 'cancel') {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });
    });
</script>