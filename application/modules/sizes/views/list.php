	<!--START PAGE HEADER -->
	
	<header class="page-header">
		<div class="d-flex align-items-center">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<h1>Sizes List</h1>
					</div>
					<div class="col-lg-4"><a href="<?= base_url() ?>add_sizes" class="btn btn-primary btn-rounded float-right m-r-20">
							Add Sizes</a></div>
				</div>
			</div>
		</div>
	</header>
	<!--END PAGE HEADER -->
	<!--START PAGE CONTENT -->
	<section class="page-content container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body p-20">
						<table id="bs4-table" class="table table-striped table-bordered text-center sizes_table" style="width:100%">
							<thead>
								<tr>
									<th>S.no</th>
									<th>Category Name</th>
									<th>Size</th>
									<th>Status</th>
									<th>Date</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $i = 1;
								foreach ($sizes as $sizes) { ?>
									<tr>
										<td><?= $i++; ?></td>
										<td><?php 
										
										foreach($category as $list)
										{
											if($sizes['cat_id'] == $list['category_id'])
											{
												$cat_name = $list['category_name'];
											}
										}
										?><?php echo $cat_name;?></td>
										<td><?= $sizes['size']; ?></td>
										<td class="text-center">
											<?php if ($sizes['status'] == 1) {
												$status = "Active";
												$class = "badge-primary";
											} else {
												$status = "In Active";
												$class = "badge-danger";
											}
											echo '<span class="badge ' . $class . ' badge-sm">' . $status . '</span>';
											?>
										</td>
										<td><?= date('d-m-Y', strtotime($sizes['created_at'])); ?></td>
										<td class="text-center action">
											<div class="m-0">
												<div class="icon col-xs-6 col-md-3"><a href="<?= base_url() ?>edit_size?id=<?= $sizes['size_id']; ?>" class="tile-primary"><i class="zmdi zmdi-edit zmdi-hc-fw zmdi-hc-lg"></i></div>
												<div class="icon col-md-3"><a href="javascript:;" id="delete_sizes" data-id="<?= $sizes['size_id']; ?>" class="delete"><i class="zmdi zmdi-delete zmdi-hc-fw zmdi-hc-lg"></i></a></div>
											</div>
										</td>
									</tr>
								<?php	}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>	
	</section>


	

	
	<!--END PAGE CONTENT -->