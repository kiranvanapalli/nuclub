<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Edit Size</h1>
                </div>
                <div class="col-lg-4"><a href="<?= base_url() ?>category" class="btn btn-primary btn-rounded float-right m-r-20">
                        Category List</a></div>
            </div>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form name="update_size_form" id="update_size_form">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="category">Category Name</label>
                                <select name="category" id="category" class="form-control">
                                    <option value="">Select Category</option>
                                <?php
                                foreach ($category as $categorys) { ?>
                                    <option value="<?php echo $categorys['category_id']; ?>" <?php
                                                                                                if ($categorys['category_id'] == $get_size['cat_id']) {
                                                                                                    echo 'selected="selected"';
                                                                                                } ?>>
                                        <?php echo $categorys['category_name'];  ?>
                                    </option>

                                <?php
                                } ?>
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="size">Size Name</label>
                                <input type="text" class="form-control" id="size" name="size" placeholder="Size Name" value="<?= $get_size['size']; ?>">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="status">Status</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">Select Status</option>
                                    <option value="1" <?php if ($get_size['status'] == "1") {
                                                            echo 'selected';
                                                        } ?>>Active</option>
                                    <option value="0" <?php if ($get_size['status'] == "0") {
                                                            echo 'selected';
                                                        } ?>>In Active</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light text-right">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-rounded" type="submit">Update</a>
                                        <button class="btn btn-light btn-rounded btn-outline">Cancel</button>
                                        <input type="hidden" name="edit_id" id="edit_id" value="<?= $get_size['size_id']; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>