<header class="page-header">
    <div class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h1>Add Size</h1>
                </div>
                <div class="col-lg-4"><a href="<?= base_url() ?>sizes" class="btn btn-primary btn-rounded float-right m-r-20">
                        Sizes List</a></div>
            </div>
        </div>
    </div>
</header>
<!--END PAGE HEADER -->
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form name="sizes_form" id="sizes_form">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="category">Category Name</label>
                                <select id="category" name="category" class="form-control">
                                    <option selected="">Choose...</option>
                                    <?php

                                    foreach ($category as $category) { ?>
                                        <option value="<?php echo $category['category_id'] ?>"><?php echo $category['category_name']; ?></option>
                                    <?php }

                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="size">Size Name</label>
                                <input type="text" class="form-control" id="size" name="size" placeholder="Size Name">
                            </div>
                            <div>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-light text-right">
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button class="btn btn-primary btn-rounded" type="submit">Submit</a>
                                        <button class="btn btn-light btn-rounded btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>