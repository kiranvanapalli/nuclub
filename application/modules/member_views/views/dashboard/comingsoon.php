<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-sm-10 offset-1">
                <div class="card d-block p-4 text-center">
                    <img src="member_assets/images/logofull.png" alt="" class="text-center w-30">
                    <img src="member_assets/card/SalN.gif" alt="" class="w-70">

                    <a href="<?= base_url() ?>member_dashboard" class="btn btn-primary mb-4 mt-3 w-45" type="submit">Back to Home</a>
                </div>
            </div>
        </div>
    </div>
</div>