<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="border-left border-right-0 border-top-0 card text-success borderthree">
                    <a href="dashboard-from.html">
                        <div class="stat-widget-one card-body">
                            <div class="stat-icon d-inline-block">
                                <i class="border-success icon icon-wallet-90 text-success"></i>
                            </div>
                            <div class="stat-content d-inline-block">
                                <div class="stat-text">Wallet</div>
                                <div class="stat-digit"><i class="fa fa-rupee mr-1"></i><?php echo $wallet ?></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="border-left border-right-0 border-top-0 card text-primary borderthree">
                    <a href="<?= base_url() ?>Referralse">
                        <div class="stat-widget-one card-body">
                            <div class="stat-icon d-inline-block">
                                <i class="border-primary icon icon- icon-users-mm text-primary"></i>
                            </div>
                            <div class="stat-content d-inline-block">
                                <div class="stat-text">Total Referral Income</div>
                                <div class="stat-digit"><i class="fa fa-rupee mr-1"></i><?php echo $get_member_details['points']; ?></div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="border-left border-right-0 border-top-0 card text-warning borderthree">
                    <a href="<?= base_url() ?>comingsoon">
                        <div class="stat-widget-one card-body">
                            <div class="stat-icon d-inline-block">
                                <i class="border-pink text-warning ti-server"></i>
                            </div>
                            <div class="stat-content d-inline-block">
                                <div class="stat-text">Affiliated Market</div>
                                <div class="stat-digit"></div>
                            </div>
                            <img src="member_assets/card/SalN.gif" alt="" class="w-35">
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="border-left border-right-0 border-top-0 card text-danger borderthree">
                <a href="<?php echo base_url() ?>nucoin">
                        <div class="stat-widget-one card-body">
                            <div class="stat-icon d-inline-block">
                                <i class="border-danger text-danger ti-gift"></i>
                            </div>
                            <div class="stat-content d-inline-block">
                            <div class="stat-text">NU Coins</div>
                                <div class="stat-digit"><i class="fa fa-rupee mr-1"></i><?php echo $ref_nucoin; ?></div>
                            </div>
                            </div>
                        </a></div>
                </div>
        </div>
    </div>
</div>