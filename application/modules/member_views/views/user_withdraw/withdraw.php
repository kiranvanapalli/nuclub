<style>
    #loading {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('member_assets/images/loader-1.gif') 56% 50% no-repeat;
    }
</style>
<!-- <div id='loading' style='display:none'>
  <img src='member_assets/images/loader.gif'/>
</div> -->
<div class="loader" id="loading" style="display: none;"></div>
<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <div class="border-left border-right-0 border-top-0 card card-action text-primary borderthree">
                    <div class="stat-widget-one card-body">
                        <div class="stat-icon d-inline-block">
                            <i class="border-primary text-primary ti-user"></i>
                        </div>
                        <div class="stat-content d-inline-block">
                            <div class="stat-text">Referrals Coins</div>
                            <div class="stat-digit"><?php echo $member_details['points']; ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6">
                <div class="border-left border-right-0 border-top-0 card text-danger borderthree">
                    <div class="stat-widget-one card-body">
                        <div class="stat-icon d-inline-block">
                            <i class="border-danger text-danger ti-server"></i>
                        </div>
                        <div class="stat-content d-inline-block">
                            <div class="stat-text">Affiliated Income</div>
                            <div class="stat-digit"><?php echo $member_details['affiliated_rupees']; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-lg-8 col-sm-8 offset-2">
                <div class="border-bottom border-left border-right card p-4">
                    <form name="withdraw_form" id="withdraw_form" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Enter Amont</label>
                                <input type="text" name="amount" id="amount" placeholder="000.00/-" class="form-control" style="font-size: large;font-weight: 600;color: #004193;" onkeypress="return onlyNumberKey(event)">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Debit From</label>
                                <select class="form-control" id="withdraw_from" name="withdraw_from" style="height: 41px;">
                                    <option value="">Debit From...</option>
                                    <option value="1">Referrals Coins</option>
                                    <option value="2">Affiliated Income</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <label>Name</label>
                                <input type="text" placeholder="Account Holder Name" class="form-control" name="name" id="name">
                            </div>
                            <div class="form-group col-md-8">
                                <label>IFSC Code</label>
                                <input type="text" placeholder="IOB152020" class="form-control" name="ifsc_code" id="ifsc_code">
                            </div>
                            <div class="col-md-4 form-group" style="margin-top: 29px !important; ">
                                <!-- <a href="" class="badge-dark d-block mt-4 pb-2 pt-2 text-center">Search</a> -->
                                <!-- <input type="button" class="badge-dark d-block mt-4 pb-2 pt-2 text-center" name="search_ifsc" id="search_ifsc" value="Search IFSC" onclick="getIFSC()"> -->
                                <button type="button" class="btn btn-dark" style="width: 150px; border-radius: 0px !important;" name="search_ifsc" id="search_ifsc" value="Search IFSC" onclick="getIFSC()">Search</button>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Bank Name</label>
                                <input type="text" placeholder="Bank Name" class="form-control" name="bank" id="bank" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Branch Name</label>
                                <input type="email" placeholder="Branch Area" class="form-control" name="branch" id="branch" readonly>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Account Number</label>
                                <input type="text" placeholder="IOB15000002020" class="form-control" name="account_number" id="account_number" onkeypress="return onlyNumberKey(event)">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Confirm Account Number</label>
                                <input type="text" placeholder="IOB15000002020" class="form-control" name="conf_acc_no" id="conf_acc_no" onkeypress="return onlyNumberKey(event)">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary float-md-right">Submit</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>