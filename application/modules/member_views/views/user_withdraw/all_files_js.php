<script>
    function onlyNumberKey(evt) {

        // Only ASCII character in that range allowed
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }

    function getIFSC() {
        var ifsc = $('#ifsc_code').val();
        $('#loading').show();
        $.ajax({
            url: "<?php echo base_url() ?>get_IFSC_Code",
            method: 'POST',
            data: 'ifsc_code=' + ifsc,
            dataType: 'JSON',
            success: function(data) {
                console.log(data);
                if (data.message == "success") {
                    $('#loading').hide();
                    $('#bank').val(data.bank);
                    $('#branch').val(data.branch);
                } else {
                    $('#loading').hide();
                    $('#bank').val('');
                    $('#branch').val('');
                    toastr["error"]("Invalid IFSC Code");
                    return false;
                }
            }
        });
    }
    $(document).ready(function() {
        var withdraw_table = $('#withdraw_table').DataTable({});
        $(document).on('submit', '#withdraw_form', function(event) {
            event.preventDefault();
            var amount = $('#amount').val();
            var withdraw_from = $('#withdraw_from').val();
            var name = $('#name').val();
            var ifsc_code = $('#ifsc_code').val();
            var bank = $('#bank').val();
            var branch = $('#branch').val();
            var account_number = $('#account_number').val();
            var conf_acc_no = $('#conf_acc_no').val();
            if (amount == '') {
                toastr["error"]("Please Enter Amount");
                return false;
            }

            if (withdraw_from == '') {
                toastr["error"]("Please Select Debit Account");
                return false;
            }
            if (name == '') {
                toastr["error"]("Name is required!");
                return false;
            }
            if (ifsc_code == '') {
                toastr["error"]("Please Enter IFSC Code!");
                return false;
            }
            if (bank == '') {
                toastr["error"]("Bank Name is required!");
                return false;
            }
            if (branch == '') {
                toastr["error"]("Branch Name is required");
                return false;
            }
            if (account_number == '') {
                toastr["error"]("Please Enter Account Number");
                return false;
            }
            if (conf_acc_no == '') {
                toastr["error"]("Please Enter Confirm Account Number");
                return false;
            }
            if (account_number != conf_acc_no) {
                toastr["error"]("Account and Confirm Account Not Matched");
                return false;
            }
            if (amount < 1000) {
                toastr["error"]("Please Enter Amount More Then 1000/-");
                return false;
            }
            $.ajax({
                url: "<?php echo base_url() ?>saveWithdraw",
                data: $(this).serialize(),
                method: 'POST',
                dataType: "JSON",
                success: function(res) {
                    console.log(res);
                    if (res.status == "fail") {
                        toastr["error"](res.message);
                    } else {
                        var url = "<?php echo base_url(); ?>withdraw";
                        window.setTimeout(function() {
                            window.location.href = url;
                        });
                    }
                }
            });

        });
    });
</script>