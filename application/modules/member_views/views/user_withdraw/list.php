<div class="content-body">
    <div class="container-fluid">
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Hi, welcome back! <strong><?php echo $member_details['fullname'] ?></strong></h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="mr-3">
                        <div class="mt-2 welcome-text">
                            <h4 class="float-right"><span class="text-black-50">Total Wallet :</span> <i class="fa fa-rupee mr-1"></i> <?php echo $total_amount; ?>/-</h4>
                        </div>
                    </li>
                    <li><a href="<?php echo base_url() ?>withdrawrequest"><span class="badge badge-warning">Withdraw</span></a></li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <!--                                <h4 class="card-title">Affiliated Market Details</h4>-->
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="withdraw_table" class="display text-center withdraw_table" style="min-width: 845px">
                                <thead>
                                    <tr>
                                        <th>Transaction ID</th>
                                        <!--                                                <th>Name</th>-->
                                        <th>Type of Transaction</th>
                                        <th><i class="fa fa-rupee mr-1"></i> Rupees</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($withdraw_list as $withdraw_list) { ?>
                                        <tr>
                                            <td><?php echo $withdraw_list['tr_id']; ?></td>
                                            <!--                                                <td>Name</td>-->
                                            <td>
                                                <?php if ($withdraw_list['withdraw_from'] == 1) {
                                                    echo $withdraw_from = "Referral Income";
                                                } else {
                                                    echo $withdraw_from = "Affiliated Income";
                                                } ?>
                                            </td>
                                            <td><i class="fa fa-rupee mr-1"></i> <strong><?php echo $withdraw_list['amount']; ?>/-</strong></td>
                                            <td><?php echo $withdraw_list['created_at']; ?></td>
                                            <td>
                                                <?php if ($withdraw_list['status'] == 0) {
                                                    $status = "Admin Rejected";
                                                    $class = "badge badge-danger";
                                                } elseif ($withdraw_list['status'] == 1) {
                                                    $status = "Success";
                                                    $class = "badge badge-success";
                                                } elseif ($withdraw_list['status'] == 2) {
                                                    $status = "Pending with Admin";
                                                    $class = "badge badge-warning";
                                                }?>
                                                <span class="<?php echo $class; ?>"><?php echo $status; ?></span>
                                            </td>
                                            
                                            <td><a href="<?php echo base_url() ?>withdrawlist_view?id=<?php echo $withdraw_list['withdraw_id']; ?>"><span class="badge badge-default"><i class="h5 mdi mdi-eye"></i></span></a></td>
                                            
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>