<style>
    #loading {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('member_assets/images/loader-1.gif') 56% 50% no-repeat;
    }
</style>
<!-- <div id='loading' style='display:none'>
  <img src='member_assets/images/loader.gif'/>
</div> -->
<div class="loader" id="loading" style="display: none;"></div>
<div class="content-body">
    <div class="container-fluid">
    <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <div class="welcome-text">
                    <h4>Hi, welcome back! <strong><?php echo $withdraw_list_data['name'] ?></strong></h4>
                </div>
            </div>
            <div class="col-sm-6 p-md-0">
            <div class="welcome-text">
                <h4>Withdraw Transaction Number: <span class="trans" style="color:black; font-size:1.25rem;"><?php echo $withdraw_list_data['tr_id'] ?></span></h4>
       </div>
        </div>
        </div>



        <div class="row">
            <div class="col-lg-8 col-sm-8 offset-2">
                <div class="border-bottom border-left border-right card p-4">
                    <form name="withdraw_form" id="withdraw_form" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label> Amount</label>
                                <input type="text" name="amount" id="amount" placeholder="000.00/-" class="form-control" style="font-size: large;font-weight: 600;color: #004193;" readonly value="<?php echo $withdraw_list_data['amount'] ?>">
                            </div>
                            <div class="form-group col-md-6">
                            <?php if($withdraw_list_data['withdraw_from'] == 1)
                                        {
                                            $debit_from = "Referral Income";
                                        }
                                        elseif($withdraw_list_data['withdraw_from'] == 2)
                                        {
                                            $debit_from = "Affiliated Income";
                                        }
                                         ?>
                                <label>Debit From</label>
                                <input type="text" class="form-control" placeholder="Debit From : Affiliated Income " readonly value="<?php echo $debit_from ?>">

                            </div>
                            <div class="form-group col-md-12">
                                <label>Name</label>
                                <input type="text" placeholder="Account Holder Name" class="form-control" name="name" id="name" value="<?php echo $withdraw_list_data['name'] ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Account Number</label>
                                <input type="text" placeholder="IOB152020" class="form-control" name="ifsc_code" id="ifsc_code" value="<?php echo $withdraw_list_data['account_number'] ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label>IFSC Code</label>
                                <input type="text" placeholder="IOB152020" class="form-control" name="ifsc_code" id="ifsc_code" value="<?php echo $withdraw_list_data['ifsc_code'] ?>">
                            </div>
                           
                            <div class="form-group col-md-6">
                                <label>Bank Name</label>
                                <input type="text" placeholder="Bank Name" class="form-control" name="bank" id="bank" readonly value="<?php echo $withdraw_list_data['bank'] ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Branch Name</label>
                                <input type="email" placeholder="Branch Area" class="form-control" name="branch" id="branch" readonly value="<?php echo $withdraw_list_data['bank'] ?>">
                            </div>
                            <div class="form-group col-md-12">
                            <?php if($withdraw_list_data['status'] == 0)
                                        {
                                            $status = "Rejected";
                                        }
                                        elseif($withdraw_list_data['status'] == 1)
                                        {
                                            $status = "Approved";
                                        }
                                         ?>
                                <label>Admin Status</label>
                                <input type="text" class="form-control" placeholder="Type : Appoved" readonly value="<?php echo $status ?>">
                            </div>
                            <div class="col-lg-12">
                                <label for="">Admin Remark</label>
                                      <textarea id="text" name="text" class="form-control" placeholder="Comment" readonly ><?php echo $withdraw_list_data['remark'] ?></textarea>
                                  </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>