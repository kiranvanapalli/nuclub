<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">NU Coins Details</h4>
                        <h4>Total NU Coins:<?php echo $nu_coins; ?></h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="display text-center" style="min-width: 845px">
                                <thead>
                                    <tr>
                                        <th style="width: 30px !important;" >Sl.No</th>
                                        <th style="width: 90px !important;">TR ID</th>
                                        <!-- <th>Full Name</th>
                                        <th>NU Code</th>
                                        <th>Mobile Number</th> -->
                                        <th style="width: 150px !important;">Debit/Credit</th>
                                        <th style="width: 150px !important;">Date</th>
                                        <th>Remark</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $i=1; 
                                foreach ($all_nu_trascations as $all_nu_trascations) {
                                    ?>
                                    <tr>
                                    <td><?php echo $i++; ?></td>
                                        <td><?php echo $all_nu_trascations['tr_id'] ?></td>
                                        <!-- <td><?php echo $all_nu_trascations['fullname'] ?></td>
                                        <td><?php echo $all_nu_trascations['nucode'] ?></td>
                                        <td><?php echo $all_nu_trascations['mobilenumber'] ?></td> -->
                                        <?php if($all_nu_trascations['credit_debit'] == 1) 
                                        {
                                            $class = "text-success";
                                            $symbol = "+";
                                        }
                                            
                                                else
                                                {
                                                    $class = "text-danger";
                                                    $symbol = "-";
                                                }
                                             ?>

                                        <td class = <?php echo $class ?>><?php echo $symbol." ".$all_nu_trascations['nucoins']  ?></td>
                                        
                                        <td><?php echo $all_nu_trascations['created_at'] ?></td>
                                        <td><?php echo $all_nu_trascations['remark'] ?></td>
                                    </tr>
                                   <?php
                                }
                                   ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>