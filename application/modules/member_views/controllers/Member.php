<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Member extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*if user not loged in redirect to home page*/
        modules::run('frontend_side/Frontend/is_logged_in');
        $this->load->model('Allfiles_model');
    }

    public function index()
    {
        $data['file'] = 'member_views/dashboard/dashboard';
        $member_id = $this->session->userdata('member_id');
        $data['member_name'] = $this->session->userdata('fullname');
        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $data['get_member_details'] = $get_member_details['resultSet'];
        $this->load->view('member_template/main', $data);
    }

    public function Profile()
    {
        $member_id = $this->session->userdata('member_id');
        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $data['get_member_details'] = $get_member_details['resultSet'];
        $where = ['status' => 1];
        $type = "array";
        $data['state_list'] = $this->Allfiles_model->GetDataAllmodels("tb_states", $where, $type, 'state_id', $limit = '');
        $data['file'] = 'member_views/profile/profile';
        $this->load->view('member_template/main', $data);
    }
    public function editProfile()
    {
        $member_id = $this->session->userdata('member_id');
        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $data['get_member_details'] = $get_member_details['resultSet'];
        $where = ['status' => 1];
        $type = "array";
        $data['state_list'] = $this->Allfiles_model->GetDataAllmodels("tb_states", $where, $type, 'state_id', $limit = '');
        $data['file'] = 'member_views/profile/edit_profile';
        $data['custom_js'] = 'member_views/profile/all_files_js';
        $this->load->view('member_template/main', $data);
    }

    public function updateProfile()
    {
        $member_id = $this->session->userdata('member_id');

        $data = array(
            'fullname' => $this->input->post("fullname"),
            'gender' => $this->input->post("gender"),
            'date_of_birth' => $this->input->post("date"),
            'state' => $this->input->post("state"),
            'city' => $this->input->post("city"),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        $result = $this->Allfiles_model->update_profile('tb_members', $data, $member_id);
        echo json_encode($result);
    }
    public function updatePassword()
    {
        if (isset($_POST['password']) && !empty($_POST['password'])) {

            $password = base64_encode(base64_encode($this->input->post('password')));
            $member_id = $this->session->userdata('member_id');
            $data = array(
                'password' => $password,
            );
            $result = $this->Allfiles_model->update_profile('tb_members', $data, $member_id);
            echo json_encode($result);
        }
    }
    public function Wallet()
    {
        $data['file'] = 'member_views/dashboard/wallet';
        $member_id = $this->session->userdata('member_id');
        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $data['get_member_details'] = $get_member_details['resultSet'];
        $data_member_details = $get_member_details['resultSet'];

        $referal_rupees = $data_member_details['points'];
        $data['ref_nucoin'] = $data_member_details['nu_coins'];
        // echo $ref_nucoin;die();
        $affiliated_rupees = $data_member_details['affiliated_rupees'];
        $data['wallet'] = $referal_rupees + $affiliated_rupees;
        // echo $referal_rupees;die();
        $this->load->view('member_template/main', $data);
    }

    public function comingsoon()
    {
        $data['file'] = 'member_views/dashboard/comingsoon';
        $this->load->view('member_template/main', $data);
    }

    public function update_password()
    {
        $data['file'] = 'member_views/update_password/change_password';
        $data['custom_js'] = 'member_views/update_password/all_files_js';
        $this->load->view('member_template/main', $data);
    }

    public function Referralsefview()
    {

        $member_id = $this->session->userdata('member_id');
        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $member_details = $get_member_details['resultSet'];
        $nucode = $member_details['member_code'];
        $member_name = $this->session->userdata('fullname');
        $where = ["member_nucode" => $nucode, 'member_id' => $member_id];
        $type = "array";
        $data['refer_list'] = $this->Allfiles_model->GetDataAllmodels("tb_referrals", $where, $type, 'ref_id', $limit = '');
        $data['file'] = 'member_views/referrals/list';
        $data['custom_js'] = 'member_views/referrals/all_files_js';
        $this->load->view('member_template/main', $data);
    }

    public function saveRefferData()
    {
        $member_id = $this->session->userdata('member_id');
        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $member_details = $get_member_details['resultSet'];
        $nucode = $member_details['member_code'];
        $code = mt_rand(11111, 99999999);
        $tr_id = "NRF" . $code;
        $data = array(
            'member_id' => $member_id,
            'tr_id' => $tr_id,
            'member_nucode' => $nucode,
            'fullname' => $this->input->post('fullname'),
            'email' => $this->input->post('email'),
            'mobilenumber' => $this->input->post('mobilenumber'),
            'points' => 0,
            'status' => 2,
            'created_at' => date('Y-m-d H:i:s'),
        );

        $result = $this->Allfiles_model->data_save('tb_referrals', $data);
        echo json_encode($result);
        if ($result) {
            $insert_id = $this->db->insert_id();
            $get_member_details = $this->Allfiles_model->get_data('tb_referrals', '*', 'ref_id', $insert_id);
            $member_details = $get_member_details['resultSet'];
            $ref_id = $member_details['ref_id'];
            $decode = base64_encode(base64_encode($ref_id));
            $fullname = $member_details['fullname'];
            $email = $member_details['email'];
            $url = base_url() . "Referral?id=" . $decode;
            // Load PHPMailer library
            $this->load->library('phpmailer_lib');
            // PHPMailer object
            $mail = $this->phpmailer_lib->load();
            // SMTP configuration
            $mail->isSMTP();
            $mail->Host = 'sg2plmcpnl485224.prod.sin2.secureserver.net';
            $mail->SMTPDebug = 1;
            $mail->SMTPAuth = true;
            $mail->Username = 'info@nuclubindia.com';
            $mail->Password = 'Adnectics@1222';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;

            $mail->setFrom('info@nuclubindia.com', 'NU CLUB');
            $mail->addReplyTo('info@nuclubindia.com', 'NU CLUB');

            // Add a recipient
            $mail->addAddress($email);

            // Email subject
            $mail->Subject = 'Referral for NU CLUB';

            // Set email format to HTML
            $mail->isHTML(true);

            // Email body content
            $mailContent = "Dear $fullname,<br/>
                    Please find the below Referral Link  here.<br/>";
            $mailContent .= "Your Referral Link is: <strong>" . $url . "</strong> Please Register Your Details";
            $mail->Body = $mailContent;

            // Send email
            if (!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                $this->session->set_flashdata('success_msg', "Mail Sent Succesfully!");
                redirect(base_url());
            }
        }
    }

    public function getReferrals()
    {
        $member_id = $this->session->userdata('member_id');
        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $member_details = $get_member_details['resultSet'];
        $nucode = $member_details['member_code'];
        $where = ["member_code" => $nucode];
        $type = "array";
        $data['refer_list'] = $this->Allfiles_model->GetDataAllmodels("tb_members", $where, $type, 'member_code', $limit = '');
    }

    public function nucoins()
    {
        $member_id = $this->session->userdata('member_id');

        $where = [["column" => "b.member_id =", "value" => $member_id]];

        $row_type = "array";
        $order_by =  ["column" => "a.tr_id", "Type" => "DESC"];
        $array = [
            "fileds" => "a.*,b.member_id as member_id,b.mobilenumber as mobilenumber,b.fullname as fullname, b.member_code as member_code, b.nu_coins as nu_coins",
            "table" => 'tb_nucoin_transaction as a',
            "join_tables" => [['table' => 'tb_members as b', 'join_on' => 'a.member_id = b.member_id', 'join_type' => 'left']],
            "where" => $where,
            "row_type" => $row_type,
            "order_by" => $order_by,
        ];

        $data['all_nu_trascations'] = $this->Allfiles_model->GetDataFromJoin($array);
        // echo $this->db->last_query();die();

        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $member_details = $get_member_details['resultSet'];
        $data['nu_coins'] = $member_details['nu_coins'];

        $data['file'] = 'member_views/nucoins/list';
        $this->load->view('member_template/main', $data);
    }
    public function afiliated()
    {
        $data['file'] = 'member_views/afiliated/list';
        $this->load->view('member_template/main', $data);
    }
    public function delete_member()
    {

        $where = ['member_id' => $_POST['member_id']];
        $result = $this->Allfiles_model->deleteData("tb_members", $where);
        // echo $this->db->last_query();
        echo $result;
    }
    public function withdraw()
    {
        $member_id = $this->session->userdata('member_id');
        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $data['member_details'] = $get_member_details['resultSet'];
        $member_details = $get_member_details['resultSet'];
        $data['total_amount'] = $member_details['points'] + $member_details['affiliated_rupees'];
        $where = ['member_id' => $member_id];
        $type = "array";
        $data['withdraw_list'] = $this->Allfiles_model->GetDataAllmodels("tb_withdraw", $where, $type, 'withdraw_id', $limit = '');
        $data['file'] = "member_views/user_withdraw/list";
        $data['custom_js'] = "member_views/user_withdraw/all_files_js";
        $this->load->view('member_template/main', $data);
    }
    // public function saveWithdraw()
    // {
    //     $data['title'] = 'Withdraw Form';
    //     if ($this->form_validation->run() == FALSE) 
    //     { 
    //         $data['file'] = "member_views/user_withdraw/withdraw";
    //         $this->load->view('member_views/user_withdraw/list',$data);    
    //     } 
    // }

    public function Withdrawview()
    {
        $member_id = $this->session->userdata('member_id');
        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $data['member_details'] = $get_member_details['resultSet'];
        $data['file'] = "member_views/user_withdraw/withdraw";
        $data['custom_js'] = "member_views/user_withdraw/all_files_js";
        $this->load->view('member_template/main', $data);
    }

    public function getifsccode()
    {
        $response = array();
        if (isset($_POST['ifsc_code'])) {
            $ifsc_code = trim($_POST['ifsc_code']);
            $json = @file_get_contents('https://ifsc.razorpay.com/' . $ifsc_code, false);
            if ($json) {
                $arr = json_decode($json);
                if (isset($arr) && !empty($arr)) {
                    $bank = $arr->BANK;
                    $branch = $arr->BRANCH;
                    $response = ['message' => 'success', 'bank' => $bank, 'branch' => $branch];
                } else {
                    $response = ['message' => 'false', 'bank' => '', 'branch' => ''];
                }
            } else {
                $response = ['message' => 'false', 'bank' => '', 'branch' => ''];
            }
        } else {
            $response = ['message' => 'false', 'bank' => '', 'branch' => ''];
        }
        echo json_encode($response);
    }
    public function saveWithdraw()
    {
        $response = array();
        $result = '';
        $message = '';
        $member_id = $this->session->userdata('member_id');
        $get_member_details = $this->Allfiles_model->get_data('tb_members', '*', 'member_id', $member_id);
        $member_details = $get_member_details['resultSet'];
        $ref_coins = $member_details['points'];
        $affiliated_rupees = $member_details['affiliated_rupees'];
        $withdraw_type = $this->input->post('withdraw_from');
        $amount = $this->input->post('amount');
        if ($withdraw_type == 1) {
            if($ref_coins >= $amount)
            {
                $update_wallet = $ref_coins - $amount;
                $data = array(
                    'points' => $update_wallet,
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $where = ['member_id' => $member_id];
                $result = $this->Allfiles_model->updateData('tb_members',$data,$where);
                $response = ['status' => 'success','message' => "Update Wallet Balance"];
            }
            else
            {
                $response = ['status' => 'fail','message' => "Insufficient Coins"];
                $message = "Insufficient Referral Coins";
            }
        }
        else if($withdraw_type == 2)
        {
            if($affiliated_rupees >= $amount)
            {
                $update_wallet = $affiliated_rupees - $amount;
                $data = array(
                    'affiliated_rupees' => $update_wallet,
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $where = ['member_id' => $member_id];
                $result = $this->Allfiles_model->updateData('tb_members',$data,$where);
                $response = ['status' => 'success','message' => "Update Wallet Balance"];
            }
            else
            {
                $response = ['status' => 'fail','message' => "Insufficient Affiliated Rupees"];
                $message = "Insufficient Affiliated Rupees";
            }

        }
        if(!empty($result))
        {
            $code = mt_rand(11111, 99999999);
            $tr_id = "NUW" . $code;
    
            $data = array(
               'tr_id' => $tr_id,
               'member_id' => $member_id,
               'name' => $this->input->post('name'),
               'amount' => $this->input->post('amount'),
               'withdraw_from' => $this->input->post('withdraw_from'),
               'bank' => $this->input->post('bank'),
               'branch'=>$this->input->post('branch'),
               'ifsc_code' => $this->input->post('ifsc_code'),
               'account_number'=> $this->input->post('account_number'),
               'status' => 2,
               'created_at' => date('Y-m-d H:i:s'),
               'updated_at' => date('Y-m-d H:i:s')
            );

            $result = $this->Allfiles_model->data_save('tb_withdraw',$data);
            if($result)
            {
                $response = ['status' => 'success','message' => "Withdraw Request Accepted"];
            }
            else
            {
                $response = ['status' => 'fail','message' => "Data Not Saved"];
                $message ="Data Not Saved";
            }

        }
        else
        {
            $response = ['status' => 'fail','message' => $message];
        }

        echo json_encode($response);
       
    }


    public function withdrawlist_view()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $id = $_GET['id'];
          
            $type = "array";
            $withdraw_list_data = $this->Allfiles_model->get_data('tb_withdraw', '*', 'withdraw_id', $id);
            $data['withdraw_list_data']   = $withdraw_list_data['resultSet'];
            $data['file'] = 'member_views/user_withdraw/withdrawlist_view';
            $data['custom_js'] = 'user_withdraw/all_files_js';
            $this->load->view('member_template/main',$data);
        }
    }
}
