<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH . '/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Transactions extends REST_Controller
{

    private $tbl_users = "tb_users";
    private $tbl_coins_trascations = "tb_coins_trascations";
    private $tbl_rewards_transcations = "tb_rewards_transcations";
    private $tbl_referrals_trasncation = " tb_referrals_trasncation";

    private $tbl_referrals = "tb_referrals";

    private $tbl_withdraw = "tb_withdraw_request";
    private $tbl_querys = "tb_querys";
    private $tbl_banners = "tb_banners";



    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model", "general");
    }


    public function cointrascations_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id = "";
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        $this->form_validation->set_rules('user_id', 'User ID', 'required|trim|numeric');
        $errors = array();
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $where[] = ["column" => "user_id", "value" => $user_id];
            $get_coins_transcations = $this->general->get_all($this->tbl_coins_trascations, '*', $where);
            if (isset($get_coins_transcations['resultSet']) && !empty($get_coins_transcations['resultSet'])) {

                $data = $get_coins_transcations['resultSet'];
            } else {
                $data = [];
            }
        }
        if (isset($errors) && !empty($errors)) {
            $response = array(
                "status" => FALSE,
                "errors" => $errors
            );

            $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Sucess",
                "data" => $data

            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    public function nucredits_rewards_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id = "";
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        $this->form_validation->set_rules('user_id', 'User ID', 'required|trim|numeric');
        $errors = array();
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $where[] = ["column" => "user_id", "value" => $user_id];
            $get_reward_transcations = $this->general->get_all($this->tbl_rewards_transcations, '*', $where);
            if (isset($get_reward_transcations['resultSet']) && !empty($get_reward_transcations['resultSet'])) {

                $data = $get_reward_transcations['resultSet'];
            } else {
                $data = [];
            }
        }
        if (isset($errors) && !empty($errors)) {
            $response = array(
                "status" => FALSE,
                "errors" => $errors
            );

            $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Sucess",
                "data" => $data

            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    public function getReferallData_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id = "";
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        $this->form_validation->set_rules('user_id', 'User ID', 'required|trim|numeric');
        $errors = array();
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $where[] = ["column" => "ref_memberid", "value" => $user_id];
            $get_reffer_transcations = $this->general->get_all($this->tbl_referrals, '*', $where);
            if (isset($get_reffer_transcations['resultSet']) && !empty($get_reffer_transcations['resultSet'])) {

                $data = $get_reffer_transcations['resultSet'];
            } else {
                $data = [];
            }
        }
        if (isset($errors) && !empty($errors)) {
            $response = array(
                "status" => FALSE,
                "errors" => $errors
            );

            $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Sucess",
                "data" => $data

            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    public function getwithdraw_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id = "";
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        $this->form_validation->set_rules('user_id', 'User ID', 'required|trim|numeric');
        $errors = array();
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $where[] = ["column" => "user_id", "value" => $user_id];
            $get_reffer_transcations = $this->general->get_all($this->tbl_withdraw, '*', $where);
            if (isset($get_reffer_transcations['resultSet']) && !empty($get_reffer_transcations['resultSet'])) {

                $data = $get_reffer_transcations['resultSet'];
            } else {
                $data = [];
            }
        }
        if (isset($errors) && !empty($errors)) {
            $response = array(
                "status" => FALSE,
                "errors" => $errors
            );

            $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Sucess",
                "data" => $data

            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }

    public function withdraw_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id = $name = $account_number = $ifsc_code = $bank = $branch = $request_type = $amount = "";
        $created_on  = current_date();


        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }

        if (isset($params['name']) && !empty($params['name'])) {
            $_POST['name'] = $name = $params['name'];
        }

        if (isset($params['account_number']) && !empty($params['account_number'])) {
            $_POST['account_number'] = $account_number = $params['account_number'];
        }

        if (isset($params['ifsc_code']) && !empty($params['ifsc_code'])) {
            $_POST['ifsc_code'] = $ifsc_code = $params['ifsc_code'];
        }
        if (isset($params['bank']) && !empty($params['bank'])) {
            $_POST['bank'] = $bank = $params['bank'];
        }
        if (isset($params['branch']) && !empty($params['branch'])) {
            $_POST['branch'] = $branch = $params['branch'];
        }
        if (isset($params['request_type']) && !empty($params['request_type'])) {
            $_POST['request_type'] = $request_type = $params['request_type'];
        }
        if (isset($params['amount']) && !empty($params['amount'])) {
            $_POST['amount'] = $amount = $params['amount'];
        }
        $errors = array();
        $this->form_validation->set_rules('user_id', 'User ID', 'required|trim|numeric');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('account_number', 'Account Number', 'required|trim|numeric');
        $this->form_validation->set_rules('ifsc_code', 'IFSC Code', 'required');
        $this->form_validation->set_rules('bank', 'Bank Name', 'required');
        $this->form_validation->set_rules('branch', 'Branch Name', 'required');
        $this->form_validation->set_rules('request_type', 'Select Withdraw Account', 'required|trim|numeric');
        $this->form_validation->set_rules('amount', 'Amount', 'required|trim|numeric');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            if ($amount < 1000) {
                $errors['errors'][] = "Withdraw amount must be greater than 1000";
            }
            $response = array();
            if (empty($errors)) {
                $where = [['column' => 'user_id', 'value' => $user_id]];
                $get = $this->general->get_row($this->tbl_users, 'nufount,referral_amount', $where);
                $user_data = $get['resultSet'];
                if ($request_type == 1) {
                    if ($user_data['nufount'] < $amount) {
                        $errors['errors'][] = "Insufficient Funds in NURewards";
                    }
                }
                if ($request_type == 2) {
                    if ($user_data['referral_amount'] < $amount) {
                        $errors['errors'][] = "Insufficient Funds in Refferal Wallet";
                    }
                }

                $randNumber = mt_rand(11111, 99999);
                $code = 'NUWD' . $randNumber;


                if (empty($errors)) {

                    $data = array(

                        'transaction_id' => $code,
                        'user_id' => $user_id,
                        'name' => $name,
                        'account_number' => $account_number,
                        'ifsc_code' => $ifsc_code,
                        'bank' => $bank,
                        'branch' => $branch,
                        'amount' => $amount,
                        'withdraw_wallet' => $request_type

                    );
                    $insert = $this->general->add($this->tbl_withdraw, $data);

                    if ($insert['status'] != true) {
                        $errors['errors'][] = $insert;
                    } else {
                       
                        
                        if ($request_type == 1) {
                            $randNumber = mt_rand(11111, 99999);
                            $code = 'NURE' . $randNumber;
                            $data = array(
                                'transaction_id' => $code,
                                'trasncation_type' => "dr",
                                'user_id' => $user_id,
                                'rewards' => $amount,
                                'remark' => "Debit Request",
                                'status' => 1,
                                'created_at' => current_date()
                            );
                            $insert = $this->general->add($this->tbl_rewards_transcations, $data);
                        } else if ($request_type == 2) {
                            $randNumber = mt_rand(11111, 99999);
                            $code = 'NURF' . $randNumber;
                            $data = array(
                                'transaction_id' => $code,
                                'transaction_type' => "dr",
                                'user_id' => $user_id,
                                'amount' => $amount,
                                'remark' => 'Debit Request',
                                'status' => 1,
                                'created_at' => current_date()
                            );
                            $insert = $this->general->add($this->tbl_referrals_trasncation, $data);
                        }
                        $this->updateWallet($user_id, $amount, $request_type);
                    }
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "WithDraw Request Fail";

            $this->set_response($errors, REST_Controller::HTTP_OK);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "WithDraw Request Raised",

            );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    function updateWallet($user_id, $amount, $request_type)
    {
        $id = $user_id;
        $req_amount = $amount;
        $wallet_type = $request_type;
        $where = [['column' => 'user_id', 'value' => $id]];
        $get = $this->general->get_row($this->tbl_users, '*', $where);
        if (isset($get['resultSet']) && !empty($get['resultSet'])) {
            $user_data = $get['resultSet'];
            if ($request_type == 1) {
                $get_nurewards = $user_data['nufount'];
                $update_wallet = $get_nurewards - $req_amount;
                $data = array(
                    'nufount' => $update_wallet
                );
            }
            if ($request_type == 2) {
                $get_referral_amount = $user_data['referral_amount'];
                $update_wallet = $get_referral_amount - $req_amount;
                $data = array(
                    'referral_amount' => $update_wallet
                );
            }

            $update = $this->general->update($this->tbl_users, $data, $where);
            if ($update['status']) {
                return true;
            } else {
                return false;
            }
        }
    }
    public function query_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id = $subject = $comment = "";
        $created_on  = current_date();
        $response = array();
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }

        if (isset($params['subject']) && !empty($params['subject'])) {
            $_POST['subject'] = $subject = $params['subject'];
        }
        if (isset($params['comment']) && !empty($params['comment'])) {
            $_POST['comment'] = $comment = $params['comment'];
        }
        $errors = array();
        $this->form_validation->set_rules('user_id', 'User ID', 'required|trim|numeric');
        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('comment', 'Comment', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            if (empty($errors)) {
                $data = array(
                    'user_id' => $user_id,
                    'subject' => $subject,
                    'comment' => $comment,
                    'created_at' => $created_on
                );
                $insert = $this->general->add($this->tbl_querys, $data);

                if ($insert['status'] != true) {
                    $errors['errors'][] = $insert;
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Query Not Inserted";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Query Submitted Successfully",
            );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function getbanners_get()
    {

        $where[] = ["column" => "status", "value" => 1];
        $get_banners = $this->general->get_all($this->tbl_banners, '*', $where);
        if (isset($get_banners['resultSet']) && !empty($get_banners['resultSet'])) {

            $data = $get_banners['resultSet'];
        } else {
            $data = "";
        }
        $response = array(
            "status" => TRUE,
            "data" => $data
        );
        $this->set_response($response, REST_Controller::HTTP_OK);
    }
}
