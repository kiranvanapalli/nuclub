<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');
Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH . '/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class bank extends REST_Controller
{

    private $tbl_users = "tb_users";
    private $tbl_bank_details = "tb_user_bank_details";

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model", "general");
    }


    public function addbankdetails_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $user_id = $name = $account_number = $ifsc_code = $bank = $branch =  "";

        $created_on  = current_date();


        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }

        if (isset($params['name']) && !empty($params['name'])) {
            $_POST['name'] = $name = $params['name'];
        }

        if (isset($params['account_number']) && !empty($params['account_number'])) {
            $_POST['account_number'] = $account_number = $params['account_number'];
        }

        if (isset($params['ifsc_code']) && !empty($params['ifsc_code'])) {
            $_POST['ifsc_code'] = $ifsc_code = $params['ifsc_code'];
        }
        if (isset($params['bank']) && !empty($params['bank'])) {
            $_POST['bank'] = $bank = $params['bank'];
        }
        if (isset($params['branch']) && !empty($params['branch'])) {
            $_POST['branch'] = $branch = $params['branch'];
        }




        $errors = array();
        $this->form_validation->set_rules('user_id', 'User ID', 'required|trim|numeric');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('account_number', 'Account Number', 'required|trim|numeric');
        $this->form_validation->set_rules('ifsc_code', 'IFSC Code', 'required');
        $this->form_validation->set_rules('bank', 'Bank Name', 'required');
        $this->form_validation->set_rules('branch', 'Branch Name', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {


            $response = array();
            if (empty($errors)) {
                $data   = array(
                    'user_id' => $user_id,
                    'name' => $name,
                    'account_number' => $account_number,
                    'ifsc_code' => $ifsc_code,
                    'bank' => $bank,
                    'branch' => $branch,
                    'status' => 1,
                    'created_at' => $created_on,
                );
                $insert = $this->general->add($this->tbl_bank_details, $data);

                if ($insert['status'] != true) {
                    $errors['errors'][] = $insert;
                }
            }
        }



        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Bank Details Added Fail";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Bank Details Added Successfully",
                "id" => $insert['insert_id']
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }

    // public function getBankDetails_post()
    // {
    //     $params = json_decode(file_get_contents('php://input'), TRUE);
    //     $user_id = "";

    //     if (isset($params['user_id']) && !empty($params['user_id'])) {
    //         $user_id = $_POST['user_id'] = $params['user_id'];
    //     }
    //     $this->form_validation->set_rules('user_id', 'User ID ', 'required|trim|numeric');
    //     $errors = array();
    //     if ($this->form_validation->run() == FALSE) {
    //         $errors['errors'] = $this->form_validation->error_array();
    //     }
    //     else
    //     {
    //         $response = array();
    //         $order_by = []; 
    //         $where[] = ["column" => "b.user_id","value" =>$_POST['user_id']];   
    //         $like = [];
    //         $limit_start = (isset($_GET['limit_start']))?$_GET['limit_start']:0;
    //         $limit_end = (isset($_GET['limit_end']))?$_GET['limit_end']:0;   
    //         $array = [
    //             "fileds" => "b.*,u.full_name",
    //             "table" => $this->tbl_users.' AS u',
    //             "join_tables" => [
    //                 ["table"=>$this->tbl_bank_details.' AS b',"join_on"=>'b.user_id = u.user_id',"join_type" => "left"]],
    //             "where" => $where,
    //             "like" => $like,
    //             "limit" => ["start" => $limit_start, "end" => $limit_end],
    //             "order_by" => $order_by,
    //         ];       


    //         $p = $this->general->get_all_from_join($array); 
    //         if(isset($p['resultSet']) && !empty($p['resultSet']))
    //         {
    //             $res = [
    //                 'status'=>TRUE,
    //                 'message'=>'',
    //                 'data'=>$p['resultSet']
    //             ];
    //         }
    //         else
    //         {
    //             $res = [
    //                 'status'=>FALSE,
    //                 'message'=>'Data not found',
    //             ];
    //         }
    //         $this->response($res, REST_Controller::HTTP_OK); 

    //     }
    // }
    public function getBankDetails_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id = "";
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        $this->form_validation->set_rules('user_id', 'User ID', 'required|trim|numeric');
        $errors = array();
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $where[] = ["column" => "user_id", "value" => $user_id];
            $get_bank_details = $this->general->get_row($this->tbl_bank_details, '*', $where);
            if (isset($get_bank_details['resultSet']) && !empty($get_bank_details['resultSet'])) {

                $data = array(
                    $get_bank_details['resultSet']
                );
            } else {
                $data = array();
            }
        }
        if (isset($errors) && !empty($errors)) {
            $response = array(
                "status" => FALSE,
                "errors" => $errors
            );

            $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Sucess",
                "data" => $data

            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }


    public function deposits_get($id = null)
    {
        $where = [];
        $like = [];
        if (isset($id) && !empty($id) && is_numeric($id)) {
            $where[] = ["column" => "d.deposite_id", "value" => $id];
        }

        if (isset($_GET['user_id']) && !empty($_GET['user_id'])) {
            $where[] = ["column" => "d.user_id", "value" => $_GET['user_id']];
        }

        if (isset($_GET['transation_id']) && !empty($_GET['transation_id'])) {
            $where[] = ["column" => "d.transation_id", "value" => $_GET['transation_id']];
        }

        $order_by = [];
        $order_by[] =  ["column" => "d.deposite_id", "type" => (isset($_GET['order']) && !empty($_GET['order'])) ? $_GET['order'] : 'DESC'];

        $limit_start = (isset($_GET['limit_start'])) ? $_GET['limit_start'] : 0;
        $limit_end = (isset($_GET['limit_end'])) ? $_GET['limit_end'] : 0;

        $array = [
            "fileds" => "d.deposite_id, d.user_id, d.deposite_amount, d.transation_id, d.transation_response, d.created_on as transtion_on, u.full_name, u.email, u.phone",
            "table" => $this->tbl_deposits . ' AS d',
            "join_tables" => [
                ["table" => $this->tbl_users . ' AS u', "join_on" => 'u.user_id = d.user_id', "join_type" => "left"]
            ],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];


        $p = $this->general->get_all_from_join($array);
        if (isset($p['resultSet']) && !empty($p['resultSet'])) {
            $res = [
                'status' => TRUE,
                'message' => '',
                'data' => $p['resultSet']
            ];
        } else {
            $res = [
                'status' => FALSE,
                'message' => 'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }

    function update_user_wallet($user_id = null, $deposite_amount = null)
    {
        if (isset($user_id, $deposite_amount) && !empty($user_id) && is_numeric($user_id) && !empty($deposite_amount) && is_numeric($deposite_amount)) {
            $where = [['column' => 'user_id', 'value' => $user_id]];
            $get = $this->general->get_row($this->tbl_users, 'wallet_amount', $where);
            if (isset($get['resultSet']) && !empty($get['resultSet'])) {
                $wallet_amount = $get['resultSet']['wallet_amount'];
                $total_amount = $wallet_amount + $deposite_amount;
                $where = [['column' => 'user_id', 'value' => $user_id]];
                $data = ["wallet_amount" => $total_amount];
                $update = $this->general->update($this->tbl_users, $data, $where);
                if ($update['status']) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public function updateBankDetails_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);

        $user_id = $name = $account_number = $ifsc_code = $bank = $branch =  "";

        $created_on  = current_date();


        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }

        if (isset($params['name']) && !empty($params['name'])) {
            $_POST['name'] = $name = $params['name'];
        }

        if (isset($params['account_number']) && !empty($params['account_number'])) {
            $_POST['account_number'] = $account_number = $params['account_number'];
        }

        if (isset($params['ifsc_code']) && !empty($params['ifsc_code'])) {
            $_POST['ifsc_code'] = $ifsc_code = $params['ifsc_code'];
        }
        if (isset($params['bank']) && !empty($params['bank'])) {
            $_POST['bank'] = $bank = $params['bank'];
        }
        if (isset($params['branch']) && !empty($params['branch'])) {
            $_POST['branch'] = $branch = $params['branch'];
        }




        $errors = array();
        $this->form_validation->set_rules('user_id', 'User ID', 'required|trim|numeric');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('account_number', 'Account Number', 'required|trim|numeric');
        $this->form_validation->set_rules('ifsc_code', 'IFSC Code', 'required');
        $this->form_validation->set_rules('bank', 'Bank Name', 'required');
        $this->form_validation->set_rules('branch', 'Branch Name', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {


            $response = array();
            $where = [['column' => 'user_id', 'value' => $user_id]];
            if (empty($errors)) {
                $data   = array(
                    'user_id' => $user_id,
                    'name' => $name,
                    'account_number' => $account_number,
                    'ifsc_code' => $ifsc_code,
                    'bank' => $bank,
                    'branch' => $branch,
                    'status' => 1,
                    'updated_at' => $created_on,
                );
                $update = $this->general->update($this->tbl_bank_details, $data,$where);

                if ($update['status'] != true) {
                    $errors['errors'][] = $update;
                }
            }
        }



        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Bank Details Updated Fail";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Bank Details Updated Successfully"
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
}
