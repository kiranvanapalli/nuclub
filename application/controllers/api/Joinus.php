<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH . '/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Joinus extends REST_Controller
{

    private $tbl_joinus = "tb_joinus";

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model", "general");
        $this->load->model('User_model');
        $this->load->model('Restapi_model');
    }

    // public function userjoinus_post()
    // {
    //     $params = json_decode(file_get_contents('php://input'), TRUE);
    //     $fullname = $mobileno = $email = "";
    //     $date  = current_date();
    //     if (isset($params['fullname']) && !empty($params['fullname'])) {
    //         $fullname = $_POST['fullname'] = $params['fullname'];
    //     }
    //     if (isset($params['mobileno']) && !empty($params['mobileno'])) {
    //         $mobileno = $_POST['mobileno'] = $params['mobileno'];
    //     }

    //     if (isset($params['email']) && !empty($params['email'])) {
    //         $email = $_POST['email'] = $params['email'];
    //     }
       
    //     $errors = array();
    //     $this->form_validation->set_rules('fullname', 'Full Name', 'required');
    //     $this->form_validation->set_rules('mobileno', 'Mobile Number', 'required|trim|numeric|min_length[10]|max_length[10]');
    //     $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tb_users.email]', array('is_unique' => 'This %s already exists. Please login'));
    //     if ($this->form_validation->run() == FALSE) {
    //         $errors['errors'] = $this->form_validation->error_array();
    //     }
    //     else {
    //         $response = array();
    //             $data = array(
    //                 'fullname' => $fullname,
    //                 'email' => $email,
    //                 'mobileno' => $mobileno,
    //                 'created_at' => $date,
    //                 'status' => 1,

    //             );


    //             $insert = $this->general->add($this->tbl_joinus, $data);
    //             $join_user_id = $this->db->insert_id();
    //             if ($insert['status'] != true) {
    //                 $errors['errors'][] = $insert;
    //             }

    //         }
            
        
    //     if (isset($errors) && !empty($errors)) {
    //         $errors['status'] = FALSE;
    //         $errors['message'] = "Join Us Details Added Fail";
    //         $this->set_response($errors, REST_Controller::HTTP_CREATED);
    //     } else {
    //         $response = array(
    //             "status" => TRUE,
    //             "message" => "Join Us Details Added Sucessfully",
    //             "Data" => array(
    //                 "Joinus ID" => $join_user_id, "Full Name" => $fullname, "Mobile Number" => $mobileno
    //             )
    //         );
    //         $this->set_response($response, REST_Controller::HTTP_OK);
    //     }
       
    // }
    

    public function userjoin_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);

        $created_at = $updated_at = current_date();

        $fullname = $email = $mobileno = "";
        if (isset($params['fullname']) && !empty($params['fullname'])) {
            $_POST['fullname'] = $fullname = $params['fullname'];
        }
        if (isset($params['email']) && !empty($params['email'])) {
            $_POST['email'] = $email = $params['email'];
        }
        if (isset($params['mobileno']) && !empty($params['mobileno'])) {
            $_POST['mobileno'] = $mobileno = $params['mobileno'];
        }

        $errors = array();
        $this->form_validation->set_rules('fullname', 'Full Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('mobileno', 'Mobile Number', 'required');



        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        }
        else {
            $response = array();
                $data = array(
                    'fullname' => $fullname,
                    'email' => $email,
                    'mobileno' => $mobileno,
                    'created_at' => $created_at,
                    'status' => 1,

                );


                $insert = $this->general->add($this->tbl_joinus, $data);
                $join_user_id = $this->db->insert_id();
                if ($insert['status'] != true) {
                    $errors['errors'][] = $insert;
                }

            }
            
        
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Join us Details Added Fail";
            $this->set_response($errors, REST_Controller::HTTP_CREATED);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Enquiry Details Added Sucessfully",
                "Data" => array(
                    "Join us ID" => $join_user_id, "Full Name" => $fullname
                )
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }


    public function joinus_user_list_get($id = null)
    {
        $where = [];
        $like = [];
        if (isset($id) && !empty($id) && is_numeric($id)) {
            $where[] = ["column" => "d.join_user_id", "value" => $id];
        }
        $order_by = [];
        $order_by[] =  ["column" => "d.join_user_id", "type" => (isset($_GET['order']) && !empty($_GET['order'])) ? $_GET['order'] : 'DESC'];
        $limit_start = (isset($_GET['limit_start'])) ? $_GET['limit_start'] : 0;
        $limit_end = (isset($_GET['limit_end'])) ? $_GET['limit_end'] : 0;
        $array = [
            "fileds" => "d.*",
            "table" => $this->tbl_joinus . ' AS d',
            "join_tables" => [],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];
        $p = $this->general->get_all_from_join($array);
        if (isset($p['resultSet']) && !empty($p['resultSet'])) {
            $res = [
                'status' => TRUE,
                'message' => '',
                'data' => $p['resultSet']
            ];
        } else {
            $res = [
                'status' => FALSE,
                'message' => 'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }

}