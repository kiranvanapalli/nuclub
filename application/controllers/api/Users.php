<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require(APPPATH . '/libraries/REST_Controller.php');



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Users extends REST_Controller
{

    private $tbl_users = "tb_users";
    private $tbl_states = "tb_states";
    private $tbl_tras = "tb_transaction";
    private $tbl_adminSettings = "admin_settings";
    private $tbl_coins_trascations = "tb_coins_trascations";

    private $tbl_referrals = "tb_referrals";

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("general_model", "general");
    }


    public function users_get($id = null)
    {
        $where = [];
        $like = [];
        if (isset($id) && !empty($id) && is_numeric($id)) {
            $where[] = ["column" => "u.user_id", "value" => $id];
        }

        if (!empty($_GET['status'])) {
            $where[] = ["column" => "u.status", "value" => $_GET['status']];
        }

        $order_by = [];
        $order_by[] =  ["column" => "u.user_id", "type" => (isset($_GET['order']) && !empty($_GET['order'])) ? $_GET['order'] : 'DESC'];

        $limit_start = (isset($_GET['limit_start'])) ? $_GET['limit_start'] : 0;
        $limit_end = (isset($_GET['limit_end'])) ? $_GET['limit_end'] : 0;

        $array = [
            "fileds" => "*",
            "table" => $this->tbl_users . ' AS u',
            "join_tables" => [],
            "where" => $where,
            "like" => $like,
            "limit" => ["start" => $limit_start, "end" => $limit_end],
            "order_by" => $order_by,
        ];


        $p = $this->general->get_all_from_join($array);
        if (isset($p['resultSet']) && !empty($p['resultSet'])) {
            $res = [
                'status' => TRUE,
                'message' => '',
                'data' => $p['resultSet']
            ];
        } else {
            $res = [
                'status' => FALSE,
                'message' => 'Data not found',
            ];
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function UserDataReg_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $full_name = $mobile_number = $email = $dob = $gender = $state = $city = $nucode = $nucoins = $nufount = $refcode = "";
        $date  = current_date();
        if (isset($params['mobile_number']) && !empty($params['mobile_number'])) {
            $mobile_number = $_POST['mobile_number'] = $params['mobile_number'];
        }
        if (isset($params['full_name']) && !empty($params['full_name'])) {
            $full_name = $_POST['full_name'] = $params['full_name'];
        }

        if (isset($params['email']) && !empty($params['email'])) {
            $email = $_POST['email'] = $params['email'];
        }
        if (isset($params['dob']) && !empty($params['dob'])) {
            $dob = $_POST['dob'] = $params['dob'];
        }
        if (isset($params['gender']) && !empty($params['gender'])) {
            $gender = $_POST['gender'] = $params['gender'];
        }
        if (isset($params['state']) && !empty($params['state'])) {
            $state = $_POST['state'] = $params['state'];
        }
        if (isset($params['city']) && !empty($params['city'])) {
            $city = $_POST['city'] = $params['city'];
        }
        $errors = array();
        $this->form_validation->set_rules('full_name', 'Full Name', 'required');
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|trim|numeric|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[tb_users.email]', array('is_unique' => 'This %s already exists. Please login'));
        $this->form_validation->set_rules('dob', 'Date Of Birth', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            $ref_code = "";
            $user_id = "";
            $where = [['column' => 'mobile_number', 'value' => $mobile_number]];
            $get = $this->general->get_row($this->tbl_users, '*', $where);

            if (isset($get['resultSet']) && !empty($get['resultSet'])) {
                $user_id_data = $get['resultSet']['user_id'];
                if (isset($params['refcode']) && !empty($params['refcode'])) {
                    $refcode = $_POST['refcode'] = $params['refcode'];
                    $this->form_validation->set_rules('refcode', 'Referral Code', 'required|min_length[7]|max_length[7]');
                    $where_data = [['column' => 'nucode', 'value' => $refcode]];
                    $get_ref = $this->general->get_row($this->tbl_users, '*', $where_data);

                    if (isset($get_ref['resultSet']) && !empty($get_ref['resultSet'])) {
                        $ref_code = $get_ref['resultSet']['nucode'];
                    } else {
                        $errors[] = "Referral Code Not Valid";
                    }
                }
                if (empty($errors)) {
                    $data = array(
                        "full_name" => $full_name,
                        "email" => $email,
                        "dob" => $dob,
                        "gender" => $gender,
                        "state" => $state,
                        "city" => $city,
                        "created_at" => $date,
                        "status" => 0,
                        "nucoins" => 0,
                        "nufount" => 0,
                        "refcode" => $ref_code

                    );
                    $where[] = ["column" => "mobile_number", "value" => $mobile_number];
                    $update = $this->general->update($this->tbl_users, $data, $where);
                    if ($update['status'] != true) {
                        $errors['errors'][] = $update;
                    } else {
                        // $last_id = $this->db->insert_id();
                        $randNumber = mt_rand(11111, 99999);
                        $order_id = 'OD' . $randNumber;
                        $data =  array(
                            'order_id' => $order_id,
                            'user_id' => $user_id_data
                        );

                        $insert_transcation = $this->general->add($this->tbl_tras, $data);
                        if ($insert_transcation['status'] != true) {
                            $errors['errors'][] = $insert_transcation;
                        }
                    }
                }
            } else {
                $errors[] = "User Not Found";
            }
        }
        if (isset($errors) && !empty($errors)) {
            // $errors['status'] = FALSE;
            // $errors['message'] = $errors;
            $res = array(
                "status" => FALSE,
                "message" => $errors,
            );
            $this->set_response($res, REST_Controller::HTTP_CREATED);
        } else {
            $where = [];
            $get_admindata = $this->general->get_row($this->tbl_adminSettings, '*', $where);

            $admin_data = $get_admindata['resultSet'];
            $amount = $admin_data['amount'];
            $response = array(
                "status" => TRUE,
                "message" => "User Data Updated Successfully",
                "Order ID" => $order_id,
                "Amount" => $amount,
                "User ID" => $user_id_data,
                "data" => array("User Name" => $full_name, "Email" => $email, "Mobile Number" => $mobile_number, "User id" => $user_id_data),

            );
            $this->add_contact_razorpay($response['data']);
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    public function UserReg_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $mobile_number = $otp = "";
        if (isset($params['mobile_number']) && !empty($params['mobile_number'])) {
            $mobile_number = $_POST['mobile_number'] = $params['mobile_number'];
        }
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|trim|numeric|min_length[10]|max_length[10]|is_unique[tb_users.mobile_number]', array('is_unique' => 'This %s already exists. Please login'));
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $contacts = $mobile_number;
            $otp = rand(1000, 9999);
            // print_r($contacts);exit;
            // $sms_text = 'Your OTP is '.$otp.' for Easy Trade Mobile Number validation. Thank you & Happy Trading.';
            // $sms_text = 'Your OTP to register/access ESY TRADE is '. $otp .'It will be valid for 3 minutes. - THANK YOU.';
            $sms_text = 'Your OTP to register/access ESY TRADE is ' . $otp . ' . It will be valid for 3 minutes. - THANK YOU';

            $send = sms_gate_way($contacts, $sms_text);

            $response = array();
            if (empty($errors)) {
                $data = array(
                    'mobile_number' => $mobile_number,
                    'otp' => $otp
                );

                $insert = $this->general->add($this->tbl_users, $data);
                if ($insert['status'] != true) {
                    $errors['errors'][] = $insert;
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Mobile already exist";
            $this->set_response($errors, REST_Controller::HTTP_CREATED);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "OTP Send Successfully",
                "data" => array("Mobile Number" => $mobile_number, "OTP" => $otp),
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    function add_contact_razorpay($data = [])
    {
        if (isset($data) && !empty($data) && is_array($data)) {
            $name = $data["User Name"];
            $email = $data["Email"];
            $user_id = $data["User id"];
            $contact = $data["Mobile Number"];
            $pass_data = [
                "name" => $name,
                "email" => $email,
                "contact" => $contact,
                "notes" => [
                    "user_id" => $user_id
                ]
            ];
            $d = json_encode($pass_data);

            $url = 'https://api.razorpay.com/v1/contacts';
            $curl_pass = array(
                "data" => $d,
                "url" => $url,
                "type" => "POST",
                "header" => [RAZORPAY, 'Content-Type: application/json']
            );
            $response = _curl_request($curl_pass);
            if (!empty($response)) {
                $where = [["column" => "user_id", "value" => $user_id]];
                $data = [
                    "razorpay_response" => json_encode($response),
                    "razorpay_contact_id" => $response['id']
                ];
                $update = $this->general->update($this->tbl_users, $data, $where);
            }
        }
    }

    public function OTPVerify_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $mobile_number = $otp = "";
        if (isset($params['mobile_number']) && !empty($params['mobile_number'])) {
            $mobile_number = $_POST['mobile_number'] = $params['mobile_number'];
        }
        if (isset($params['otp']) && !empty($params['otp'])) {
            $otp = $_POST['otp'] = $params['otp'];
        }
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|trim|numeric|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('otp', 'OTP', 'required|trim|numeric|min_length[4]|max_length[4]');

        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            $where = [['column' => 'mobile_number', 'value' => $mobile_number]];
            $get = $this->general->get_row($this->tbl_users, '*', $where);
            if (isset($get['resultSet']) && !empty($get['resultSet'])) {
                $get_otp = $get['resultSet']['otp'];
                if ($get_otp != $otp) {
                    $errors[] = "Invalid OTP";
                } else {
                    $data = array(
                        'otp_verification' => 1,
                        'status' => 2
                    );
                    $update = $this->general->update($this->tbl_users, $data, $where);
                    if ($update['status'] != true) {
                        $errors['errors'][] = $update;
                    }
                }
            } else {
                $errors[] = "User Not Found";
            }
        }
        if (isset($errors) && !empty($errors)) {
            // $errors['status'] = FALSE;
            // $errors[] = $errors;
            $res = [
                'status' => FALSE,
                'message' => $errors
            ];
            $this->set_response($res, REST_Controller::HTTP_CREATED);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Registration successful",
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    public function resendOTP_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $mobile_number = "";
        if (isset($params['mobile_number']) && !empty($params['mobile_number'])) {
            $mobile_number = $_POST['mobile_number'] = $params['mobile_number'];
        }
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|trim|numeric|min_length[10]|max_length[10]');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            $where = [['column' => 'mobile_number', 'value' => $mobile_number]];
            $get = $this->general->get_row($this->tbl_users, '*', $where);
            if (isset($get['resultSet']) && !empty($get['resultSet'])) {
                $get_otp = $get['resultSet']['otp'];
                $sms_text = 'Your OTP to register/access ESY TRADE is ' . $get_otp . ' . It will be valid for 3 minutes. - THANK YOU';
                $send = sms_gate_way($mobile_number, $sms_text);

                if (!$send) {
                    $errors[] = "OTP Sending Fail";
                }
            } else {
                $errors[] = "User Not Found";
            }
        }
        if (isset($errors) && !empty($errors)) {

            $res = [
                'status' => FALSE,
                'message' => $errors
            ];
            $this->set_response($res, REST_Controller::HTTP_CREATED);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "otp sending successfully",
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    public function getStates_get()
    {
        $response = array();
        $where = [];
        $filed = [];
        $get_all = $this->general->get_all($this->tbl_states, 'state_id,state_name', $where);
        if (isset($get_all['resultSet']) && !empty($get_all['resultSet'])) {
            $state_list = $get_all['resultSet'];
        } else {
            $errors[] = "Data Not Found";
        }
        if (isset($errors) && !empty($errors)) {
            // $errors['status'] = FALSE;
            // $errors[] = $errors;
            $res = [
                'status' => FALSE,
                'message' => $errors
            ];
            $this->set_response($res, REST_Controller::HTTP_CREATED);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "States List",
                "Data" => $state_list
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    public function VerifyRefCode_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $refcode = "";
        if (isset($params['refcode']) && !empty($params['refcode'])) {
            $refcode = $_POST['refcode'] = $params['refcode'];
        }
        $this->form_validation->set_rules('refcode', 'Referral Code ', 'required|trim|min_length[7]|max_length[7]');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            $where_data = [['column' => 'nucode', 'value' => $refcode]];
            $get_ref = $this->general->get_row($this->tbl_users, '*', $where_data);
            if (isset($get_ref['resultSet']) && !empty($get_ref['resultSet'])) {

                $ref_code = $refcode;
            } else {
                $errors[] = "Referral Code Not Valid";
            }
        }
        if (isset($errors) && !empty($errors)) {
            // $errors['status'] = FALSE;
            // $errors[] = $errors;
            $res = [
                'status' => FALSE,
                'message' => $errors
            ];
            $this->set_response($res, REST_Controller::HTTP_CREATED);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Referral Code Found",
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    public function createPin_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $mobile_number = $new_pin = $cofig_pin = "";
        $date  = current_date();
        if (isset($params['mobile_number']) && !empty($params['mobile_number'])) {
            $mobile_number = $_POST['mobile_number'] = $params['mobile_number'];
        }
        if (isset($params['new_pin']) && !empty($params['new_pin'])) {
            $new_pin = $_POST['new_pin'] = $params['new_pin'];
        }
        if (isset($params['cofig_pin']) && !empty($params['cofig_pin'])) {
            $cofig_pin = $_POST['cofig_pin'] = $params['cofig_pin'];
        }


        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|trim|numeric|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('new_pin', 'New Pin', 'required|trim|numeric|min_length[4]|max_length[4]');
        $this->form_validation->set_rules('cofig_pin', 'Confirm New PIN', 'required|trim|numeric|min_length[4]|max_length[4]');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            $where_data = [['column' => 'mobile_number', 'value' => $mobile_number]];
            $get = $this->general->get_row($this->tbl_users, '*', $where_data);
            if (isset($get['resultSet']) && !empty($get['resultSet'])) {

                if ($new_pin != $cofig_pin) {
                    $errors[] = "New Pin And Confirm New PIN Not Matched";
                } else {
                    $data = array(
                        'pin' => $new_pin,
                        'updated_at' => $date
                    );

                    $update = $this->general->update($this->tbl_users, $data, $where_data);
                    if ($update['status'] != true) {
                        $errors['errors'][] = $update;
                    }
                }
            } else {
                $errors[] = "User Not Found";
            }
        }
        if (isset($errors) && !empty($errors)) {
            // $errors['status'] = FALSE;
            // $errors[] = $errors;
            $res = [
                'status' => FALSE,
                'message' => $errors
            ];
            $this->set_response($res, REST_Controller::HTTP_CREATED);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Pin Updated Successfully",
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }

    public function UserLogin_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $selectlogintype = $mobile_number = $email = $nucode = $value = "";
        $date  = current_date();
        if (isset($params['selectlogintype']) && !empty($params['selectlogintype'])) {
            $selectlogintype = $_POST['selectlogintype'] = $params['selectlogintype'];
        }
        $this->form_validation->set_rules('selectlogintype', 'Select Login Type', 'required|trim|numeric');

        if ($selectlogintype == 1) {
            if (isset($params['value']) && !empty($params['value'])) {
                $value = $_POST['value'] = $params['value'];
            }
            $this->form_validation->set_rules('value', 'Mobile Number', 'required|trim|numeric|min_length[10]|max_length[10]');
        } elseif ($selectlogintype == 2) {
            $email = "";
            if (isset($params['value']) && !empty($params['value'])) {
                $value = $_POST['value'] = $params['value'];
            }
            $this->form_validation->set_rules('value', 'Email', 'required|trim|valid_email');
        } elseif ($selectlogintype == 3) {
            $nucode = "";
            if (isset($params['value']) && !empty($params['value'])) {
                $value = $_POST['value'] = $params['value'];
            }
            $this->form_validation->set_rules('value', 'Member Code', 'required|trim|min_length[7]|max_length[7]');
        }
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            $amount = '0';
            $sql = "SELECT * FROM `tb_users` WHERE mobile_number = '$value' OR email = '$value' OR nucode = '$value'";
            $res = $this->general->custom_query($sql, 'row');
            if (isset($res['resultSet']) && !empty($res['resultSet'])) {
                $data = $res['resultSet'];
                if ($data['payment_status'] != "Sucess") {
                    $where = [];
                    $get_admindata = $this->general->get_row($this->tbl_adminSettings, '*', $where);

                    $admin_data = $get_admindata['resultSet'];
                    $amount = $admin_data['amount'];
                }
            } else {
                $errors[] = "No User Found";
            }
        }
        if (isset($errors) && !empty($errors)) {
            // $errors['status'] = FALSE;
            // $errors[] = $errors;
            $res = [
                'status' => FALSE,
                'message' => $errors
            ];
            $this->set_response($res, REST_Controller::HTTP_CREATED);
        } else {
            $response = array(
                "status" => TRUE,
                "data" => $data,
                "Amount" => $amount
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }



    public function GetLoginType_get()
    {
        $response = array();
        $details = array();
        $a = array();
        // $a = [
        //     1 => "Mobile No",
        //     2 => "Email ID",
        //     3 => "Member ID"

        // ];
        $a['id'] = "1";
        $a['login_type'] = "Mobile No";
        $details[] = $a;
        $a['id'] = "2";
        $a['login_type'] = "Email ID";
        $details[] = $a;
        $a['id'] = "3";
        $a['login_type'] = "Member ID";
        $details[] = $a;


        // print_r($json_data);
        $response = array(
            "status" => TRUE,
            "data" => $details
        );
        $this->set_response($response, REST_Controller::HTTP_OK);
    }
    public function updateUserTranscation_post()
    {

        $params = json_decode(file_get_contents('php://input'), TRUE);
        $transactionID = $transactionAmount = $transactionMode = $customerID = $requestID = $transactionStatus = "";
        $date  = current_date();

        if (isset($params['transactionID']) && !empty($params['transactionID'])) {
            $transactionID = $_POST['transactionID'] = $params['transactionID'];
        }
        if (isset($params['transactionAmount']) && !empty($params['transactionAmount'])) {
            $transactionAmount = $_POST['transactionAmount'] = $params['transactionAmount'];
        }
        if (isset($params['transactionMode']) && !empty($params['transactionMode'])) {
            $transactionMode = $_POST['transactionMode'] = $params['transactionMode'];
        }
        if (isset($params['customerID']) && !empty($params['customerID'])) {
            $customerID = $_POST['customerID'] = $params['customerID'];
        }
        if (isset($params['requestID']) && !empty($params['requestID'])) {
            $requestID = $_POST['requestID'] = $params['requestID'];
        }
        if (isset($params['transactionStatus']) && !empty($params['transactionStatus'])) {
            $transactionStatus = $_POST['transactionStatus'] = $params['transactionStatus'];
        }
        $user_id = $customerID;
        $this->form_validation->set_rules('transactionID', 'Transcation ID', 'required|trim');
        $this->form_validation->set_rules('transactionAmount', 'Transcation Amount', 'required|trim');
        $this->form_validation->set_rules('transactionMode', 'Transcation Mode', 'required|trim');
        $this->form_validation->set_rules('customerID', 'Customer ID', 'required|trim');
        $this->form_validation->set_rules('requestID', 'RequestID', 'required|trim');
        $this->form_validation->set_rules('transactionStatus', 'Transcation Status', 'required|trim');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $response = array();
            // $where_data = [[ "column" =>"user_id", "value" => $user_id]];
            // $where_data[] = ["column" => "user_id", "value" => $user_id ];
            // $where_member = ['user_id' => $user_id];
            // $where[] = ["column"=> "user_id","value" => $user_id];
            $where_data = [['column' => 'user_id', 'value' => $user_id]];
            $data = array(
                "transactionID" => $transactionID,
                "transactionAmount" => $transactionAmount,
                "transactionMode" => $transactionMode,
                "customerID" => $customerID,
                "requestID" => $requestID,
                "transactionStatus" => $transactionStatus

            );
            // $add = $this->general->add($this->tbl_tras,$data);
            $update = $this->general->update($this->tbl_tras, $data, $where_data);
            // echo $this->db->last_query();die();
            $where[] = ["column" => "user_id", "value" => $user_id];
            $data_user = array(
                'payment_status' => $transactionStatus,
                'nuclub_user' => 1,
                'status' => 1
            );
            $update_user = $this->general->update($this->tbl_users, $data_user, $where);
            if ($update['status'] != true) {
                $errors['errors'][] = $update;
            } else {
                if ($update_user['status'] != true) {
                    $errors['errors'][] = $update_user;
                } else {
                    $where = [['column' => 'user_id', 'value' => $user_id]];
                    $user_data = $this->general->get_row($this->tbl_users, '*', $where);
                    $admin_data = $user_data['resultSet'];
                    $this->generatecode($admin_data);
                    $this->addbonus($admin_data);
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            // $errors['status'] = FALSE;
            // $errors[] = $errors;
            $res = [
                'status' => FALSE,
                'message' => $errors
            ];
            $this->set_response($res, REST_Controller::HTTP_CREATED);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => "Transcation Updated"
            );
            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
    public function order_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);

        $amount = $currency = $receipt = "";

        $created_on  = current_date();

        if (isset($params['amount']) && !empty($params['amount'])) {
            $_POST['amount'] = $amount = $params['amount'];
        }
        if (isset($params['currency']) && !empty($params['currency'])) {
            $_POST['currency'] = $currency = $params['currency'];
        }
        if (isset($params['receipt']) && !empty($params['receipt'])) {
            $_POST['receipt'] = $receipt = $params['receipt'];
        }


        $errors = array();
        $this->form_validation->set_rules('amount', 'amount', 'required|trim|numeric');
        $this->form_validation->set_rules('currency', 'currency', 'required|trim');
        $this->form_validation->set_rules('receipt', 'receipt', 'required|trim');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {

            if (empty($errors)) {
                $post_data = $_POST;
                // pr($post_data);
                $ifsc_url = 'https://api.razorpay.com/v1/orders';
                $curl_pass = array(
                    "data" => json_encode($post_data),
                    "url" => $ifsc_url,
                    "type" => "POST",
                    "header" => [RAZORPAY, 'Content-Type: application/json']
                );
                $response = _curl_request($curl_pass);
            }
        }

        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Failed to get details";
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND);
        } else {
            $response = array(
                "status" => TRUE,
                "message" => $response,
            );
            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }



    public function ifsc_code_verify_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);

        $ifsc_code = "";

        $created_on  = current_date();


        if (isset($params['ifsc_code']) && !empty($params['ifsc_code'])) {
            $_POST['ifsc_code'] = $ifsc_code = $params['ifsc_code'];
        }


        $errors = array();
        $this->form_validation->set_rules('ifsc_code', 'ifsc_code', 'required|trim');
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {

            if (empty($errors)) {
                $ifsc_url = 'https://ifsc.razorpay.com/' . $ifsc_code;
                $curl_pass = array(
                    "data" => '',
                    "url" => $ifsc_url,
                    "type" => "GET",
                );
                $response = _curl_request($curl_pass);
            }
        }

        if (isset($errors) && !empty($errors)) {
            $errors['status'] = FALSE;
            $errors['message'] = "Failed to get details";
            $errors['data'] = array();
            $this->set_response($errors, REST_Controller::HTTP_NOT_FOUND);
        } else {

            if ($response == "Not Found") {
                $res = array(
                    "status" => TRUE,
                    "message" => "Data Not Found",
                    "data" => array()
                );
            } else {
                $res = array(
                    "status" => TRUE,
                    "message" => "Data Found",
                    "data" => array(
                        $response
                    )
                );
            }

            $this->set_response($res, REST_Controller::HTTP_CREATED);
        }
    }
    function generatecode($UserDetails)
    {
        $randNumber = mt_rand(11111, 99999);
        $code = 'NU' . $randNumber;
        $where[] = ["column" => "user_id", "value" => $UserDetails['user_id']];
        $data = array(
            'nucode' => $code
        );
        $update = $this->general->update($this->tbl_users, $data, $where);

        if ($update) {
            $ref_code = $UserDetails['refcode'];
            if ($ref_code != '') {
                $this->refamountadd($ref_code, $UserDetails['user_id']);
            }
        }
        return $update;
    }

    function addbonus($UserDetails)
    {

        $where_data = [];
        $get_admindata = $this->general->get_row($this->tbl_adminSettings, '*', $where_data);
        $admin_data = $get_admindata['resultSet'];
        $add_bonus = $admin_data['suss_coins'];
        $where[] = ["column" => "user_id", "value" => $UserDetails['user_id']];
        $data = array(
            'nucoins' => $add_bonus
        );
        $update = $this->general->update($this->tbl_users, $data, $where);
        if ($update) {
            $randNumber = mt_rand(11111, 99999);
            $code = 'NUCO' . $randNumber;
            $data = array(
                'transaction_id' => $code,
                'user_id' => $UserDetails['user_id'],
                'coins' => $add_bonus,
                'remark' => "Registration Bonus",
                'created_at' => current_date(),
                'updated_at' => current_date()
            );
            $insert = $this->general->add($this->tbl_coins_trascations, $data);

            if ($insert) {
                return true;
            } else {
                return false;
            }
        }
    }
    function refamountadd($ref_code, $user_id)
    {
        $where = [];
        $get_admindata = $this->general->get_row($this->tbl_adminSettings, '*', $where);
        $admin_data = $get_admindata['resultSet'];
        $ref_amount = $admin_data['ref_amount'];
        $where_userdata = [['column' => 'nucode', 'value' => $ref_code]];
        $get_userData = $this->general->get_row($this->tbl_users, '*', $where_userdata);
        $user_data = $get_userData['resultSet'];
        $current_ref_amount = $user_data['referral_amount'];
        $add_amount = $current_ref_amount + $ref_amount;
        $randNumber = mt_rand(11111, 99999);
        $code = 'NURF' . $randNumber;
        $date = current_date();
        $data = array(
            'ref_transcation_id' => $code,
            'ref_memberid' => $user_data['user_id'],
            'ref_amount' => $ref_amount,
            'ref_code' => $ref_code,
            'user_id' => $user_id,
            'status' => 1,
            'created_at' => $date,
            'updated_at' => $date,
        );
        $insert = $this->general->add($this->tbl_referrals, $data);
        if ($insert) {
            $data =  array(
                'referral_amount' => $add_amount
            );
            $where_update = [['column' => 'user_id', 'value' => $user_data['user_id']]];
            $update_wallet = $this->general->update($this->tbl_users, $data, $where_update);
            if ($update_wallet) {
                return $update_wallet;
            }
        }
    }

    public function refer_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);

        $name = $mobile_number = $email = "";

        if (isset($params['name']) && !empty($params['name'])) {
            $_POST['name'] = $name = $params['name'];
        }
        if (isset($params['mobile_number']) && !empty($params['mobile_number'])) {
            $_POST['mobile_number'] = $mobile_number = $params['mobile_number'];
        }


        $errors = array();
        $this->form_validation->set_rules('name', 'Full Name', 'required');
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required|trim|numeric|min_length[10]|max_length[10]|is_unique[tb_users.mobile_number]', array('is_unique' => 'This %s already exists. Please login'));
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            if (empty($errors)) {
                $otp = rand(1000, 9999);
                $sms_text = 'Your OTP to register/access ESY TRADE is ' . $otp . ' . It will be valid for 3 minutes. - THANK YOU';
                $send = sms_gate_way($mobile_number, $sms_text);
                if ($send) {
                    $response = array(
                        "status" => TRUE,
                        "message" => "Message Send to user",
                    );
                    $this->set_response($response, REST_Controller::HTTP_CREATED);
                }
            }
        }
        if (isset($errors) && !empty($errors)) {
            $res = [
                'status' => FALSE,
                'message' => $errors['errors']['mobile_number']
            ];
            $this->set_response($res, REST_Controller::HTTP_NOT_FOUND);
        } else {

            $this->set_response($response, REST_Controller::HTTP_CREATED);
        }
    }

    public function dashboard_post()
    {
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $user_id = "";
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $_POST['user_id'] = $user_id = $params['user_id'];
        }
        $this->form_validation->set_rules('user_id', 'User ID', 'required|trim|numeric');
        $errors = array();
        if ($this->form_validation->run() == FALSE) {
            $errors['errors'] = $this->form_validation->error_array();
        } else {
            $where[] = ["column" => "user_id", "value" => $user_id];
            $get = $this->general->get_row($this->tbl_users, '*', $where);
            if (isset($get['resultSet']) && !empty($get['resultSet'])) {

                $data = $get['resultSet'];
                $full_name = $data['full_name'];
                $nucode = $data['nucode'];
                $email =  $data['email'];
                $total =  $data['referral_amount'] + $data['nufount'];
                $nucoins = $data['nucoins'];
                $nufount = $data['nufount'];
                $referral_amount =  $data['referral_amount'];
            } else {
                $data = [];
            }
        }
        if (isset($errors) && !empty($errors)) {
            $response = array(
                "status" => FALSE,
                "errors" => $errors
            );

            $this->set_response($response, REST_Controller::HTTP_NOT_FOUND);
        } else {

            if (isset($get['resultSet']) && !empty($get['resultSet'])) {
                $response = array(
                    "status" => TRUE,
                    "message" => "Sucess",
                    "data" => array(
                        "fullname" =>  $full_name,
                        "Member ID" =>  $nucode,
                        "Email" => $email,
                        "Total Amount" => $total,
                        "NU Coins" => $nucoins,
                        "NU Credits/Rewards" => $nufount,
                        "Referraal Income" => $referral_amount
                    )

                );
            } else {
                $response = array(
                    "status" => FALSE,
                    "message" => "User Not Found",

                );
            }

            $this->set_response($response, REST_Controller::HTTP_OK);
        }
    }
}
