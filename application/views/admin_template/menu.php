<!-- START MENU SIDEBAR WRAPPER -->
<aside class="sidebar sidebar-left">
   <div class="sidebar-content">
      <div class="aside-toolbar">
         <ul class="site-logo">
            <li>
               <!-- START LOGO -->
               <a href="<?= base_url() ?>dashboard">
                  <div class="logo">
                     <img src="admin_assets/img/logo.png" alt="">
                  </div>
               </a>
               <!-- END LOGO -->
            </li>
         </ul>
      </div>
      <nav class="main-menu">
         <ul class="nav metismenu">
            <li class="sidebar-header"><span>NAVIGATION</span></li>
            <li class="<?php if ($this->uri->segment(1) == "dashboard") {
                           echo "active";
                        } ?>"><a href="<?= base_url() ?>dashboard"><i class="icon dripicons-meter"></i><span>Dashboard</span></a></li>
            
            
            
            
            <li class="<?php if ($this->uri->segment(1) == "users") {
                           echo "active";
                        } else if ($this->uri->segment(1) == "member_view") {
                           echo "active";
                        } else if ($this->uri->segment(1) == "edit_member") {
                           echo "active";
                        }
                        ?>"><a href="<?= base_url() ?>users"><i class="dripicons-user icon"></i><span>Register
                     Users</span></a></li>

            <li class="<?php if ($this->uri->segment(1) == "withdraw") {
                  echo "active";
               } else if ($this->uri->segment(1) == "view_info") {
                  echo "active";
               }
               ?>"><a href="<?= base_url() ?>withdraw"><i class="dripicons-swap icon"></i><span>Withdraw List</span></a></li>


            <li class="<?php if ($this->uri->segment(1) == "subadmin") {
                           echo "active";
                        } else if ($this->uri->segment(1) == "add_admin") {
                           echo "active";
                        } else if ($this->uri->segment(1) == "edit_admin") {
                           echo "active";
                        }
                        ?>"><a href="<?= base_url() ?>subadmin"><i class="dripicons-user-group icon"></i><span>Sub
                     Admin</span></a></li>
                     <li class="<?php if ($this->uri->segment(1) == "banners") {
                           echo "active";
                        } else if ($this->uri->segment(1) == "add_banner") {
                           echo "active";
                        }
                        else if ($this->uri->segment(1) == "edit_banner") {
                           echo "active";
                        } ?>"><a href="<?= base_url() ?>banners"><i class="dripicons-photo-group icon"></i><span>Banners</span></a></li>
            
            
            
            
            <li class="<?php if ($this->uri->segment(1) == "category") {
                           echo "active";
                        } else if ($this->uri->segment(1) == "add_category") {
                           echo "active";
                        } ?>"><a href="<?= base_url() ?>category"><i class="dripicons-store icon"></i><span>Category</span></a></li>
            
            
            
            
            <li class="<?php if ($this->uri->segment(1) == "products") {
                           echo "active";
                        } else if ($this->uri->segment(1) == "add_product") {
                           echo "active";
                        } ?>"><a href="<?= base_url() ?>products"><i class="dripicons-shopping-bag icon"></i><span>Products</span></a></li>
            
            
            
            
            
            <li class="<?php if ($this->uri->segment(1) == "sizes") {
                           echo "active";
                        } else if ($this->uri->segment(1) == "add_sizes") {
                           echo "active";
                        } 
                        else if ($this->uri->segment(1) == "edit_size") {
                           echo "active";
                        }
                        ?>"><a href="<?= base_url() ?>quires"><i class="dripicons-scale icon"></i><span>Sizes</span></a></li>
                         <li class="<?php if ($this->uri->segment(1) == "users") {
                           echo "active";
                        } else if ($this->uri->segment(1) == "member_view") {
                           echo "active";
                        } 
                        ?>"><a href="<?= base_url() ?>quires"><i class="dripicons-question icon"></i><span>Quieres</span></a></li>
                          


         </ul>
      </nav>
   </div>
</aside>
<!-- END MENU SIDEBAR WRAPPER -->
<div class="content-wrapper">
   <!-- START TOP TOOLBAR WRAPPER -->
   <nav class="top-toolbar navbar navbar-desktop flex-nowrap">
      <!-- START LEFT DROPDOWN MENUS -->

      <!-- END LEFT DROPDOWN MENUS -->
      <!-- START RIGHT TOOLBAR ICON MENUS -->
      <ul class="navbar-nav nav-right">



         <li class="nav-item dropdown">
            <a class="nav-link nav-pill user-avatar" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
               <img src="admin_assets/img/avatars/1.jpg" class="w-35 rounded-circle" alt="Albert Einstein">
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-accout">
               <a class="dropdown-item" href="<?= base_url() ?>edit_profile"><i class="icon dripicons-user"></i>
                  Profile</a>
               <div class="dropdown-divider"></div>
               <a class="dropdown-item" href="<?php echo base_url() ?>logout"><i class="icon dripicons-lock-open"></i> Sign Out</a>
            </div>
         </li>
      </ul>
      <!-- END RIGHT TOOLBAR ICON MENUS -->
   </nav>
   <!-- END TOP TOOLBAR WRAPPER -->