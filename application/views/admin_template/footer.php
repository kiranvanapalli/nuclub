<!-- <div class="box-footer position-relative">
    <footer class="border-top mt-auto">
        <div class="col-12 text-center">
            <p>Copyright © 2022 NUClub</p>
        </div>
    </footer>
</div> -->

<footer class="page-footer font-small purple pt-4 text-center">
    <!-- <footer class="navbar footer fixed-bottom footer-light footer-shadow content container-fluid"> -->
    <div class="footer-copyright text-center py-3">
        <p>Copyright © 2022 NUClub</p>
    </div>
</footer>
<!-- <footer class="border-top">
    <div class="row">
        <div class="col-12 col-sm-6">This template is designed by <b>Adnectics</b></div>
    </div>
</footer> -->
</section>
<!--END PAGE CONTENT -->
</div>
</div>

<!-- END CONTENT WRAPPER -->
<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
<script src="admin_assets/vendor/modernizr/modernizr.custom.js"></script>
<script src="admin_assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="admin_assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="admin_assets/vendor/js-storage/js.storage.js"></script>
<script src="admin_assets/vendor/js-cookie/src/js.cookie.js"></script>
<script src="admin_assets/vendor/pace/pace.js"></script>
<script src="admin_assets/vendor/metismenu/dist/metisMenu.js"></script>
<script src="admin_assets/vendor/switchery-npm/index.js"></script>
<script src="admin_assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- ================== PAGE LEVEL VENDOR SCRIPTS ==================-->
<script src="admin_assets/vendor/countup.js/dist/countUp.min.js"></script>
<script src="admin_assets/vendor/chart.js/dist/Chart.bundle.min.js"></script>
<!-- <script src="admin_assets/vendor/flot/jquery.flot.js"></script> -->
<!-- <script src="admin_assets/vendor/jquery.flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="admin_assets/vendor/flot/jquery.flot.resize.js"></script>
<script src="admin_assets/vendor/flot/jquery.flot.time.js"></script> -->
<!-- <script src="admin_assets/vendor/flot.curvedlines/curvedLines.js"></script> -->
<script src="admin_assets/vendor/datatables.net/js/jquery.dataTables.js"></script>
<script src="admin_assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
<!-- ================== GLOBAL APP SCRIPTS ==================-->
<script src="admin_assets/js/global/app.js"></script>
<!-- ================== PAGE LEVEL SCRIPTS ==================-->
<script src="admin_assets/js/components/countUp-init.js"></script>
<script src="admin_assets/js/cards/counter-group.js"></script>
<script src="admin_assets/js/cards/recent-transactions.js"></script>
<script src="admin_assets/vendor/moment/min/moment.min.js"></script>
<script src="admin_assets/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="admin_assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="admin_assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="admin_assets/vendor/ckeditor/ckeditor.js"></script>
<!-- <script src="admin_assets/js/cards/monthly-budget.js"></script> -->
<script src="admin_assets/js/cards/users-chart.js"></script>
<script src="admin_assets/js/cards/bounce-rate-chart.js"></script>
<script src="admin_assets/js/cards/session-duration-chart.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="admin_assets/js/components/bootstrap-datepicker-init.js"></script>
<script src="admin_assets/js/components/bootstrap-date-range-picker-init.js"></script>

 <!--Toster end plugins -->

   <!--Start toster validation -->
   <?php if ($this->session->flashdata('success')) {  ?>
      <script type="text/javascript">
         toastr.success("<?php echo $this->session->flashdata('success'); ?>", "", {
            "closeButton": "true",
            "progressBar": "true",
            "timeOut": "5000",
            "extendedTimeOut": "2000"
         });
      </script>
   <?php  } elseif ($this->session->flashdata('error')) { ?>
      <script type="text/javascript">
         toastr.error("<?php echo $this->session->flashdata('error'); ?>", "", {
            "closeButton": "true",
            "progressBar": "true"
         });
      </script>
   <?php } ?>

   <?php
   $err = validation_errors();
   $err_msg   = str_replace(array("\r", "\n"), '\n', $err);
   if (isset($err_msg) &&  $err_msg != "") { ?>
      <script type="text/javascript">
         toastr.error("<?php echo $err_msg; ?>", "", {
            "closeButton": "true",
            "progressBar": "true"
         });
      </script>
   <?php  } ?>
   <!--End toster validation -->
  
</body>

</html>